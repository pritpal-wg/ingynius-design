<?php
if (is_array($_FILES)) {
    if (is_uploaded_file($_FILES['userImage']['tmp_name'])) {
        $sourcePath = $_FILES['userImage']['tmp_name'];
        $targetPath = "uploads/" . $_FILES['userImage']['name'];
        if (move_uploaded_file($sourcePath, $targetPath)) {
            ?>
            <img data-name="<?php echo $_FILES['userImage']['name']; ?>" id="img_name" src="<?php echo $targetPath; ?>" class="img img-thumbnail"/>
            <?php
        }
    }
}
?>