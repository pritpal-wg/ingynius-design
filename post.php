<?php
$post_type = $_GET['post_type'];
$title = "Post Details";
?>
<?php include_once __DIR__ . '/header.php' ?>
<div class="col-md-1 bhoechie-tab-menu" style="width:6%;background-image: url('images/sub_nav_menu_bg.png');">
    <div class="list-group">
        <a href="#" class="list-group-item active text-center" data-post_type="a-z">
            <h4 class="fa fa-2x fa-clock-o"></h4><br/>A-Z
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="a-z">
            <h4 class="fa fa-2x fa-user"></h4><br/>Only Mine
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="all posts">
            <h4 class="fa fa-2x fa-square"></h4><br/>Show All
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="media">
            <h4 class="fa fa-2x fa-music"></h4><br/>Media
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="collection">
            <h4 class="fa fa-2x fa-columns"></h4><br/><p style="margin-left: -10px;">Collections</p>
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="events">
            <h4 class="fa fa-2x fa-calendar"></h4><br/>Events 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="blogs">
            <h4 class="fa fa-2x fa-edit"></h4><br/>Blogs
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="Groups">
            <h4 class="fa fa-2x fa-users"></h4><br/>Groups 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="Pages">
            <h4 class="fa fa-2x fa-paste"></h4><br/>Pages 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="favorites">
            <h4 class="fa fa-2x fa-star"></h4><br/>Favorites 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="member">
            <h4 class="fa fa-2x fa-users"></h4><br/>Members
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="settings">
            <h4 class="fa fa-2x fa-cog"></h4><br/>Settings 
        </a>
    </div>
</div>
<div id="all_posts_div" class="col-md-7" style="<?php echo $f_name == 'index.php' ? '' : 'background-color: #fff;'; ?>padding: 5px 5px;width:50%">
    <section id="service" class="layers">
        <div class="page_content">
            <div class="container-fluid no-marg">
                <div>
                    <div class="col-md-12" style="background-color: #fff;margin-bottom: 20px">
                        <div class="row" style="margin-bottom: 10px;margin-top: 10px">
                            <div class="col-md-12">
                                <?php if ($post_type == 'image') { ?>
                                    <img class="img img-thumbnail" src="images/cover-1.jpg" style="width:100%;height: 325px;"/>
                                <?php } else { ?>
                                    <iframe src="https://www.youtube.com/embed/xaSH0d60Zso" style="width:100%;height: 325px;"></iframe>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="text-align:center">
                                <span style="font-size: 15px"><i style="color:darkred" class="fa fa-user"></i> John Doe</span>
                                <span style="color:#999;margin-left: 10px"><i style="color:darkred" class="fa fa-clock-o"></i> Posted on 2015-10-02</span>
                                <span style="color:#999;margin-left: 10px"><i style="color:darkred" class="fa fa-map-marker"></i> Sydney, Australia</span>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;margin-bottom: 10px;padding-bottom: 5px;border-bottom: 1px #eee solid;">
                            <div class="col-md-12" style="margin-top: 10px;margin-bottom: 10px;padding-bottom: 5px;text-align:center;font-size: 25px">Comments</div>
                            <div class="col-md-2">
                                <img class="img img-thumbnail" src="images/cover-2.jpg" alt="Logo">
                            </div>
                            <div class="col-md-10">
                                <div style="margin-left: -20px;">
                                    <div class="form-group">
                                        <input placeholder="Post Comment..." type="text" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;margin-bottom: 10px;padding-bottom: 5px;border-bottom: 1px #eee solid;">
                            <div class="col-md-2">
                                <img class="img img-thumbnail" src="images/cover-2.jpg" alt="Logo">
                            </div>
                            <div class="col-md-10">
                                <div style="margin-left: -20px;">
                                    <span style="color:grey">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry.Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</span>
                                    <br/>
                                    <div class="col-md-12" style="margin-left: -20px;">
                                        <span style="margin-left: 10px"><i style="color:darkred" class="fa fa-thumbs-up"></i> 54</span>
                                        <span style="margin-left: 10px"><i style="color:darkred" class="fa fa-thumbs-down"></i> 11</span>
                                        <span style="margin-left: 10px"><i style="color:darkred" class="fa fa-clock-o"></i> 11:05 AM</span>
                                        <span style="margin-left: 10px"><i style="color:darkred" class="fa fa-map-marker"></i> Tronto, Australia</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php for ($i = 0; $i < 6; $i++) { ?>
                            <div class="row" style="margin-bottom: 10px;padding-bottom: 5px;border-bottom: 1px #eee solid;">
                                <div class="col-md-2">
                                    <img class="img img-thumbnail" src="images/cover-1.jpg" alt="Logo">
                                </div>
                                <div class="col-md-10">
                                    <div style="margin-left: -20px;">
                                        <span style="color:grey">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry.Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</span>
                                        <br/>
                                        <div class="col-md-12" style="margin-left: -20px;">
                                            <span style="margin-left: 10px"><i style="color:darkred" class="fa fa-thumbs-up"></i> 54</span>
                                            <span style="margin-left: 10px"><i style="color:darkred" class="fa fa-thumbs-down"></i> 11</span>
                                            <span style="margin-left: 10px"><i style="color:darkred" class="fa fa-clock-o"></i> 05:40 PM</span>
                                            <span style="margin-left: 10px"><i style="color:darkred" class="fa fa-map-marker"></i> Sydney, Australia</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <!-- End .page_content -->
    </section>
</div>
<?php
include_once __DIR__ . '/footer.php';
