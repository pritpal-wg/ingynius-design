<?php
$group = FALSE;
if (isset($_GET['group']))
    $group = TRUE;
$title = $group ? 'About US' : "About Me";
$pic_type = $group ? 'square' : 'circle';
if ($group) {
    $menus = array(
        'index.php' => array(
            'icon' => 'home',
            'label' => 'Home',
        ),
        'index-2.php' => array(
            'icon' => 'home',
            'label' => 'Home 2',
        ),
        'about.php?group' => array(
            'icon' => 'user',
            'label' => 'About Us',
        ),
        'blog.php' => array(
            'icon' => 'comments',
            'label' => 'Blog',
        ),
        'events.php' => array(
            'icon' => 'calendar',
            'label' => 'Events',
        ),
        'members.php' => array(
            'icon' => 'users',
            'label' => 'Members',
        ),
        'followers.php' => array(
            'icon' => 'users',
            'label' => 'Followers',
        ),
        'groups_contact.php' => array(
            'icon' => 'paper-plane',
            'label' => 'Contact',
        ),
    );
    $show_quick_icons = FALSE;
}
?>
<?php include_once __DIR__ . '/header.php' ?>
<div class="col-md-1 bhoechie-tab-menu" style="width:6%;background-image: url('images/sub_nav_menu_bg.png');">
    <div class="list-group">
        <a href="#" class="list-group-item active text-center" data-post_type="a-z">
            <h4 class="fa fa-2x fa-clock-o"></h4><br/>A-Z
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="a-z">
            <h4 class="fa fa-2x fa-user"></h4><br/>Only Mine
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="all posts">
            <h4 class="fa fa-2x fa-square"></h4><br/>Show All
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="media">
            <h4 class="fa fa-2x fa-music"></h4><br/>Media
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="collection">
            <h4 class="fa fa-2x fa-columns"></h4><br/><p style="margin-left: -10px;">Collections</p>
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="events">
            <h4 class="fa fa-2x fa-calendar"></h4><br/>Events 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="blogs">
            <h4 class="fa fa-2x fa-edit"></h4><br/>Blogs
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="Groups">
            <h4 class="fa fa-2x fa-users"></h4><br/>Groups 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="Pages">
            <h4 class="fa fa-2x fa-paste"></h4><br/>Pages 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="favorites">
            <h4 class="fa fa-2x fa-star"></h4><br/>Favorites 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="member">
            <h4 class="fa fa-2x fa-users"></h4><br/>Members
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="settings">
            <h4 class="fa fa-2x fa-cog"></h4><br/>Settings 
        </a>
    </div>
</div>
<div id="all_posts_div" class="col-md-7" style="<?php echo $f_name == 'index.php' ? '' : 'background-color: #fff;'; ?>padding: 5px 5px;width:50%">
    <div class="row no-marg my_profile">
        <div class="col-md-12 no-padd info_profile">
            <!-- .bg_desc -->
            <div class="bg_desc clearfix">
                <div class="inner-text">
                    <h2>I am John Doe</h2>
                    <small class="myjob">web developer</small>

                    <div class="feature-desc">
                        <p>My money’s in that office, right? If she start giving me some bullshit about it ain’t there, and we got to go someplace else and get it, I’m gonna shoot you in the head then and there. Then I’m gonna shoot that bitch in the kneecaps, find out where my goddamn money is. She gonna tell me too.</p>
                        <p>Quickly pursue revolutionary relationships before scalable technologies. Proactively pontificate out-of-the-box alignments with equity invested results.</p>
                        <p>Aliquam lobortis nec nibh a blandit. Quisque ultricies pharetra hendrerit. Nunc fringilla sapien eget viverra mollis.</p>
                    </div> 

                    <div class="signature text-right">
                        <div class="inner">
                            <img src="images/signature.png" alt="Signature">
                            <cite class="name">john doe</cite>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End .bg_desc -->
            <!-- .bg_info -->
            <div class="bg_info clearfix">
                <div class="inner-text">
                    <ul>
                        <li class="user_name"><strong><i class="fa fa-user"></i>Name</strong> : john doe</li>
                        <li><strong><i class="fa fa-calendar"></i>Birthdate</strong> : March 18, 1988</li>
                        <li><strong><i class="fa fa-phone"></i>Phone</strong> : +123 (21) 1234-5678</li>
                        <li><strong><i class="glyphicon glyphicon-phone"></i>Mobile</strong> : +123 (21) 1234-5679</li>
                        <li><strong><i class="fa fa-skype"></i>Skype</strong> : John_Doe</li>
                        <li><strong><i class="fa fa-envelope"></i>Email</strong> : john.doe@gmail.com</li>
                        <li><strong><i class="fa fa-globe"></i>Website</strong> : <a href="#" target="_blank">www.anderson-smith.com</a></li>
                        <li><strong><i class="glyphicon glyphicon-map-marker"></i>Adresse</strong> : 1732 Monroe Street Houston, TX 77055.</li>
                    </ul>
                </div>
            </div>
            <!-- End .bg_info -->
            <!-- .header_social_ul -->
            <div class="social-ul bg_social" id="profile_social_ul">
                <ul>
                    <li class="social-twitter"><a href="#" target="_blank" title="" data-toggle="tooltip" data-original-title="Twitter"><i class="fa fa-twitter"></i></a></li>
                    <li class="social-facebook"><a href="#" target="_blank" title="" data-toggle="tooltip" data-original-title="Facebook"><i class="fa fa-facebook"></i></a></li>
                    <li class="social-google"><a href="#" target="_blank" title="" data-toggle="tooltip" data-original-title="Google Plus"><i class="fa fa-google-plus"></i></a></li>
                    <li class="social-linkedin"><a href="#" target="_blank" title="" data-toggle="tooltip" data-original-title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                    <li class="social-dribbble"><a href="#" target="_blank" title="" data-toggle="tooltip" data-original-title="Dribbble"><i class="fa fa-dribbble"></i></a></li>
                    <li class="social-behance"><a href="#" target="_blank" title="" data-toggle="tooltip" data-original-title="Behance"><i class="fa fa-behance"></i></a></li>
                </ul>
            </div>
            <!-- End .header_social_ul -->
        </div>
        <!-- End .info_profile -->
    </div>
</div>
<?php include_once __DIR__ . '/footer.php'; ?>