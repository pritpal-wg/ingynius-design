<?php
$title = "Create Group";
$pic_type = 'square';
$menus = array(
    'index.php' => array(
        'icon' => 'home',
        'label' => 'Home',
    ),
    'index-2.php' => array(
        'icon' => 'home',
        'label' => 'Home 2',
    ),
    'groups.php' => array(
        'icon' => 'home',
        'label' => 'Group Home',
    ),
    'groups_about.php' => array(
        'icon' => 'user',
        'label' => 'About Us',
    ),
    'groups_blog.php' => array(
        'icon' => 'comments',
        'label' => 'Blog',
    ),
    'groups_events.php' => array(
        'icon' => 'calendar',
        'label' => 'Events',
    ),
    'groups_members.php' => array(
        'icon' => 'users',
        'label' => 'Members',
    ),
    'groups_followers.php' => array(
        'icon' => 'users',
        'label' => 'Followers',
    ),
    'groups_contact.php' => array(
        'icon' => 'paper-plane',
        'label' => 'Contact',
    ),
);
$show_quick_icons = FALSE;
?>
<?php include_once __DIR__ . '/header.php' ?>
<div class="col-md-1 bhoechie-tab-menu" style="width:6%;background-image: url('images/sub_nav_menu_bg.png');">
    <div class="list-group">
        <a href="#" class="list-group-item active text-center" data-post_type="a-z">
            <h4 class="fa fa-2x fa-clock-o"></h4><br/>A-Z
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="a-z">
            <h4 class="fa fa-2x fa-user"></h4><br/>Only Mine
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="all posts">
            <h4 class="fa fa-2x fa-square"></h4><br/>Show All
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="media">
            <h4 class="fa fa-2x fa-music"></h4><br/>Media
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="collection">
            <h4 class="fa fa-2x fa-columns"></h4><br/><p style="margin-left: -10px;">Collections</p>
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="events">
            <h4 class="fa fa-2x fa-calendar"></h4><br/>Events 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="blogs">
            <h4 class="fa fa-2x fa-edit"></h4><br/>Blogs
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="Groups">
            <h4 class="fa fa-2x fa-users"></h4><br/>Groups 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="Pages">
            <h4 class="fa fa-2x fa-paste"></h4><br/>Pages 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="favorites">
            <h4 class="fa fa-2x fa-star"></h4><br/>Favorites 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="member">
            <h4 class="fa fa-2x fa-users"></h4><br/>Members
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="settings">
            <h4 class="fa fa-2x fa-cog"></h4><br/>Settings 
        </a>
    </div>
</div>
<div id="all_posts_div" class="col-md-7" style="<?php echo $f_name == 'index.php' ? '' : 'background-color: #fff;'; ?>padding: 5px 5px;width:50%">
    <div class="row-fluid" style="min-height: 890px">
        <div class="col-md-12">
            <h2 class="page-header" style="margin: 40px 0 20px 15px">
                <span class="pull-right"style="margin-right: 15px;">
                    <a href="groups_manage.php" class="btn btn-success"><i class="fa fa-list"></i> My Groups</a>
                </span>
                Groups You Manage
            </h2>
            <form class="form-horizontal" method="post">
                <div class="form-group">
                    <label for="fname" class="col-sm-2 control-label">Group Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="fname" id="gname" placeholder="Group Name">
                    </div>
                </div>
                <div class="form-group">
                    <label for="lname" class="col-sm-2 control-label">Members</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="lname" id="lname" placeholder="Who do you want to add to the group?">
                    </div>
                </div>
                <hr/>
                <div class="form-group">
                    <label class="control-label col-md-2">Favorites</label>
                    <div class="col-sm-10">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="">
                                Add this group to your favorites.
                            </label>
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="form-group">
                    <label class="control-label col-md-2">Privacy</label>
                    <div class="col-sm-10">
                        <div class="radio">
                            <label>
                                <input type="radio" value="">
                                <i class="fa fa-globe"></i> Public<br/>
                                <font color="grey">Anyone can see the group, its members and their posts.</font>
                                <hr/>
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" value="">
                                <i class="fa fa-lock"></i> Closed<br/>
                                <font color="grey">Anyone can find the group and see who's in it. Only members can see posts.</font>
                                <hr/>
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" value="">
                                <i class="fa fa-key"></i> Secret<br/>
                                <font color="grey">Anyone can see the group, its members and their posts.</font>
                            </label>
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <!--<button type="submit" class="btn btn-default">Sign up</button>-->
                        <button type="button" id="save_group" class="btn btn-success">Create</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        if (!localStorage.pages) {
            localStorage.pages = JSON.stringify([]);
        }
    });
    $(document).on('click', '#save_group', function (e) {
        e.preventDefault();
        toastr.remove();
        var g_name = $('#gname').val();
        var time_id = Date.now();
        if (g_name) {
            var group = {};
            group.id = time_id;
            group.name = $('#gname').val();
            if (localStorage !== undefined) {
                if (!localStorage.groups) {
                    localStorage.groups = JSON.stringify([]);
                } else {
                    var groups = JSON.parse(localStorage.groups);
                    groups.push(group);
                    localStorage.groups = JSON.stringify(groups);
                    window.location.href = "groups_manage.php";
                }
            }
        } else {
            toastr.error('Please Enter Group Name!', 'Error');
        }
    });
</script>
<!-- #content-wrapper -->
<?php include_once __DIR__ . '/footer.php'; ?>