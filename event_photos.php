<?php
$title = "Event Photos";
$menus = array(
    'index.php' => array(
        'icon' => 'home',
        'label' => 'Home',
    ),
    'index-2.php' => array(
        'icon' => 'home',
        'label' => 'Home 2',
    ),
    'event_details.php' => array(
        'icon' => 'file-text',
        'label' => 'Details',
    ),
    'event_register_ticket.php' => array(
        'icon' => 'edit',
        'label' => 'Register Tickets',
    ),
    'event_photos.php' => array(
        'icon' => 'image',
        'label' => 'Photos',
    ),
    'event_videos.php' => array(
        'icon' => 'video-camera',
        'label' => 'Videos',
    ),
    'event_attending.php' => array(
        'icon' => 'users',
        'label' => 'Who\'s Attending',
    ),
    'event_contact.php' => array(
        'icon' => 'paper-plane',
        'label' => 'Contact',
    ),
);
$show_quick_icons = FALSE;
?>
<?php include_once __DIR__ . '/header.php' ?>
<div class="col-md-1 bhoechie-tab-menu" style="width:6%;background-image: url('images/sub_nav_menu_bg.png');">
    <div class="list-group">
        <a href="#" class="list-group-item active text-center" data-post_type="a-z">
            <h4 class="fa fa-2x fa-clock-o"></h4><br/>A-Z
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="a-z">
            <h4 class="fa fa-2x fa-user"></h4><br/>Only Mine
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="all posts">
            <h4 class="fa fa-2x fa-square"></h4><br/>Show All
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="media">
            <h4 class="fa fa-2x fa-music"></h4><br/>Media
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="collection">
            <h4 class="fa fa-2x fa-columns"></h4><br/><p style="margin-left: -10px;">Collections</p>
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="events">
            <h4 class="fa fa-2x fa-calendar"></h4><br/>Events 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="blogs">
            <h4 class="fa fa-2x fa-edit"></h4><br/>Blogs
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="Groups">
            <h4 class="fa fa-2x fa-users"></h4><br/>Groups 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="Pages">
            <h4 class="fa fa-2x fa-paste"></h4><br/>Pages 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="favorites">
            <h4 class="fa fa-2x fa-star"></h4><br/>Favorites 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="member">
            <h4 class="fa fa-2x fa-users"></h4><br/>Members
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="settings">
            <h4 class="fa fa-2x fa-cog"></h4><br/>Settings 
        </a>
    </div>
</div>
<div id="all_posts_div" class="col-md-7" style="<?php echo $f_name == 'index.php' ? '' : 'background-color: #fff;'; ?>padding: 5px 5px;width:50%">
    <section class="blog-content-grid">
        <div class="row">
            <?php for ($i = 0; $i < 12; $i++) { ?>
                <!-- post -->
                <div class="col-md-3 col-sm-6">
                    <article>
                        <div class="post-thumb">
                            <a href="blog_single.php" class="image-link">
                                <!--<img src="http://lorempixel.com/300/200/" />-->
                                <img src="http://lorempixel.com/300/<?php echo (200 + $i) ?>" />
                            </a>					
                        </div>
                        <div class="post-body">
                            <h3 class="post-title"><a href="blog_single.php">Event Photo <?php echo $i + 1 ?></a></h3>
                            <div class="post-meta">
                                <ul>
                                    <li><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-clock"></i>January 18, 2015</li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-folder"></i><a href="#" rel="category tag">WordPress</a></li>
                                </ul>
                            </div>
                            <div class="post-content">
                                <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat.</p>
                            </div>
                            <a href="blog_single.php" class="read_more_but"><i class="fa fa-eye"></i> View Photo</a>
                            <div class="footer_post">
                                <ul>
                                    <li><i class="icon-picture"></i></li>
                                    <li><i class="icon-bubble"></i> <a href="#">4</a></li>
                                    <li><i class="icon-heart"></i> <a href="#">35</a></li>
                                    <li><i class="icon-eye"></i> 216</li>
                                </ul>
                            </div>
                        </div>
                    </article>
                </div>
                <!-- //post -->
            <?php } ?>
        </div>
    </section>
    <div class="main-pagination">
        <span class="page-numbers current">1</span>
        <a class="page-numbers" href="#">2</a>
        <a class="page-numbers" href="#">3</a>
        <a class="page-numbers" href="#">4</a>
        <a class="next page-numbers" href="#">
            <span class="visuallyhidden">Next</span><i class="fa fa-angle-right"></i>
        </a>	
    </div>
</div>
<?php include_once __DIR__ . '/footer.php'; ?>