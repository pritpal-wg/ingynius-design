<?php
$title = "Company Pages";
$pic_type = 'square';
$menus = array(
    'index.php' => array(
        'icon' => 'home',
        'label' => 'Home',
    ),
    'index-2.php' => array(
        'icon' => 'home',
        'label' => 'Home 2',
    ),
    'company.php' => array(
        'icon' => 'home',
        'label' => 'Company Home',
    ),
    'company_upgrade.php' => array(
        'icon' => 'wrench',
        'label' => 'Upgrade Account',
    ),
    'company_services.php' => array(
        'icon' => 'cog',
        'label' => 'Services',
    ),
    'company_blog.php' => array(
        'icon' => 'comments',
        'label' => 'Blog',
    ),
    'company_followers.php' => array(
        'icon' => 'users',
        'label' => 'Followers',
    ),
    'company_contact.php' => array(
        'icon' => 'paper-plane',
        'label' => 'Contact',
    ),
);
$show_quick_icons = FALSE;
?>
<?php include_once __DIR__ . '/header.php' ?>
<div class="col-md-1 bhoechie-tab-menu" style="width:6%;background-image: url('images/sub_nav_menu_bg.png');">
    <div class="list-group">
        <a href="#" class="list-group-item active text-center" data-post_type="a-z">
            <h4 class="fa fa-2x fa-clock-o"></h4><br/>A-Z
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="a-z">
            <h4 class="fa fa-2x fa-user"></h4><br/>Only Mine
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="all posts">
            <h4 class="fa fa-2x fa-square"></h4><br/>Show All
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="media">
            <h4 class="fa fa-2x fa-music"></h4><br/>Media
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="collection">
            <h4 class="fa fa-2x fa-columns"></h4><br/><p style="margin-left: -10px;">Collections</p>
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="events">
            <h4 class="fa fa-2x fa-calendar"></h4><br/>Events 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="blogs">
            <h4 class="fa fa-2x fa-edit"></h4><br/>Blogs
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="Groups">
            <h4 class="fa fa-2x fa-users"></h4><br/>Groups 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="Pages">
            <h4 class="fa fa-2x fa-paste"></h4><br/>Pages 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="favorites">
            <h4 class="fa fa-2x fa-star"></h4><br/>Favorites 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="member">
            <h4 class="fa fa-2x fa-users"></h4><br/>Members
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="settings">
            <h4 class="fa fa-2x fa-cog"></h4><br/>Settings 
        </a>
    </div>
</div>
<div id="all_posts_div" class="col-md-7" style="<?php echo $f_name == 'index.php' ? '' : 'background-color: #fff;'; ?>padding: 5px 5px;width:50%">
    <div class="row-fluid" style="min-height: 890px">
        <div class="col-md-12">
            <h2 class="page-header" style="border:none;margin: 40px 0 20px 0">
                <span class="pull-right"style="margin-right: 15px;">
                    <a href="company_pages_manage.php" class="btn btn-success"><i class="fa fa-plus"></i> Create Page</a>
                </span>
                Pages
            </h2>
        </div>
        <br/>
        <br/>
        <div class="row-fluid">
            <div class="col-md-12">
                <div id="pages_container">
                    <h3 class="page-container">You do not have any pages yet.</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        populatePages();
    });
    $(document).on('click', '#delete_page', function () {
        var id = Number($(this).attr('data-id'));
        var pages = $.parseJSON(localStorage.pages);
        var new_pages = [];
        $.each(pages, function (l, v) {
            if (v.id !== id) {
                new_pages.push(v);
            }
        });
        localStorage.pages = JSON.stringify(new_pages);
        $(this).closest('[for="ig_page"]').hide(400, function () {
            $(this).remove();
            if (!$('#pages_container .list-group li[for="ig_page"]').length) {
                $('#pages_container').html('<h3 class="page-container">You do not have any pages yet.</h3>');
            }
        });
    });
    function populatePages() {
        if (localStorage && localStorage.pages) {
            var pages = $.parseJSON(localStorage.pages);
            if (pages.length) {
                var pages_html = '';
                pages_html += '<ul class="list-group">';
                $.each(pages, function (l, v) {
                    pages_html += '<li class="list-group-item" style="height: 47px;" for="ig_page">';
                    pages_html += '<span class="pull-right">';
                    pages_html += '<span class="btn-group">';
                    pages_html += '<a href="manage_page.php?p_id=' + v.id + '" class="btn btn-primary btn-xs">Manage Page</a>';
                    pages_html += '</span>';
                    pages_html += '<a href="create_page.php?p_id=' + v.id + '" class="text-primary" style="margin: 0 10px"><i class="fa fa-cog"></i></a>';
                    pages_html += '<button id="delete_page" data-id=' + v.id + ' type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>';
                    pages_html += '</span>';
                    pages_html += '<a style="color:#3b5998" href="page.php?p_id=' + v.id + '"><img style="height: 25px;width:43px" class="img img-thumbnail" src="uploads/' + v.photo + '"/> ' + v.name + '</a>';
                    pages_html += '</li>';
                });
                pages_html += '</ul>';
                $('#pages_container').html(pages_html);
            }
        }
    }

</script>
<!-- #content-wrapper -->
<?php include_once __DIR__ . '/footer.php'; ?>