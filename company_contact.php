<?php
$title = "Company Contact";
$pic_type = 'square';
$menus = array(
    'index.php' => array(
        'icon' => 'home',
        'label' => 'Home',
    ),
    'index-2.php' => array(
        'icon' => 'home',
        'label' => 'Home 2',
    ),
    'company.php' => array(
        'icon' => 'home',
        'label' => 'Company Home',
    ),
    'company_upgrade.php' => array(
        'icon' => 'wrench',
        'label' => 'Upgrade Account',
    ),
    'company_services.php' => array(
        'icon' => 'cog',
        'label' => 'Services',
    ),
    'company_blog.php' => array(
        'icon' => 'comments',
        'label' => 'Blog',
    ),
    'company_followers.php' => array(
        'icon' => 'users',
        'label' => 'Followers',
    ),
    'company_contact.php' => array(
        'icon' => 'paper-plane',
        'label' => 'Contact',
    ),
);
$show_quick_icons = FALSE;
?>
<?php include_once __DIR__ . '/header.php' ?>
<!-- content -->
<div id="content-wrapper">
    <div id="main-content">
        <section id="contact" class="layers">
            <h2 style="display:none">contact</h2>
            <!-- .page_content -->
            <div class="page_content">
                <!-- #contact -->
                <div id="contacts">
                    <div id="mapContainer">
                        <div id="tabs" class="tab_close" data-toggle="tooltip" data-original-title="Contact">
                            <i class="fa fa-book"></i>
                        </div>
                        <div id="tabs_resp" class="tab_close" data-toggle="tooltip" data-original-title="Contact">
                            <i class="fa fa-book"></i>
                        </div>
                        <div id="map_canvas"></div>
                    </div>
                    <div id="contentContact" class="two">
                        <div class="contact_closed" id="contact_closed" title="Close Contact" data-toggle="tooltip">
                            <i class="fa fa-times"></i>
                        </div>
                        <div class="innerpadding">
                            <div class="separte-content">
                                <h2>Let’s be social</h2>
                                <!-- social-ul -->
                                <div class="social-ul" id="contact_social_ul">
                                    <ul>
                                        <li class="social-twitter"><a href="#" target="_blank" title="" data-toggle="tooltip" data-original-title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                        <li class="social-facebook"><a href="#" target="_blank" title="" data-toggle="tooltip" data-original-title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                        <li class="social-google"><a href="#" target="_blank" title="" data-toggle="tooltip" data-original-title="Google Plus"><i class="fa fa-google-plus"></i></a></li>
                                        <li class="social-linkedin"><a href="#" target="_blank" title="" data-toggle="tooltip" data-original-title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                                        <li class="social-dribbble"><a href="#" target="_blank" title="" data-toggle="tooltip" data-original-title="Dribbble"><i class="fa fa-dribbble"></i></a></li>
                                        <li class="social-behance"><a href="#" target="_blank" title="" data-toggle="tooltip" data-original-title="Behance"><i class="fa fa-behance"></i></a></li>
                                        <li class="social-pinterest"><a href="#" target="_blank" title="" data-toggle="tooltip" data-original-title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
                                        <li class="social-github"><a href="#" target="_blank" title="" data-toggle="tooltip" data-original-title="Github"><i class="fa fa-github"></i></a></li>
                                        <li class="social-flickr"><a href="#" target="_blank" title="" data-toggle="tooltip" data-original-title="Flickr"><i class="fa fa-flickr"></i></a></li>
                                        <!--<li class="social-vine"><a href="#" target="_blank" title="" data-toggle="tooltip" data-original-title="Vine"><i class="fa fa-vine"></i></a></li>-->
                                    </ul>
                                </div>
                                <!-- End social-ul -->
                            </div>
                            <div class="separte-content">
                                <!-- Send a Message -->
                                <h2>Send a Message</h2>
                                <div id="contact-status"></div>
                                <form action="#" id="contactform" class="contact-form">
                                    <div class="row form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12 form-group" id="contact-name">
                                            <i class="fa fa-user icon-contact"></i>
                                            <input type="text" name="name" class="form-control name-contact" style="margin-top: 0;" placeholder="Name..." />
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12 form-group" id="contact-email">
                                            <i class="fa fa-envelope icon-contact"></i>
                                            <input type="text" name="email" class="form-control email-contact" placeholder="Email..." />
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12 form-group" id="contact-subject">
                                            <i class="fa fa-question icon-contact"></i>
                                            <input type="text" name="subject" class="form-control subject-contact" placeholder="Subject..." />
                                        </div> 	
                                        <div class="col-md-12 col-sm-12 col-xs-12 form-group" id="contact-message">
                                            <i class="fa fa-comments icon-contact"></i>
                                            <textarea name="message" cols="90" rows="10" class="form-control message-contact" id="inputError" placeholder="Message..."></textarea>
                                        </div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 form-group" style="margin-bottom: 0;">
                                            <button type="submit" class="btn btn-block btn-cta btn-cta-contact-2">Send<i class="fa fa-angle-right"></i></button>
                                        </div>   
                                    </div><!--//row-->
                                </form><!--//contact-form-->
                                <!-- End Send a Message -->
                            </div>
                            <div class="collapser">
                                <h2>other way</h2>
                                <ul class="info_contact">
                                    <li><span><i class="fa fa-map-marker"></i></span> 44 E. 8th Street Suite 300 Holland.</li>
                                    <li><span><i class="fa fa-phone"></i></span> +1-888-999-7776</li>
                                    <li><span><i class="glyphicon glyphicon-phone"></i></span> +1-888-999-7776</li>
                                    <li><span><i class="fa fa-envelope"></i></span> info@loremips.com</li>
                                    <li><span><i class="fa fa-globe"></i></span> <a href="#">www.loremips.com</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- end .innerpadding -->
                    </div>
                    <!-- end #contentContact -->
                </div>
                <!-- #contact -->
            </div>
            <!-- .page_content -->
        </section>
    </div>
</div>
<!-- #content-wrapper -->
<?php include_once __DIR__ . '/footer.php'; ?>