<?php

$title = 'Group About';
$pic_type = 'square';
$menus = array(
    'index.php' => array(
        'icon' => 'home',
        'label' => 'Home',
    ),
    'index-2.php' => array(
        'icon' => 'home',
        'label' => 'Home 2',
    ),
    'groups.php' => array(
        'icon' => 'home',
        'label' => 'Group Home',
    ),
    'groups_about.php' => array(
        'icon' => 'user',
        'label' => 'About Us',
    ),
    'groups_blog.php' => array(
        'icon' => 'comments',
        'label' => 'Blog',
    ),
    'groups_events.php' => array(
        'icon' => 'calendar',
        'label' => 'Events',
    ),
    'groups_members.php' => array(
        'icon' => 'users',
        'label' => 'Members',
    ),
    'groups_followers.php' => array(
        'icon' => 'users',
        'label' => 'Followers',
    ),
    'groups_contact.php' => array(
        'icon' => 'paper-plane',
        'label' => 'Contact',
    ),
);
$show_quick_icons = FALSE;
?>
<?php include_once __DIR__ . '/header.php' ?>
<style>
    #main-content {
        padding: 0;
        margin: 0;
        margin-right: 10px;
    }
    #main {
        position: absolute;
        z-index: 1;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
        margin-left: 320px;
        background: #fff;
    }
</style>
<div id="main">
    <div id="main-content">
        <div class="row no-marg my_profile">

            <div class="col-xs-12 col-md-6 no-padd image_profile">
                <!--<div class="overfly"></div>-->	
            </div>

            <div class="col-xs-12 col-md-6 no-padd image_profile_resp">
                <div class="home_profile_resp"> 
                    <img src="http://placehold.it/800x991" alt=""> 
                </div>
            </div>

            <!-- .info_profile -->
            <div class="col-xs-12 col-md-6 no-padd info_profile">

                <!-- .bg_desc -->
                <div class="bg_desc clearfix">
                    <div class="inner-text">
                        <h2>I am John Doe</h2>
                        <small class="myjob">web developer</small>

                        <div class="feature-desc">
                            <p>My money’s in that office, right? If she start giving me some bullshit about it ain’t there, and we got to go someplace else and get it, I’m gonna shoot you in the head then and there. Then I’m gonna shoot that bitch in the kneecaps, find out where my goddamn money is. She gonna tell me too.</p>
                            <p>Quickly pursue revolutionary relationships before scalable technologies. Proactively pontificate out-of-the-box alignments with equity invested results.</p>
                            <p>Aliquam lobortis nec nibh a blandit. Quisque ultricies pharetra hendrerit. Nunc fringilla sapien eget viverra mollis.</p>
                        </div> 

                        <div class="signature text-right">
                            <div class="inner">
                                <img src="images/signature.png" alt="Signature">
                                <cite class="name">john doe</cite>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End .bg_desc -->
                <!-- .bg_info -->
                <div class="bg_info clearfix">
                    <div class="inner-text">
                        <ul>
                            <li class="user_name"><strong><i class="fa fa-user"></i>Name</strong> : john doe</li>
                            <li><strong><i class="fa fa-calendar"></i>Birthdate</strong> : March 18, 1988</li>
                            <li><strong><i class="fa fa-phone"></i>Phone</strong> : +123 (21) 1234-5678</li>
                            <li><strong><i class="glyphicon glyphicon-phone"></i>Mobile</strong> : +123 (21) 1234-5679</li>
                            <li><strong><i class="fa fa-skype"></i>Skype</strong> : John_Doe</li>
                            <li><strong><i class="fa fa-envelope"></i>Email</strong> : john.doe@gmail.com</li>
                            <li><strong><i class="fa fa-globe"></i>Website</strong> : <a href="#" target="_blank">www.anderson-smith.com</a></li>
                            <li><strong><i class="glyphicon glyphicon-map-marker"></i>Adresse</strong> : 1732 Monroe Street Houston, TX 77055.</li>
                        </ul>
                    </div>
                </div>
                <!-- End .bg_info -->
                <!-- .header_social_ul -->
                <div class="social-ul bg_social" id="profile_social_ul">
                    <ul>
                        <li class="social-twitter"><a href="#" target="_blank" title="" data-toggle="tooltip" data-original-title="Twitter"><i class="fa fa-twitter"></i></a></li>
                        <li class="social-facebook"><a href="#" target="_blank" title="" data-toggle="tooltip" data-original-title="Facebook"><i class="fa fa-facebook"></i></a></li>
                        <li class="social-google"><a href="#" target="_blank" title="" data-toggle="tooltip" data-original-title="Google Plus"><i class="fa fa-google-plus"></i></a></li>
                        <li class="social-linkedin"><a href="#" target="_blank" title="" data-toggle="tooltip" data-original-title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                        <li class="social-dribbble"><a href="#" target="_blank" title="" data-toggle="tooltip" data-original-title="Dribbble"><i class="fa fa-dribbble"></i></a></li>
                        <li class="social-behance"><a href="#" target="_blank" title="" data-toggle="tooltip" data-original-title="Behance"><i class="fa fa-behance"></i></a></li>
                    </ul>
                </div>
                <!-- End .header_social_ul -->
            </div>
            <!-- End .info_profile -->
        </div>
    </div>
</div>
<?php include_once __DIR__ . '/footer.php'; ?>