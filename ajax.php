<?php
if (isset($_GET['action']) && $_GET['action']) {
    extract($_GET);
    if ($action === 'get_more_followers') {
        for ($i = 0; $i < $limit; $i++) {
            ?>
            <!-- post -->
            <div class="col-md-3 col-sm-6">
                <article>
                    <div class="post-thumb">
                        <a href="blog_single.php" class="image-link"><img src="http://lorempixel.com/333/222/people" /></a>
                    </div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="blog_single.php">Follower <?php echo ($last_id + ($i + 1)) ?></a></h3>
                        <div class="post-meta" hidden="">
                            <ul>
                                <li><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></li>
                                <li class="separate_li">|</li>
                                <li><i class="icon-clock"></i>January 18, 2015</li>
                                <li class="separate_li">|</li>
                                <li><i class="icon-folder"></i><a href="#" rel="category tag">WordPress</a></li>
                            </ul>
                        </div>
                        <div class="post-content" hidden>
                            <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat. Mauris sit amet sapien eget lacus...</p>
                        </div>
                        <!--<a href="blog_single.php" class="read_more_but">Continue Reading</a>-->
                        <div class="footer_post" hidden>
                            <ul>
                                <li><i class="icon-picture"></i></li>
                                <li><i class="icon-bubble"></i> <a href="#">4</a></li>
                                <li><i class="icon-heart"></i> <a href="#">35</a></li>
                                <li><i class="icon-eye"></i> 216</li>
                            </ul>
                        </div>
                    </div>
                </article>
            </div>
            <!-- //post -->
            <?php
        }
    }
    if ($action === 'more_home_posts') {
        ?>
        <div class="item" data-type="text">
            <div class="row item_content">
                <div class="col-md-2" style="width:65px">
                    <img src="images/cover-3.jpg" class="img img-circle" style="height:35px"/>
                </div>
                <div class="col-md-8" style="margin-left: -20px;">
                    <div class="post_user_detail">
                        <span class="p_bold">John Doe</span>said<br/><span class="p_bold">15 mins ago</span>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="post_content">
                        Closed That Deal I've been waiting on..
                    </div>
                </div>
            </div>
        </div>
        <div class="item" data-type="media">
            <div class="row item_content">
                <div class="col-md-2" style="width:65px">
                    <img src="images/cover-1.jpg" class="img img-circle" style="height:35px"/>
                </div>
                <div class="col-md-8" style="margin-left: -20px;">
                    <i><b>John Doe</b></i>  <font style="color:#999">said<br/>15 mins ago</font>
                </div>
                <div class="col-md-12" style="padding: 0px;">
                    <div class="post_content" style="padding:5px">
                        Visit with sis and mon and dad..
                    </div>
                    <br/>
                    <div class="img-list">
                        <img class="img post_photo_img img_options post_video_size" src="images/cover-1.jpg" />
                        <span class="text-content">
                            <span>
                                <i class="fa fa-arrows-alt  img_options" src="images/cover-1.jpg" style="padding:10px"></i>
                                <i class="fa fa-edit  img_options" src="images/cover-1.jpg"></i>
                                <i class="fa fa-star  later" style="margin-top:-10px;margin-left: 18%;position: absolute;margin-top: -10px;">&nbsp;88</i>
                                <i class="fa fa-thumb-tack  later" src="images/cover-1.jpg" style="padding: 8%;margin-left: 11%;position: absolute;">&nbsp;77</i>
                                <i class="fa fa-arrow-down  later" src="images/cover-1.jpg" style="margin-top:10px;padding: 15%;margin-left: 3%;position: absolute;margin-top: 10px;">&nbsp;137</i>
                            </span>
                        </span>
                    </div>
                </div>
                <div class="col-md-6 p_padding_left">
                    <div class="photo_content">
                        <span class="p_bold">Wisterio</span><span class="p_bold"></span><br />@username
                    </div>
                </div>
                <div class="col-md-6 p_padding_right">
                    <span class="smiley">&#9786;</span>10 
                    <span class="smiley2">&#9785;</span>0 
                    <span class="smiley2">&#9825;</span>18
                </div>
                <div class="col-md-12 photo_comments">
                    <div class="col-md-2 comments_col_width">
                        <img src="images/cover-1.jpg" class="img img-circle" style="height:35px"/>
                    </div>
                    <div class="col-md-9" style="margin-left: -20px;">
                        <div class="pull-right"><i class="fa fa-comment"></i> 3</div>
                        <div class="post_user_detail">
                            <span class="p_bold">Brenda Smith</span><br/>
                            <span class="photo_content">Hey Aunt @Wis, I wish I could have been there</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item" data-type="text">
            <div class="row item_content">
                <div class="col-md-2" style="width:65px">
                    <img src="images/cover-3.jpg" class="img img-circle" style="height:35px"/>
                </div>
                <div class="col-md-8" style="margin-left: -20px;">
                    <div class="post_user_detail">
                        <span class="p_bold">John Doe</span>  said<br/><span class="p_bold">15 mins ago</span>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="post_content">
                        Closed That Deal I've been waiting on..
                    </div>
                </div>
            </div>
        </div>
        <div class="item" data-type="media">
            <div class="row item_content">
                <div style="padding:5px;">
                    <div class="col-md-2" style="width:65px">
                        <img src="images/cover-3.jpg" class="img img-circle" style="height:35px"/>
                    </div>
                    <div class="col-md-8" style="margin-left: -20px;">
                        <i><b>John Doe</b></i>  <font style="color:#999">said<br/>15 mins ago</font>
                    </div>
                </div>
                <div class="col-md-12" style="padding:0px;">
                    <div class="post_content" style="padding:5px">
                        Visit with sis and mon and dad..
                    </div>
                    <br/>
                    <div class="img-list">
                        <img class="video_options post_video_size" data-video="E6KwXYmMiak" src="images/E6KwXYmMiak-play.jpg" />
                        <span class="text-content">
                            <span>
                                <i class="fa fa-arrows-alt  video_options" data-video="E6KwXYmMiak" src="images/E6KwXYmMiak-play.jpg" style="padding:10px"></i>
                                <i class="fa fa-edit video_options" src="images/cover-1.jpg"></i>
                                <i class="fa fa-star  later" style="margin-top:-10px;margin-left: 18%;position: absolute;margin-top: -10px;">&nbsp;88</i>
                                <i class="fa fa-thumb-tack  later" src="images/cover-1.jpg" style="padding: 8%;margin-left: 11%;position: absolute;">&nbsp;77</i>
                                <i class="fa fa-arrow-down  later" src="images/cover-1.jpg" style="margin-top:10px;padding: 15%;margin-left: 3%;position: absolute;margin-top: 10px;">&nbsp;137</i>
                            </span>
                        </span>

                    </div>
                </div>
                <div class="col-md-6 p_padding_left">
                    <div class="photo_content">
                        <span class="p_bold">Wisterio</span><span class="p_bold"></span><br />@username
                    </div>
                </div>
                <div class="col-md-6 p_padding_right">
                    <span class="smiley">&#9786;</span>10 
                    <span class="smiley2">&#9785;</span>0 
                    <span class="smiley2">&#9825;</span>18
                </div>
                <div class="col-md-12 photo_comments">
                    <div class="col-md-2 comments_col_width">
                        <img src="images/cover-3.jpg" class="img img-circle" style="height:35px"/>
                    </div>
                    <div class="col-md-9" style="margin-left: -20px;">
                        <div class="pull-right"><i class="fa fa-comment"></i> 3</div>
                        <div class="post_user_detail">
                            <span class="p_bold">Ivan Moore</span><br/>
                            <span class="photo_content">Hey Aunt @Wis, I wish I could have been there</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item" data-type="text">
            <div class="row item_content">
                <div class="col-md-2" style="width:65px">
                    <img src="images/cover-3.jpg" class="img img-circle" style="height:35px"/>
                </div>
                <div class="col-md-8" style="margin-left: -20px;">
                    <div class="post_user_detail">
                        <span class="p_bold">John Doe</span>said<br/><span class="p_bold">15 mins ago</span>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="post_content">
                        Closed That Deal I've been waiting on..
                    </div>
                </div>
            </div>
        </div>
        <div class="item" data-type="media">
            <div class="row item_content">
                <div style="padding:5px">
                    <div class="col-md-2" style="width:65px">
                        <img src="images/cover-3.jpg" class="img img-circle" style="height:35px"/>
                    </div>
                    <div class="col-md-8" style="margin-left: -20px;">
                        <i><b>John Doe</b></i>  <font style="color:#999">said<br/>15 mins ago</font>
                    </div>
                </div>
                <div class="col-md-12" style="padding:0px">
                    <div class="post_content" style="padding:5px">
                        Visit with sis and mon and dad..
                    </div>
                    <br/>
                    <div class="img-list">
                        <img class="video_options post_video_size" data-video="OargwriB8ns" src="images/OargwriB8ns-play.jpg" />
                        <span class="text-content">
                            <span>
                                <i class="fa fa-arrows-alt  video_options" data-video="OargwriB8ns" src="images/OargwriB8ns-play.jpg" style="padding:10px"></i>
                                <i class="fa fa-edit video_options" src="images/cover-1.jpg"></i>
                                <i class="fa fa-star  later" style="margin-top:-10px;margin-left: 18%;position: absolute;margin-top: -10px;">&nbsp;88</i>
                                <i class="fa fa-thumb-tack  later" src="images/cover-1.jpg" style="padding: 8%;margin-left: 11%;position: absolute;">&nbsp;77</i>
                                <i class="fa fa-arrow-down  later" src="images/cover-1.jpg" style="margin-top:10px;padding: 15%;margin-left: 3%;position: absolute;margin-top: 10px;">&nbsp;137</i>
                            </span>
                        </span>
                    </div>
                </div>
                <div class="col-md-6 p_padding_left">
                    <div class="photo_content">
                        <span class="p_bold">Wisterio</span><span class="p_bold"></span><br />@username
                    </div>
                </div>
                <div class="col-md-6 p_padding_right">
                    <span class="smiley">&#9786;</span>10 
                    <span class="smiley2">&#9785;</span>0 
                    <span class="smiley2">&#9825;</span>18
                </div>
                <div class="col-md-12 photo_comments">
                    <div class="col-md-2 comments_col_width">
                        <img src="images/cover-3.jpg" class="img img-circle" style="height:35px"/>
                    </div>
                    <div class="col-md-9" style="margin-left: -20px;">
                        <div class="pull-right"><i class="fa fa-comment"></i> 3</div>
                        <div class="post_user_detail">
                            <span class="p_bold">Ivan Moore</span><br/>
                            <span class="photo_content">Hey Aunt @Wis, I wish I could have been there</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item" data-type="text">
            <div class="row item_content">
                <div class="col-md-2" style="width:65px">
                    <img src="images/cover-3.jpg" class="img img-circle" style="height:35px"/>
                </div>
                <div class="col-md-8" style="margin-left: -20px;">
                    <div class="post_user_detail">
                        <span class="p_bold">John Doe</span>  said<br/><span class="p_bold">15 mins ago</span>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="post_content">
                        Closed That Deal I've been waiting on..
                    </div>
                </div>
            </div>
        </div>
        <div class="item" data-type="media">
            <div class="row item_content">
                <div style="padding:5px;">
                    <div class="col-md-2" style="width:65px">
                        <img src="images/cover-3.jpg" class="img img-circle" style="height:35px"/>
                    </div>
                    <div class="col-md-8" style="margin-left: -20px;">
                        <i><b>John Doe</b></i>  <font style="color:#999">said<br/>15 mins ago</font>
                    </div>
                </div>
                <div class="col-md-12" style="padding:0px">
                    <div class="post_content" style="padding:5px">
                        Visit with sis and mon and dad..
                    </div>
                    <br/>
                    <div class="img-list">
                        <img class="img post_photo_img img_options post_video_size" src="images/cover-3.jpg" />
                        <span class="text-content">
                            <span>
                                <i class="fa fa-arrows-alt  img_options" src="images/cover-3.jpg" style="padding:10px"></i>
                                <i class="fa fa-edit  img_options" src="images/cover-3.jpg"></i>
                                <i class="fa fa-star  later" style="margin-top:-10px;margin-left: 18%;position: absolute;margin-top: -10px;">&nbsp;88</i>
                                <i class="fa fa-thumb-tack  later" src="images/cover-1.jpg" style="padding: 8%;margin-left: 11%;position: absolute;">&nbsp;77</i>
                                <i class="fa fa-arrow-down  later" src="images/cover-1.jpg" style="margin-top:10px;padding: 15%;margin-left: 3%;position: absolute;margin-top: 10px;">&nbsp;137</i>
                            </span>
                        </span>
                    </div>
                </div>
                <div class="col-md-6 p_padding_left">
                    <div class="photo_content">
                        <span class="p_bold">Wisterio</span><span class="p_bold"></span><br />@username
                    </div>
                </div>
                <div class="col-md-6 p_padding_right">
                    <span class="smiley">&#9786;</span>10 
                    <span class="smiley2">&#9785;</span>0 
                    <span class="smiley2">&#9825;</span>18
                </div>
                <div class="col-md-12 photo_comments">
                    <div class="col-md-2 comments_col_width">
                        <img src="images/cover-3.jpg" class="img img-circle" style="height:35px"/>
                    </div>
                    <div class="col-md-9" style="margin-left: -20px;">
                        <div class="pull-right"><i class="fa fa-comment"></i> 3</div>
                        <div class="post_user_detail">
                            <span class="p_bold">Ivan Moore</span><br/>
                            <span class="photo_content">Hey Aunt @Wis, I wish I could have been there</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item" data-type="text">
            <div class="row item_content">
                <div class="col-md-2" style="width:65px">
                    <img src="images/cover-3.jpg" class="img img-circle" style="height:35px"/>
                </div>
                <div class="col-md-8" style="margin-left: -20px;">
                    <div class="post_user_detail">
                        <span class="p_bold">John Doe</span>  said<br/><span class="p_bold">15 mins ago</span>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="post_content">
                        Closed That Deal I've been waiting on..
                    </div>
                </div>
            </div>
        </div>
        <div class="item" data-type="media">
            <div class="row item_content">
                <div style="padding:5px;">
                    <div class="col-md-2" style="width:65px">
                        <img src="images/cover-4.jpg" class="img img-circle" style="height:35px"/>
                    </div>
                    <div class="col-md-8" style="margin-left: -20px;">
                        <i><b>John Doe</b></i>  <font style="color:#999">said<br/>15 mins ago</font>
                    </div>
                </div>
                <div class="col-md-12" style="padding:0px">
                    <div class="post_content" style="padding:5px">
                        Visit with sis and mon and dad..
                    </div>
                    <br/>
                    <div class="img-list">
                        <img class="img post_photo_img img_options post_video_size" src="images/cover-4.jpg"  style="height:131px" />
                        <span class="text-content">
                            <span>
                                <i class="fa fa-arrows-alt  img_options" src="images/cover-4.jpg" style="padding:10px"></i>
                                <i class="fa fa-edit  img_options" src="images/cover-4.jpg"></i>
                                <i class="fa fa-star  later" style="margin-top:-10px;margin-left: 18%;position: absolute;margin-top: -10px;">&nbsp;88</i>
                                <i class="fa fa-thumb-tack  later" src="images/cover-1.jpg" style="padding: 8%;margin-left: 11%;position: absolute;">&nbsp;77</i>
                                <i class="fa fa-arrow-down  later" src="images/cover-1.jpg" style="margin-top:10px;padding: 15%;margin-left: 3%;position: absolute;margin-top: 10px;">&nbsp;137</i>
                            </span>
                        </span>
                    </div>
                </div>
                <div class="col-md-6 p_padding_left">
                    <div class="photo_content">
                        <span class="p_bold">Wisterio</span><span class="p_bold"></span><br />@username
                    </div>
                </div>
                <div class="col-md-6 p_padding_right">
                    <span class="smiley">&#9786;</span>10 
                    <span class="smiley2">&#9785;</span>0 
                    <span class="smiley2">&#9825;</span>18
                </div>
                <div class="col-md-12 photo_comments">
                    <div class="col-md-2 comments_col_width">
                        <img src="images/cover-4.jpg" class="img img-circle" style="height:35px"/>
                    </div>
                    <div class="col-md-9" style="margin-left: -20px;">
                        <div class="pull-right"><i class="fa fa-comment"></i> 3</div>
                        <div class="post_user_detail">
                            <span class="p_bold">Brenda Smith</span><br/>
                            <span class="photo_content">Hey Aunt @Wis, I wish I could have been there</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item" data-type="media">
            <div class="row item_content">
                <div style="padding:5px">
                    <div class="col-md-2" style="width:65px">
                        <img src="images/cover-3.jpg" class="img img-circle" style="height:35px"/>
                    </div>
                    <div class="col-md-8" style="margin-left: -20px;">
                        <i><b>John Doe</b></i>  <font style="color:#999">said<br/>15 mins ago</font>
                    </div>
                </div>
                <div class="col-md-12" style="padding:0px">
                    <div class="post_content" style="padding:5px">
                        Visit with sis and mon and dad..
                    </div>
                    <br/>
                    <div class="img-list">
                        <img class="video_options post_video_size" data-video="XZ4X1wcZ1GE" src="images/XZ4X1wcZ1GE-play.jpg" />
                        <span class="text-content">
                            <span>
                                <i class="fa fa-arrows-alt  video_options"  data-video="XZ4X1wcZ1GE" src="images/XZ4X1wcZ1GE-play.jpg" style="padding:10px"></i>
                                <i class="fa fa-edit video_options" src="images/cover-1.jpg"></i>
                                <i class="fa fa-star  later" style="margin-top:-10px;margin-left: 18%;position: absolute;margin-top: -10px;">&nbsp;88</i>
                                <i class="fa fa-thumb-tack  later" src="images/cover-1.jpg" style="padding: 8%;margin-left: 11%;position: absolute;">&nbsp;77</i>
                                <i class="fa fa-arrow-down  later" src="images/cover-1.jpg" style="margin-top:10px;padding: 15%;margin-left: 3%;position: absolute;margin-top: 10px;">&nbsp;137</i>
                            </span>
                        </span>
                    </div>
                </div>
                <div class="col-md-6 p_padding_left">
                    <div class="photo_content">
                        <span class="p_bold">Wisterio</span><span class="p_bold"></span><br />@username
                    </div>
                </div>
                <div class="col-md-6 p_padding_right">
                    <span class="smiley">&#9786;</span>10 
                    <span class="smiley2">&#9785;</span>0 
                    <span class="smiley2">&#9825;</span>18
                </div>
                <div class="col-md-12 photo_comments">
                    <div class="col-md-2 comments_col_width">
                        <img src="images/cover-3.jpg" class="img img-circle" style="height:35px"/>
                    </div>
                    <div class="col-md-9" style="margin-left: -20px;">
                        <div class="pull-right"><i class="fa fa-comment"></i> 3</div>
                        <div class="post_user_detail">
                            <span class="p_bold">Ivan Moore</span><br/>
                            <span class="photo_content">Hey Aunt @Wis, I wish I could have been there</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}
if (isset($_POST['action']) && $_POST['action']) {
    extract($_POST);
    if ($action == 'get_post_text_html') {
        ?>
        <div class="row-fluid">
            <form class="form-inline">
                <div class="col-md-12" style="margin-top:10px">
                    <div class="form-group">
                        <div class="input-group">
                            <textarea class="form-control border-radius border-radius" rows="2" placeholder="Share something.."></textarea>
                            <div style="cursor:pointer" class="input-group-addon border-radius black-green-button colors-color" id="get_location_html"><i class="fa fa-map-marker"></i></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div id="location_input" style="display: none">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon border-radius black-green-button colors-color colors-color"><i class="fa fa-map-marker"></i></div>
                                <input id="pac-input" placeholder="Type Location" type="text" class="form-control border-radius" />
                                <div class="input-group-addon border-radius black-green-button colors-color">Find</div>
                            </div>
                        </div>
                        <div id="location_html">
                            <center><img src="images/splash-screen.png"/></center>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div class="form-group">
                        <label class="control-label">Privacy</label>
                        <div class="input-group">
                            <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-share-alt"></i></div>
                            <select class="chosen form-control border-radius" multiple="true" data-placeholder="Choose Circle" style="width: 100% !important">
                                <option>Public</option>
                                <option>Friends</option>
                                <option>Private</option>
                                <option>Protected</option>
                                <option>Other</option>
                            </select>
                            <div class="input-group-addon border-radius border-radius black-green-button colors-color">POST</div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <?php
    }
    if ($action == 'get_post_photo_html') {
        ?>
        <div class="row-fluid">
            <form class="form-inline">
                <div class="col-md-12">
                    <center>
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"></div>
                            <div>
                                <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="..."></span>
                                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>
                    </center>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div class="form-group">
                        <label class="control-label">Add Photo To Collection</label>
                        <div class="input-group">
                            <input placeholder="Enter Title" type="text" class="form-control border-radius" />
                            <div class="input-group-addon border-radius black-green-button colors-color">Create Collection</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div class="form-group">
                        <div class="input-group">
                            <textarea class="form-control border-radius" rows="2" placeholder="Share something.."></textarea>
                            <div style="cursor:pointer" class="input-group-addon border-radius black-green-button colors-color" id="get_location_html"><i class="fa fa-map-marker"></i></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div id="location_input" style="display: none">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-map-marker"></i></div>
                                <input id="pac-input" placeholder="Type Location" type="text" class="form-control border-radius" />
                                <div class="input-group-addon border-radius black-green-button colors-color">Find</div>
                            </div>
                        </div>
                        <div id="location_html">
                            <center><img src="images/splash-screen.png"/></center>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div class="form-group">
                        <label class="control-label">Privacy</label>
                        <div class="input-group">
                            <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-share-alt"></i></div>
                            <select class="chosen form-control border-radius" multiple="true" data-placeholder="Choose Circle" style="width: 100% !important">
                                <option>Public</option>
                                <option>Friends</option>
                            </select>
                            <div class="input-group-addon border-radius black-green-button colors-color">POST</div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <?php
    }
    if ($action == 'get_post_link_html') {
        ?>
        <div class="row-fluid">
            <form class="form-inline">
                <div class="col-md-12" style="margin-top:10px">
                    <div class="form-group">
                        <label class="control-label">Grab Link Info</label>
                        <div class="input-group">
                            <input id="url" placeholder="Enter URL" type="text" class="form-control border-radius" />
                            <div id="get_url_info" class="input-group-addon border-radius black-green-button colors-color">Fetch</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div id="output"></div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div class="form-group">
                        <div class="input-group">
                            <textarea class="form-control border-radius" rows="2" placeholder="Share something.."></textarea>
                            <div style="cursor:pointer" class="input-group-addon border-radius black-green-button colors-color" id="get_location_html"><i class="fa fa-map-marker"></i></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div id="location_input" style="display: none">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon border-radius  black-green-button colors-color"><i class="fa fa-map-marker"></i></div>
                                <input id="pac-input" placeholder="Type Location" type="text" class="form-control border-radius" />
                                <div class="input-group-addon border-radius black-green-button colors-color">Find</div>
                            </div>
                        </div>
                        <div id="location_html">
                            <center><img src="images/splash-screen.png"/></center>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div class="form-group">
                        <label class="control-label">Privacy</label>
                        <div class="input-group">
                            <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-share-alt"></i></div>
                            <select class="chosen form-control border-radius" multiple="true" data-placeholder="Choose Circle" style="width: 100% !important">
                                <option>Public</option>
                                <option>Friends</option>
                            </select>
                            <div class="input-group-addon border-radius  black-green-button colors-color">POST</div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <?php
    }
    if ($action == 'get_post_audio_html') {
        ?>
        <div class="row-fluid">
            <div class="col-md-12" style="margin-top:10px">
                <div class="form-group">
                    <label class="control-label">Enter Track ID</label>
                    <div class="input-group">
                        <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-map-marker"></i></div>
                        <input type="text" class="form-control border-radius" id="audioUrl" name="audioUrl" placeholder="Eg. 101276036" value="101276036"/>
                        <div id="embed_soundcloud_audio" class="input-group-addon border-radius  black-green-button colors-color">Go</div>
                    </div>
                </div>
            </div>
            <div id="col-md-12" style="margin-top:10px">
                <div id="soundcloud_frame"></div>
            </div>
            <form>
                <div class="col-md-12" style="margin-top:10px">

                    <div class="form-group">
                        <div class="input-group">
                            <textarea class="form-control border-radius" rows="2" placeholder="Share something.."></textarea>                         
                            <div style="cursor:pointer" class="input-group-addon border-radius  black-green-button colors-color" id="get_location_html"><i class="fa fa-map-marker"></i></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div id="location_input" style="display: none">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon border-radius  black-green-button colors-color"><i class="fa fa-map-marker"></i></div>
                                <input id="pac-input" placeholder="Type Location" type="text" class="form-control border-radius" />
                                <div class="input-group-addon border-radius  black-green-button colors-color">Find</div>
                            </div>
                        </div>
                        <div id="location_html">
                            <center><img src="images/splash-screen.png"/></center>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div class="form-group">
                        <label class="control-label">Privacy</label>
                        <div class="input-group">
                            <div class="input-group-addon border-radius  black-green-button colors-color"><i class="fa fa-share-alt"></i></div>
                            <select class="chosen form-control border-radius" multiple="true" data-placeholder="Choose Circle" style="width: 100% !important">
                                <option>Public</option>
                                <option>Friends</option>
                            </select>
                            <div class="input-group-addon border-radius black-green-button colors-color">Post</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <label name="select_collection">Add Audio To Collection</label>
                    <div class="form-group">
                        <select class="form-control border-radius">
                            <option>Collection Name</option>
                        </select>
                    </div>
                </div>
            </form>
        </div>
        <?php
    }
    if ($action == 'get_post_video_html') {
        ?>
        <div class="row-fluid">
            <div class="col-md-12" style="margin-top:10px">
                <div class="form-group">
                    <label class="control-label">Enter Track ID</label>
                    <div class="input-group">
                        <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-map-marker"></i></div>
                        <input value="https://www.youtube.com/embed/E6KwXYmMiak" type="text" class="form-control border-radius" id="videoUrl" name="videoUrl" placeholder="Youtube Video URL"/>
                        <div id="embed_youtube_video" class="input-group-addon border-radius  black-green-button colors-color">Go</div>
                    </div>
                </div>
            </div>
            <div id="col-md-12" style="margin-top:10px">
                <div id="youtube_frame"></div>
            </div>
            <form>
                <div class="col-md-12" style="margin-top:10px">
                    <div class="form-group">
                        <div class="input-group">
                            <textarea class="form-control border-radius" rows="2" placeholder="Share something.."></textarea>
                            <div style="cursor:pointer" class="input-group-addon border-radius  black-green-button colors-color" id="get_location_html"><i class="fa fa-map-marker"></i></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div id="location_input" style="display: none">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-map-marker"></i></div>
                                <input id="pac-input" placeholder="Type Location" type="text" class="form-control border-radius" />
                                <div class="input-group-addon border-radius  black-green-button colors-color">Find</div>
                            </div>
                        </div>
                        <div id="location_html">
                            <center><img src="images/splash-screen.png"/></center>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div class="form-group">
                        <label class="control-label">Privacy</label>
                        <div class="input-group">
                            <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-share-alt"></i></div>
                            <select class="chosen form-control border-radius" multiple="true" data-placeholder="Choose Circle" style="width: 100% !important">
                                <option>Public</option>
                                <option>Friends</option>
                            </select>
                            <div class="input-group-addon border-radius black-green-button colors-color">POST</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <label name="select_collection">Add Video To Collection</label>
                    <div class="form-group">
                        <select class="form-control border-radius">
                            <option>Collection Name</option>
                        </select>
                    </div>
                </div>
            </form>
        </div>
        <?php
    }

    if ($action == 'get_post_event_html') {
        ?>
        <div class="col-md-12" style="margin-top:10px">
            <div class="form-group">
                <input placeholder="Enter Event Title" type="text" class="form-control border-radius" />
            </div>
            <div class="form-group">
                <label class="control-label">Enter Event Date</label>
                <input type="date" class="form-control border-radius" />
            </div>
        </div>
        <div class="col-md-12" style="margin-top:10px">
            <div class="form-group">
                <div class="input-group">
                    <textarea class="form-control border-radius" rows="2" placeholder="Share something.."></textarea>
                    <div style="cursor:pointer" class="input-group-addon border-radius black-green-button colors-color" id="get_location_html"><i class="fa fa-map-marker"></i></div>
                </div>
            </div>
        </div>
        <div class="col-md-12" style="margin-top:10px">
            <div id="location_input" style="display: none">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-map-marker"></i></div>
                        <input id="pac-input" placeholder="Type Location" type="text" class="form-control border-radius" />
                        <div class="input-group-addon border-radius black-green-button colors-color">Find</div>
                    </div>
                </div>
                <div id="location_html">
                    <center><img src="images/splash-screen.png"/></center>
                </div><br />
            </div>
            <div class="form-group">
                <textarea placeholder="Enter Event Description" type="text" class="form-control border-radius" /></textarea>
            </div>
            <label name="select_collection">Add Event To Collection</label>
            <div class="form-group">
                <select class="form-control border-radius">
                    <option>Collection Name</option>
                </select>
            </div>
        </div>

    <?php } ?>
    <?php if ($action == 'get_post_collection_html') { ?>

        <?php
    }
}
?>