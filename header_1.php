<?php

function pr($e) {
    echo "<pre>";
    print_r($e);
    echo "</pre>";
}

$f_name = basename($_SERVER['SCRIPT_FILENAME']);
// Get Current FileName
$file = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);

if (!isset($show_quick_icons))
    $show_quick_icons = TRUE;
if (!isset($show_top_menu))
    $show_top_menu = TRUE;
if (!isset($menus)) {
    $menus = array(
        'index.php' => array(
            'icon' => 'home',
            'label' => 'Home',
        ),
        'index-2.php' => array(
            'icon' => 'home',
            'label' => 'Home 2',
        ),
        'profile_wizard.php' => array(
            'icon' => 'user',
            'label' => 'My Profile',
        ),
        'resume.php' => array(
            'icon' => 'tasks',
            'label' => 'Resume',
        ),
        'blog.php' => array(
            'icon' => 'comments',
            'label' => 'Blog',
        ),
        'interests.php' => array(
            'icon' => 'user',
            'label' => 'Interests',
        ),
        'groups_manage.php' => array(
            'icon' => 'users',
            'label' => 'Groups',
        ),
        'company_pages.php' => array(
            'icon' => 'building',
            'label' => 'Pages',
        ),
        'events.php' => array(
            'icon' => 'calendar',
            'label' => 'Events',
        ),
        'friends.php' => array(
            'icon' => 'users',
            'label' => 'Friends',
        ),
        'following.php' => array(
            'icon' => 'user',
            'label' => 'Following',
        ),
        'followers.php' => array(
            'icon' => 'user',
            'label' => 'Followers',
        ),
        'company.php' => array(
            'icon' => 'building',
            'label' => 'Company',
        ),
    );
}
?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
    <head>
        <title><?php echo isset($title) ? $title . ' - ' : '' ?>Ingynius</title>
        <!-- Basic Page Needs -->
        <meta charset="UTF-8" />
        <meta name="description" content="FlexyCodes - FlexyBlog Personal Blog Template. Creating my personal page!"/>
        <meta name="keywords" content="">
        <meta name="author" content="flexycodes">

        <!-- Mobile Specific Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- CSS | bootstrap -->
        <!-- Credits: http://getbootstrap.com/ -->
        <link  rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
        <link  rel="stylesheet" type="text/css" href="css/toastr.css" />

        <!-- CSS | font-awesome -->
        <!-- Credits: http://graphicburger.com/down/?q=simple-line-icons-webfont -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />

        <!-- CSS | simple-line-icons -->
        <!-- Credits: https://github.com/thesabbir/simple-line-icons -->
        <link rel="stylesheet" type="text/css" href="css/simple-line-icons.min.css">

        <!-- CSS | animate -->
        <!-- Credits: http://daneden.me/animate/ -->
        <link rel="stylesheet" type="text/css" href="css/animate.min.css" />

        <!-- CSS | flexslider -->
        <!-- Credits: http://www.woothemes.com/flexslider/ -->
        <link rel="stylesheet" type="text/css" href="css/flexslider.min.css" />

        <!-- CSS | prettyPhoto -->
        <!-- Credits: http://www.no-margin-for-errors.com/ -->
        <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css"/>

        <!-- CSS | Colors -->
        <link rel="stylesheet" type="text/css" href="css/switcher.css" />
        <link rel="stylesheet" type="text/css" href="plugins/file-upload/css/jquery.fileupload.css" />
        <link rel="stylesheet" type="text/css" href="css/colors/color_1.css" id="colors-style" />

        <!-- CSS | Style -->
        <!-- Credits: http://themeforest.net/user/FlexyCodes -->
        <link rel="stylesheet" type="text/css" href="css/main.css" />

        <!-- CSS | Google Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,700,300italic,400italic,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>	
        <link href='http://fonts.googleapis.com/css?family=Russo+One' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700' rel='stylesheet' type='text/css'>
        <link href='plugins/date-picker/datetime-picker.css' rel='stylesheet' type='text/css'>
        <link href='plugins/date-picker/datetime-picker.css' rel='stylesheet' type='text/css'>
        <link href='plugins/slider/slider.css' rel='stylesheet' type='text/css'>
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon/favicon.ico">

        <!--[if IE 7]>
                        <link rel="stylesheet" type="text/css" href="css/icons/font-awesome-ie7.min.css"/>
        <![endif]-->

        <!-- jquery | jQuery 1.11.0 -->
        <!-- Credits: http://jquery.com -->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <!-- Js | jquery.ui.effect.js -->

        <!-- Credits: https://jqueryui.com/effect-->
        <script type="text/javascript" src="js/jquery.ui.effect.min.js"></script>

        <!-- Js | bootstrap -->
        <!-- Credits: http://getbootstrap.com/ -->
        <script type="text/javascript" src="js/bootstrap.min.js"></script> 

        <!-- Js | masonry.pkgd.min.js -->
        <!-- Credits: http://masonry.desandro.com -->
        <script type="text/javascript" src="js/masonry.pkgd.min.js"></script>

        <!-- Js | flexslider -->
        <!-- Credits: http://www.woothemes.com/flexslider/ -->
        <script type="text/javascript" src="js/jquery.flexslider-min.js"></script>

        <!-- Js | jquery.cookie -->
        <!-- Credits: https://github.com/carhartl/jquery-cookie --> 
        <script type="text/javascript" src="js/jsSwitcher/jquery.cookie.js"></script>	

        <!-- Js | switcher -->
        <!-- Credits: http://themeforest.net/user/FlexyCodes -->
        <script type="text/javascript" src="js/jsSwitcher/switcher.js"></script>	

        <!-- Js | nicescroll.js -->
        <!-- Credits: http://areaaperta.com/nicescroll/ -->
        <script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>

        <!-- jquery | rotate and portfolio -->
        <!-- Credits: http://jquery.com -->
        <script type="text/javascript" src="js/jquery.mixitup.min.js"></script> 

        <!-- jquery | prettyPhoto -->
        <!-- Credits: http://www.no-margin-for-errors.com/ -->
        <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

        <!-- Js | gmaps -->
        <!-- Credits: http://maps.google.com/maps/api/js?sensor=true-->
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script type="text/javascript" src="js/gmaps.min.js"></script>

        <!-- Js | Js -->
        <!-- Credits: http://themeforest.net/user/FlexyCodes -->
        <script type="text/javascript" src="js/jquery.localStorage.js"></script>
        <script type="text/javascript" src="js/main.js"></script>
        <script type="text/javascript" src="js/toastr.js"></script>
        <script type="text/javascript" src="js/user_posts.js"></script>
        <script type="text/javascript" src="plugins/file-upload/js/vendor/jquery.ui.widget.js"></script>
        <script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
        <!-- The Canvas to Blob plugin is included for image resizing functionality -->
        <script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
        <!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="plugins/file-upload/js/jquery.iframe-transport.js"></script>
        <script type="text/javascript" src="plugins/file-upload/js/jquery.fileupload.js"></script>
        <script type="text/javascript" src="plugins/file-upload/js/jquery.fileupload-process.js"></script>
        <script type="text/javascript" src="plugins/file-upload/js/jquery.fileupload-image.js"></script>
        <script type="text/javascript" src="plugins/file-upload/js/jquery.fileupload-audio.js"></script>
        <script type="text/javascript" src="plugins/file-upload/js/jquery.fileupload-video.js"></script>
        <script type="text/javascript" src="plugins/file-upload/js/jquery.fileupload-validate.js"></script>
        <script type="text/javascript" src="plugins/date-picker/moment.js"></script>
        <script type="text/javascript" src="plugins/date-picker/datetime-picker.js"></script>
        <script type="text/javascript" src="plugins/slider/slider.js"></script>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker12').datetimepicker({
                    inline: true,
                    sideBySide: false
                });
            });
        </script>
        <style>
            body{	
                background: #F1F1F1;
            }

            .main-pagination {
                overflow: hidden;
                text-align: center;
                margin: 30px 0 30px 0;
                padding-right: 10px;
            }

            .masonry_load_more {
                padding: 5px 0;
                cursor: pointer;
                border: dotted 1px grey;
                border-radius: 5px;
            }

            .masonry_load_more:hover {
                background: #fff;
                color: #010101;
            }
            .latest_img_slider {
                width: 100% !important;
                height: 200px !important;
            }
            .events_img {
                width: 100% !important;
                height: 50px !important;
            }
            .post_title_cl {
                color: #9CD759;
                text-align: center;
                font-weight: bold;
            }
        </style>


    </head>

    <body>

        <!--[if lt IE 7]>
                        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->


        <!-- Laoding page  -->
        <div class="preloader">
            <div class="spinner">
                <div class="dot1"></div>
                <div class="dot2"></div>
            </div>
        </div>
        <!-- End Laoding page  -->
        <!-- .wrapper-content-->
        <div class="sidebar-toggle toggle_profile<?php echo isset($_COOKIE['menu_layout']) && $_COOKIE['menu_layout'] ? ' inverse' : '' ?>" id="profile-panel-toggle" title="Toggle Profile Panel" data-toggle="tooltip" style="opacity: 0.3"><i class="fa fa-chevron-left" id="sidebar_toggle_btn_profile"></i></div>
        <div class="wrapper-content">
            <!-- header -->
            <!-- #header -->
            <header id="header" class="header_1 <?php echo isset($pic_type) && $pic_type == 'square' ? 'content_2' : 'content_3' ?>">
                <div class="logo <?php echo isset($pic_type) && $pic_type == 'square' ? 'logo-square' : 'logo-circle' ?>">
                    <a href="#"><img src="http://lorempixel.com/<?php echo isset($pic_type) && $pic_type == 'square' ? '900/600' : '300/300' ?>/people" alt="Logo"></a>
                </div> <!--/ .logo -->

                <h4 class="tagline">This is a lovely area for the tagline</h4>
                <?php if ($show_quick_icons === TRUE) { ?>
                    <div class="row profile-links">
                        <div class="col-md-4 profile-link">
                            <a href="" title="Message"><i class="fa fa-2x fa-envelope"></i></a>
                        </div>
                        <div class="col-md-4 profile-link">
                            <a href="" title="Follow"><i class="fa fa-2x fa-users"></i></a>
                        </div>
                        <div class="col-md-4 profile-link">
                            <a href="" title="Video Chat"><i class="fa fa-2x fa-video-camera"></i></a>
                        </div>
                    </div>
                <?php } ?>


                <ul id="menu-main" class="fb_navigation vintage">

                    <li class="menu-item has-sub" hidden=""><a href="#">Home<i class="fa fa-angle-down icon_arrows"></i></a>
                        <ul class="sub-menu">
                            <li class="menu-item"><a href="index.php">Style 1</a></li>
                            <li class="menu-item"><a href="index-2.php">Style 2</a></li>
                        </ul>
                    </li>

                    <?php foreach ($menus as $link => $menu) { ?>
                        <?php
                        $active = ($link == $f_name ? ' colors-color' : '');
                        ?>
                        <li class="menu-item"><a class="colors-color-hover<?php echo $active; ?>" href="<?php echo $link ?>"><i class="fa fa-<?php echo $menu['icon'] ?> icon_menus"></i> <?php echo $menu['label'] ?></a></li>
                    <?php } ?>
                </ul>
                <div class="row">
                    <div class="col-md-12">
                        <h5 class="post_title_cl">
                            ONLINE
                        </h5>
                    </div>
                    <div class="col-md-12" style="margin-bottom: 10px">
                        <!-- Nav tabs -->
                        <div class="card" style="box-shadow: none">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#online_newest" aria-controls="home" role="tab" data-toggle="tab">Newest</a></li>
                                <li role="presentation"><a href="#online_featured" aria-controls="profile" role="tab" data-toggle="tab">Featured</a></li>
                                <li role="presentation"><a href="#online_active" aria-controls="messages" role="tab" data-toggle="tab">Active</a></li>
                                <li role="presentation"><a href="#online_all" aria-controls="settings" role="tab" data-toggle="tab">See All</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="online_newest">
                                    <div class="row-fluid">
                                        <?php for ($i = 0; $i < 3; $i++) { ?>
                                            <div class="col-md-4" style="margin-bottom: 10px">
                                                <img style="width: 45px;height: 45px" src="http://lorempixel.com/333/222/" class="img img-responsive img-circle"/>
                                            </div>
                                            <div class="col-md-4" style="margin-bottom: 10px">
                                                <img style="width: 45px;height: 45px" src="http://lorempixel.com/333/300/people" class="img img-responsive img-circle"/>
                                            </div>
                                            <div class="col-md-4" style="margin-bottom: 10px">
                                                <img style="width: 45px;height: 45px" src="http://lorempixel.com/333/222/people" class="img img-responsive img-circle"/>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="online_featured">
                                    <div class="row-fluid">
                                        <?php for ($i = 0; $i < 3; $i++) { ?>
                                            <div class="col-md-4" style="margin-bottom: 10px">
                                                <img style="width: 45px;height: 45px" src="http://lorempixel.com/333/222/" class="img img-responsive img-circle"/>
                                            </div>
                                            <div class="col-md-4" style="margin-bottom: 10px">
                                                <img style="width: 45px;height: 45px" src="http://lorempixel.com/333/222/people" class="img img-responsive img-circle"/>
                                            </div>
                                            <div class="col-md-4" style="margin-bottom: 10px">
                                                <img style="width: 45px;height: 45px" src="http://lorempixel.com/333/300/people" class="img img-responsive img-circle"/>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="online_active">
                                    <div class="row-fluid">
                                        <?php for ($i = 0; $i < 3; $i++) { ?>
                                            <div class="col-md-4" style="margin-bottom: 10px">
                                                <img style="width: 45px;height: 45px" src="http://lorempixel.com/333/300/people" class="img img-responsive img-circle"/>
                                            </div>
                                            <div class="col-md-4" style="margin-bottom: 10px">
                                                <img style="width: 45px;height: 45px" src="http://lorempixel.com/333/222/" class="img img-responsive img-circle"/>
                                            </div>
                                            <div class="col-md-4" style="margin-bottom: 10px">
                                                <img style="width: 45px;height: 45px" src="http://lorempixel.com/333/222/people" class="img img-responsive img-circle"/>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="online_all">
                                    <div class="row-fluid">
                                        <?php for ($i = 0; $i < 3; $i++) { ?>
                                            <div class="col-md-4" style="margin-bottom: 10px">
                                                <img style="width: 45px;height: 45px" src="http://lorempixel.com/333/300/people" class="img img-responsive img-circle"/>
                                            </div>
                                            <div class="col-md-4" style="margin-bottom: 10px">
                                                <img style="width: 45px;height: 45px" src="http://lorempixel.com/333/222/people" class="img img-responsive img-circle"/>
                                            </div>
                                            <div class="col-md-4" style="margin-bottom: 10px">
                                                <img style="width: 45px;height: 45px" src="http://lorempixel.com/333/222/" class="img img-responsive img-circle"/>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <h5 class="post_title_cl">
                                GROUPS
                            </h5>
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#groups_newest" aria-controls="home" role="tab" data-toggle="tab">Latest</a></li>
                                <li role="presentation"><a href="#groups_featured" aria-controls="profile" role="tab" data-toggle="tab">Featured</a></li>
                                <li role="presentation"><a href="#groups_popular" aria-controls="messages" role="tab" data-toggle="tab">Popular</a></li>
                                <li role="presentation"><a href="#groups_all" aria-controls="settings" role="tab" data-toggle="tab">See All</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="groups_newest">
                                    <div class="row-fluid">
                                        <div class="demo">
                                            <ul id="temp_lightSlider1">
                                                <li data-thumb="images/cover-1.jpg">
                                                    <img>
                                                    <iframe class="img img-thumbnail" src="http://player.vimeo.com/video/102513133"></iframe> 
                                                    </img>
                                                </li>
                                                <li data-thumb="images/cover-2.jpg">
                                                    <img>
                                                    <iframe class="img img-thumbnail" src="http://www.youtube.com/embed/xaSH0d60Zso"></iframe>
                                                    </img>
                                                </li>
                                                <li data-thumb="images/cover-3.jpg">
                                                    <img>
                                                    <iframe class="img img-thumbnail" src="//player.vimeo.com/video/70528799"></iframe>
                                                    </img>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="groups_featured">
                                    <div class="row-fluid">
                                        <div class="demo">
                                            <ul id="temp_lightSlider">
                                                <li data-thumb="images/cover-1.jpg">
                                                    <img>
                                                    <iframe class="img img-thumbnail" src="http://player.vimeo.com/video/102513133"></iframe> 
                                                    </img>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="groups_popular">
                                    <div class="row-fluid">
                                        <div class="demo">
                                            <ul id="temp_lightSlider">
                                                <li data-thumb="images/cover-3.jpg">
                                                    <img>
                                                    <iframe class="img img-thumbnail" src="//player.vimeo.com/video/70528799"></iframe>
                                                    </img>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="groups_all">
                                    <div class="row-fluid">
                                        <div class="demo">
                                            <ul id="temp_lightSlider">
                                                <li data-thumb="images/cover-2.jpg">
                                                    <img>
                                                    <iframe class="img img-thumbnail" src="http://www.youtube.com/embed/xaSH0d60Zso"></iframe>
                                                    </img>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <h5 class="post_title_cl">
                                PAGES
                            </h5>
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#pages_newest" aria-controls="home" role="tab" data-toggle="tab">Latest</a></li>
                                <li role="presentation"><a href="#pages_featured" aria-controls="profile" role="tab" data-toggle="tab">Featured</a></li>
                                <li role="presentation"><a href="#pages_popular" aria-controls="messages" role="tab" data-toggle="tab">Popular</a></li>
                                <li role="presentation"><a href="#pages_all" aria-controls="settings" role="tab" data-toggle="tab">See All</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="pages_newest">
                                    <div class="row-fluid">
                                        <div class="demo">
                                            <ul id="temp2_lightSlider1">
                                                <li data-thumb="images/cover-1.jpg">
                                                    <img>
                                                    <iframe class="img img-thumbnail" src="http://player.vimeo.com/video/102513133"></iframe> 
                                                    </img>
                                                </li>
                                                <li data-thumb="images/cover-2.jpg">
                                                    <img>
                                                    <iframe class="img img-thumbnail" src="http://www.youtube.com/embed/xaSH0d60Zso"></iframe>
                                                    </img>
                                                </li>
                                                <li data-thumb="images/cover-3.jpg">
                                                    <img>
                                                    <iframe class="img img-thumbnail" src="//player.vimeo.com/video/70528799"></iframe>
                                                    </img>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="pages_featured">
                                    <div class="row-fluid">
                                        <div class="demo">
                                            <ul id="temp2_lightSlider">
                                                <li data-thumb="images/cover-1.jpg">
                                                    <img>
                                                    <iframe class="img img-thumbnail" src="http://player.vimeo.com/video/102513133"></iframe> 
                                                    </img>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="pages_popular">
                                    <div class="row-fluid">
                                        <div class="demo">
                                            <ul id="temp2_lightSlider">
                                                <li data-thumb="images/cover-3.jpg">
                                                    <img>
                                                    <iframe class="img img-thumbnail" src="//player.vimeo.com/video/70528799"></iframe>
                                                    </img>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="pages_all">
                                    <div class="row-fluid">
                                        <div class="demo">
                                            <ul id="temp2_lightSlider">
                                                <li data-thumb="images/cover-2.jpg">
                                                    <img>
                                                    <iframe class="img img-thumbnail" src="http://www.youtube.com/embed/xaSH0d60Zso"></iframe>
                                                    </img>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <!-- social-ul -->
                <div class="social-ul" id="header_social_ul" style="margin-top: 10px">
                    <ul>
                        <li class="social-twitter"><a href="#" target="_blank" title="Twitter" data-toggle="tooltip"><i class="fa fa-twitter"></i></a></li>
                        <li class="social-facebook"><a href="#" target="_blank" title="Facebook" data-toggle="tooltip"><i class="fa fa-facebook"></i></a></li>
                        <li class="social-google"><a href="#" target="_blank" title="Google Plus" data-toggle="tooltip"><i class="fa fa-google-plus"></i></a></li>
                        <li class="social-linkedin"><a href="#" target="_blank" title="Linkedin" data-toggle="tooltip"><i class="fa fa-linkedin"></i></a></li>
                        <li class="social-dribbble"><a href="#" target="_blank" title="Dribbble" data-toggle="tooltip"><i class="fa fa-dribbble"></i></a></li>
                        <li class="social-behance"><a href="#" target="_blank" title="Behance" data-toggle="tooltip"><i class="fa fa-behance"></i></a></li>
                    </ul>
                </div>
                <!-- End social-ul -->

                <div class="copyright">
                    <p style="margin-bottom:0;">Copyright &copy; <?php echo date('Y') . ' - ' . date('Y', strtotime(date('Y') . '+ 1 Year')) ?>.</p>		
                </div> <!--/ .copyright -->


            </header>
            <!-- #header -->

            <div class="header-main">
                <div class="content_wrappers">		
                    <div class="logo_wrapper">

                        <span class="site_title">
                            <a href="#" title="Throne">
                                <img src="images/logo-top.png" alt="FlexyBlog">
                            </a>
                        </span>

                    </div>

                    <a class="nav-btn menu_close" id="btn_open_menu" href="#"><i class="fa fa-bars"></i></a>
                    <a class="nav-btn sidebar_close" id="btn_open_sidebar" href="#"><i class="icon-action-undo"></i></a>

                </div>

            </div>
            <!-- #header -->
            <?php if ($show_top_menu) { ?>
                <div id="content-wrapper" class="menu_content_wrapper<?php echo isset($_COOKIE['menu_layout']) && $_COOKIE['menu_layout'] ? ' inverse' : '' ?>">
                    <!--<div class="top-menu-bg"></div>-->
                    <div id="main-content" style="margin: 10px;margin-bottom: 0">
                        <ul class="top-menu transition_200">
                            <li>
                                <a href="#"><img class="menu-icon" src="images/logout-icon.png" /></a>
                            </li>
                            <li>
                                <a href="index.php" class="<?php echo $file == 'index.php' ? 'top-menu-active' : '' ?>">Home</a>
                            </li>
                            <li>
                                <a href="#">Business</a>
                            </li>
                            <li>
                                <a href="#">Community</a>
                            </li>
                            <li>
                                <a href="#">Entertainment</a>
                            </li>
                            <li>
                                <a href="#"><img class="ig-icon" src="images/logo-icon.png" /></a>
                            </li>
                            <li>
                                <a href="#">Events</a>
                            </li>
                            <li>
                                <a href="#">Leisure</a>
                            </li>
                            <li>
                                <a href="#">Self</a>
                            </li>
                            <li>
                                <a href="#">Shopping</a>
                            </li>
                            <li>
                                <a href="#"><img class="menu-icon" src="images/share-icon.png" /></a>
                            </li>
                            <li style="position: absolute;margin-top: 5px">
                                <a href="#" id="trigger_click" title="Color Switcher"><span class="changebutton"><i class="fa fa-2x fa-cog colors-color"></i></span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            <?php } ?>