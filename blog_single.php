<?php
$title = "Single Post";
?>
<?php include_once __DIR__ . '/header.php' ?>
<div class="col-md-1 bhoechie-tab-menu" style="width:6%;background-image: url('images/sub_nav_menu_bg.png');">
    <div class="list-group">
        <a href="#" class="list-group-item active text-center" data-post_type="a-z">
            <h4 class="fa fa-2x fa-clock-o"></h4><br/>A-Z
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="a-z">
            <h4 class="fa fa-2x fa-user"></h4><br/>Only Mine
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="all posts">
            <h4 class="fa fa-2x fa-square"></h4><br/>Show All
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="media">
            <h4 class="fa fa-2x fa-music"></h4><br/>Media
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="collection">
            <h4 class="fa fa-2x fa-columns"></h4><br/><p style="margin-left: -10px;">Collections</p>
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="events">
            <h4 class="fa fa-2x fa-calendar"></h4><br/>Events 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="blogs">
            <h4 class="fa fa-2x fa-edit"></h4><br/>Blogs
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="Groups">
            <h4 class="fa fa-2x fa-users"></h4><br/>Groups 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="Pages">
            <h4 class="fa fa-2x fa-paste"></h4><br/>Pages 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="favorites">
            <h4 class="fa fa-2x fa-star"></h4><br/>Favorites 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="member">
            <h4 class="fa fa-2x fa-users"></h4><br/>Members
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="settings">
            <h4 class="fa fa-2x fa-cog"></h4><br/>Settings 
        </a>
    </div>
</div>
<div id="all_posts_div" class="col-md-7" style="<?php echo $f_name == 'index.php' ? '' : 'background-color: #fff;'; ?>padding: 5px 5px;width:50%">
    <div class="content-single blog-post">
        <article class="post-standard article-content">

            <h2 style="display:none">Single Post</h2>

            <div class="post-header">

                <div class="post-categoris">
                    <a href="#" style="background:#61c436">Food</a><a href="#" style="background:#E91B23">health</a><a href="#" style="background:#f4b23f">Gallery</a><a href="#" style="background:#607ec7">Fashion</a>	
                </div>

                <h2 class="post-title">Stunning Health Benefits of Eating Chocolates</h2>

                <div class="post-meta">
                    <div><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></div>
                    <div><i class="fa fa-clock-o"></i>January 18, 2015</div>
                    <div><i class="fa fa-comments"></i><a href="#" title="">5 Comments</a></div>
                    <div><i class="icon-folder"></i>in : <a href="#" rel="category tag">WordPress</a></div>
                    <div><a class="post-lik " title="Love"><i class="icon-heart"></i><span>35</span></a></div>
                    <div><i class="icon-eye"></i><span>216</span></div>
                </div>

            </div>

            <div class="post-thumb">

                <div class="flexslider flexslider-blog">

                    <!--/ .slides -->

                    <div class="flex-viewport" style="overflow: hidden; position: relative; height: 450px;"><ul class="slides" style="width: 2000%; transition-duration: 0s; transform: translate3d(-800px, 0px, 0px);"><li class="image-link clone" aria-hidden="true" style="width: 800px; float: left; display: block;">
                                <img src="http://placehold.it/837x471" alt="Gallery Post Example" title="" draggable="false">
                            </li>

                            <li class="image-link flex-active-slide" style="width: 800px; float: left; display: block;">
                                <img src="http://placehold.it/837x471" alt="Gallery Post Example" title="" draggable="false">
                            </li>

                            <li class="image-link" style="width: 800px; float: left; display: block;">
                                <img src="http://placehold.it/837x471" alt="Gallery Post Example" title="" draggable="false">
                            </li>

                            <li class="image-link" style="width: 800px; float: left; display: block;">
                                <img src="http://placehold.it/837x471" alt="Gallery Post Example" title="" draggable="false">
                            </li>

                            <li class="image-link" style="width: 800px; float: left; display: block;">
                                <img src="http://placehold.it/837x471" alt="Gallery Post Example" title="" draggable="false">
                            </li>

                            <li class="image-link" style="width: 800px; float: left; display: block;">
                                <img src="http://placehold.it/837x471" alt="Gallery Post Example" title="" draggable="false">
                            </li>

                            <li class="image-link" style="width: 800px; float: left; display: block;">
                                <img src="http://placehold.it/837x471" alt="Gallery Post Example" title="" draggable="false">
                            </li>

                            <li class="image-link" style="width: 800px; float: left; display: block;">
                                <img src="http://placehold.it/837x471" alt="Gallery Post Example" title="" draggable="false">
                            </li>

                            <li class="image-link" style="width: 800px; float: left; display: block;">
                                <img src="http://placehold.it/837x471" alt="Gallery Post Example" title="" draggable="false">
                            </li>

                            <li class="image-link clone" aria-hidden="true" style="width: 800px; float: left; display: block;">
                                <img src="http://placehold.it/837x471" alt="Gallery Post Example" title="" draggable="false">
                            </li></ul></div><ol class="flex-control-nav flex-control-paging"><li><a class="flex-active">1</a></li><li><a class="">2</a></li><li><a class="">3</a></li><li><a class="">4</a></li><li><a class="">5</a></li><li><a class="">6</a></li><li><a class="">7</a></li><li><a class="">8</a></li></ol><ul class="flex-direction-nav"><li><a class="flex-prev" href="#"><i class="fa fa-angle-left"></i></a></li><li><a class="flex-next" href="#"><i class="fa fa-angle-right"></i></a></li></ul></div> <!--/ .flexslider -->

            </div>


            <div class="article-main-content">
                <div class="post-content entry">
                    <p><span class="dropcap1">D</span>on’t act so surprised, Your Highness. You weren’t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck. Partially, but it also obeys your commands. I want to come with you to Alderaan. There’s nothing for me here now. I want to learn the ways of the Force and be a Jedi, like my father before me. The more you tighten your grip, Tarkin, the more star systems will slip through your fingers.</p>

                    <h3><strong>Fasces of harpoons for spurs</strong></h3>

                    <p>With a frigate’s anchors for my bridle-bitts and fasces of harpoons for spurs, would I could mount that <strong>whale and leap</strong> the topmost skies, to see whether the fabled heavens with all their countless tents really lie encamped beyond!</p>

                    <p>Still, she’s got a lot of spirit. I don’t know, what do you think? What!? I don’t know what you’re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan– What good is a reward if you ain’t around to use it? Besides, attacking that battle station ain’t my idea of courage. It’s more like…suicide.</p>

                    <p><img src="http://placehold.it/1130x654" alt="image" style="width: 100%;"></p>

                    <h3><strong>Sperm Whaler like the Pequod</strong></h3>

                    <p>On the second day, numbers of Right Whales were seen, who, secure from the attack of a Sperm Whaler like the Pequod, with open jaws <em>sluggishly </em>swam through the brit, which, adhering to the fringing fibres of that wondrous Venetian blind in their mouths, was in that manner separated from the water that escaped at the lip.</p>

                    <p><img class="alignleft" src="http://placehold.it/241x300" alt="image" width="241" height="300"></p>

                    <p>You think water moves fast? You should see ice. It moves like it has a mind. Like it knows it killed the world once and got a taste for murder. After the avalanche, it took us a week to climb out. Now, I don’t know exactly when we turned on each other, but I know that seven of us survived the slide… and only five made it out. Now we took an oath, that I’m breaking now. We said we’d say it was the snow that killed the other two, but it wasn’t. Nature is lethal but it doesn’t hold a candle to man.</p>

                    <p>My money’s in that office, right? If she start giving me some bullshit about it ain’t there, and we got to go someplace else and get it, I’m gonna shoot you in the head then and there. Then I’m gonna shoot that bitch in the kneecaps, find out where my goddamn money is. She gonna tell me too. Hey, look at me when I’m talking to you, motherfucker. You listen: we go in there, and that nigga Winston or anybody else is in there, you the first motherfucker to get shot. You understand?</p>

                    <h4><strong>Then they show that show to the people</strong></h4>

                    <p>Well, the way they make shows is, they make one show. That show’s called a pilot. Then they show that show to the people who make shows, and on the strength of that one show they decide if they’re going to make more shows. Some pilots get picked and become television programs. Some don’t, become nothing. She starred in one of the ones that became nothing.</p>

                    <blockquote class="blockquote-1">
                        <p>“ You have brains in your head. You have feet in your shoes. You can steer yourself in any direction you choose. You’re on your own, and you know what you know. And you are the guy who’ll decide where to go. ”</p>
                        <span class="author">- Dr. Seuss -</span>
                    </blockquote>

                    <p>She must have hidden the plans in the escape pod. Send a detachment down to retrieve them, and see to it personally, Commander. <span class="highlight highlight-yellow">There’ll be no one to stop us this time!</span>&nbsp;You’re all clear, kid. Let’s blow this thing and go home! Partially, but it also obeys your commands.</p>

                    <ul> 
                        <li>Dantooine. They’re on Dantooine.</li> 
                        <li>He is here.</li> 
                        <li>Don’t underestimate the Force.</li> 
                    </ul>

                    <p><img src="http://placehold.it/1024x683" style="width: 100%;"></p>

                    <p>I care. So, what do you think of her, Han? A tremor in the Force. The last time I felt it was in the presence of my old master. <span class="highlight highlight-blue">But with the blast shield down,</span>&nbsp;I can’t even see! How am I supposed to fight? Obi-Wan is here. The Force is with him. But with the blast shield down, I can’t even see! How am I supposed to fight? You are a part of the Rebel Alliance and a traitor! Take her away!</p>

                    <div class="fc-padding" style="padding-left:5%; padding-right:5%;">
                        <p>Still, she’s got a lot of spirit. I don’t know, what do you think? What!? I don’t know what you’re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan– What good is a reward if you ain’t around to use it? Besides, attacking that battle station ain’t my idea of courage. It’s more like…suicide.</p>

                        <p>You don’t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you’re going. What?! The Force is strong with this one. I have you now.</p>

                        <ol> 
                            <li>I care. So, what do you think of her, Han?</li> 
                            <li>You mean it controls your actions?</li> 
                            <li>Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you’re going.</li> 
                            <li>I’m trying not to, kid.</li> 
                        </ol> 
                    </div>



                    <hr class="vertical-space1">

                    <p><img class="alignright" src="http://placehold.it/200x300" alt="image" width="200" height="300">I can’t get involved! I’ve got work to do! It’s not that I like the Empire, I hate it, but there’s nothing I can do about it right now. It’s such a long way from here. Leave that to me. Send a distress signal, and inform the Senate that all on board were killed. I’m surprised you had the courage to take the responsibility yourself. No! Alderaan is peaceful. We have no weapons. You can’t possibly…</p>

                    <p>Your eyes can deceive you. Don’t trust them. He is here. What?! Hokey religions and ancient weapons are no match for a good blaster at your side, kid. I’m trying not to, kid.</p>

                    <p>I’m trying not to, kid. I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>


                    <blockquote>
                        <p>You have brains in your head. You have feet in your shoes. You can steer yourself in any direction you choose. You're on your own, and you know what you know. And you are the guy who'll decide where to go.</p>
                        <span class="author">― Dr. Seuss</span>
                        <div class="clearfix"></div>
                    </blockquote>

                    <p>Still, she’s got a lot of spirit. I don’t know, what do you think? What!? I don’t know what you’re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan– What good is a reward if you ain’t around to use it? Besides, attacking that battle station ain’t my idea of courage. It’s more like…suicide.</p>

                    <p>Hey, Luke! May the Force be with you. Kid, I’ve flown from one side of this galaxy to the other. I’ve seen a lot of strange stuff, but I’ve never seen anything to make me believe there’s one all-powerful Force controlling everything. There’s no mystical energy field that controls my destiny. It’s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>

                    <p>You don’t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you’re going. What?! The Force is strong with this one. I have you now.</p>

                    <p><img src="http://placehold.it/1000x563" alt="image" style="width: 100%;"></p>


                    <p>Hey, Luke! May the Force be with you. Kid, I’ve flown from one side of this galaxy to the other. I’ve seen a lot of strange stuff, but I’ve never seen anything to make me believe there’s one all-powerful Force controlling everything. There’s no mystical energy field that controls my destiny. It’s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p> 

                    <p>All right. Well, take care of yourself, Han. <span class="highlight highlight-orange">I guess that’s what you’re best at,</span> ain’t it? Alderaan? I’m not going to Alderaan. I’ve got to go home. It’s late, I’m in for it as it is. The plans you refer to will soon be back in our hands.<br> </p>

                    <p>You’re all clear, kid. <span class="highlight highlight-green">Let’s blow this thing and go home!</span>&nbsp;But with the blast shield down, I can’t even see! How am I supposed to fight? Alderaan? I’m not going to Alderaan. I’ve got to go home. It’s late, I’m in for it as it is.</p>


                    <!-- Tags -->
                    <div class="post-tags">
                        <a href="#" rel="tag">fashion</a>
                        <a href="#" rel="tag">web</a>
                        <a href="#" rel="tag">voting</a>
                        <a href="#" rel="tag">elementum</a>
                        <a href="#" rel="tag">fitness</a>
                        <a href="#" rel="tag">Wordpress</a>
                    </div>
                    <!-- //Tags -->

                </div>

                <div class="share-icons-single" id="tooltip-share">
                    <h5>Share this Story</h5>

                    <ul>
                        <li class="follow-twitter">
                            <a href="#" title="" target="_blank" data-toggle="tooltip" data-original-title="Share on Twitter"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li class="follow-facebook">
                            <a href="#" title="" target="_blank" data-toggle="tooltip" data-original-title="Share on Facebook"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li class="follow-gplus">
                            <a href="#" title="" target="_blank" data-toggle="tooltip" data-original-title="Share on Google+"><i class="fa fa-google-plus"></i></a>
                        </li>
                        <li class="follow-linkedin">
                            <a href="#" title="" target="_blank" data-toggle="tooltip" data-original-title="Share on Linkedin"><i class="fa fa-linkedin"></i></a>
                        </li>
                        <li class="follow-dribbble">
                            <a href="#" title="" target="_blank" data-toggle="tooltip" data-original-title="Share on Dribbble"><i class="fa fa-dribbble"></i></a>
                        </li>
                        <li class="follow-behance">
                            <a href="#" title="" target="_blank" data-toggle="tooltip" data-original-title="Share on Behance"><i class="fa fa-behance"></i></a>
                        </li>
                        <li class="follow-email">
                            <a href="#" title="" target="_blank" data-toggle="tooltip" data-original-title="Share via Email"><i class="fa fa-envelope-o"></i></a>
                        </li>
                    </ul>

                </div>


                <!-- next and prev post  -->
                <nav class="prev-next-nav">

                    <div class="fc-prev-link">
                        <a href="#" rel="prev">
                            <span class="img-wrp image-link">
                                <img width="375" height="195" src="http://placehold.it/392x195" alt="image">
                                <span class="fc-pn-ico"><i class="fa fa fa-chevron-left"></i></span>
                            </span>
                            <span class="fc-prev-next-link">What Is the Definition of an Entrepreneur Lifestyle?</span>
                        </a>		
                    </div>


                    <div class="fc-next-link">
                        <a href="#" rel="prev">
                            <span class="img-wrp image-link">
                                <img width="375" height="195" src="http://placehold.it/392x195" alt="image">
                                <span class="fc-pn-ico"><i class="fa fa fa-chevron-right"></i></span>
                            </span>
                            <span class="fc-prev-next-link">Solar Energy for Mother Earth and Everyday Smiles</span>
                        </a>		
                    </div>

                </nav>
                <!-- end next and prev post  -->		


                <!-- about author  -->
                <div id="about-Author">

                    <div class="block-head">
                        <h3>About Author</h3>
                    </div>

                    <div class="author-page">

                        <a href="#" title="John Doe">
                            <img src="http://placehold.it/109x109" width="100" height="100" alt="John Doe" class="img-avatar test">
                        </a>

                        <div class="desc">

                            <div class="author-counters">
                                <a href="#" title="Posts by John Doe" rel="author">
                                    <span class="author-name">John Doe</span>
                                </a>
                                <a href="#">
                                    <span class="author-post-count">75 Posts</span>
                                </a>
                            </div>

                            <div class="social-icons author-social" id="tooltip_social">
                                <ul>
                                    <li class="follow-twitter">
                                        <a href="#" title="" data-toggle="tooltip" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li class="follow-facebook">
                                        <a href="#" title="" data-toggle="tooltip" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li class="follow-gplus">
                                        <a href="#" title="" data-toggle="tooltip" data-original-title="gplus"><i class="fa fa-google-plus"></i></a>
                                    </li>
                                    <li class="follow-linkedin">
                                        <a href="#" title="" data-toggle="tooltip" data-original-title="Linkedin"><i class="fa fa-linkedin"></i></a>
                                    </li>

                                    <li class="follow-dribbble">
                                        <a href="#" title="" data-toggle="tooltip" data-original-title="Dribbble"><i class="fa fa-dribbble"></i></a>
                                    </li>

                                    <li class="follow-behance">
                                        <a href="#" title="" data-toggle="tooltip" data-original-title="Behance"><i class="fa fa-behance"></i></a>
                                    </li>

                                    <li class="follow-email">
                                        <a href="#" title="" data-toggle="tooltip" data-original-title="Email"><i class="fa fa-envelope-o"></i></a>
                                    </li>


                                </ul>
                            </div>      

                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

                            <div class="clearfix"></div>

                        </div><!-- end descp -->

                    </div><!-- end author-page -->
                </div>
                <!-- end author  -->



                <!--related articles-->		
                <section id="related-posts">

                    <div class="block-head">
                        <h3>related posts</h3>

                        <div id="nav_carousel_related_post" class="nav-carousel"><ul class="flex-direction-nav"><li><a class="flex-prev" href="#"></a></li><li><a class="flex-next" href="#"></a></li></ul></div>
                    </div>


                    <div class="carousel_related_post">



                        <div class="flex-viewport" style="overflow: hidden; position: relative;"><ul class="slides clearfix" style="width: 2400%; transition-duration: 0.6s; transform: translate3d(-1640px, 0px, 0px);">

                                <li style="width: 253.333px; float: left; display: block;">

                                    <div class="related-post-img">
                                        <a href="#" class="image-link"><img src="http://placehold.it/253x181" alt="" draggable="false"></a>
                                    </div>

                                    <div class="related-post-head">
                                        <a href="#">Tips for taking great city photos with your smartphone</a>
                                        <span><i class="fa fa-clock-o"></i>March 08, 2015</span>
                                        <span><i class="fa fa-comments"></i><a href="#">25</a></span>
                                        <span><i class="fa fa-eye"></i>36</span>
                                    </div>

                                </li>

                                <li style="width: 253.333px; float: left; display: block;">
                                    <div class="related-post-img">
                                        <span class="featured_icon_post"><i class="icon-camera"></i></span>
                                        <a href="#" class="image-link"><img src="http://placehold.it/253x181" alt="" draggable="false"></a>
                                    </div>

                                    <div class="related-post-head">
                                        <a href="#">Travelling With Technology – Some Tips From Master</a>
                                        <span><i class="fa fa-clock-o"></i>March 09, 2015</span>
                                        <span><i class="fa fa-comments"></i><a href="#">21</a></span>
                                        <span><i class="fa fa-eye"></i>40</span>
                                    </div>
                                </li>

                                <li style="width: 253.333px; float: left; display: block;">
                                    <div class="related-post-img">
                                        <span class="featured_icon_post"><i class="icon-picture"></i></span>
                                        <a href="#" class="image-link"><img src="http://placehold.it/253x181" alt="" draggable="false"></a>
                                    </div>

                                    <div class="related-post-head">
                                        <a href="#">Success Is a Choice – What Are You Going to Do?</a>
                                        <span><i class="fa fa-clock-o"></i>March 10, 2015</span>
                                        <span><i class="fa fa-comments"></i><a href="#">18</a></span>
                                        <span><i class="fa fa-eye"></i>10</span>
                                    </div>
                                </li>

                                <li style="width: 253.333px; float: left; display: block;">
                                    <div class="related-post-img">
                                        <span class="featured_icon_post"><i class="icon-doc"></i></span>
                                        <a href="#" class="image-link"><img src="http://placehold.it/253x181" alt="" draggable="false"></a>
                                    </div>

                                    <div class="related-post-head">
                                        <a href="#">Be a True Entrepreneur, Adopting the Right Thought Processes</a>
                                        <span><i class="fa fa-clock-o"></i>March 11, 2015</span>
                                        <span><i class="fa fa-comments"></i><a href="#">14</a></span>
                                        <span><i class="fa fa-eye"></i>05</span>
                                    </div>
                                </li>

                                <li style="width: 253.333px; float: left; display: block;">
                                    <div class="related-post-img">
                                        <a href="#" class="image-link"><img src="http://placehold.it/253x181" alt="" draggable="false"></a>
                                    </div>

                                    <div class="related-post-head">
                                        <a href="#">Staying in Fashion With the Perfect Accessory</a>
                                        <span><i class="fa fa-clock-o"></i>March 12, 2015</span>
                                        <span><i class="fa fa-comments"></i><a href="#">07</a></span>
                                        <span><i class="fa fa-eye"></i>12</span>
                                    </div>
                                </li>

                                <li style="width: 253.333px; float: left; display: block;">
                                    <div class="related-post-img">
                                        <span class="featured_icon_post"><i class="icon-link"></i></span>
                                        <a href="#" class="image-link"><img src="http://placehold.it/253x181" alt="" draggable="false"></a>
                                    </div>

                                    <div class="related-post-head">
                                        <a href="#">Pasta makes the world go round – Wheat Rotini Pasta Salad</a>
                                        <span><i class="fa fa-clock-o"></i>March 13, 2015</span>
                                        <span><i class="fa fa-comments-o"></i><a href="#">02</a></span>
                                        <span><i class="fa fa-eye"></i>23</span>
                                    </div>
                                </li>

                                <li style="width: 253.333px; float: left; display: block;">

                                    <div class="related-post-img">
                                        <span class="featured_icon_post"><i class="icon-link"></i></span>
                                        <a href="#" class="image-link"><img src="http://placehold.it/253x181" alt="" draggable="false"></a>
                                    </div>

                                    <div class="related-post-head">
                                        <a href="#">Alibaba Is Investing Huge Sums In An Array Of U.S. Tech Companies</a>
                                        <span><i class="fa fa-clock-o"></i>March 08, 2015</span>
                                        <span><i class="fa fa-comments"></i><a href="#">8</a></span>
                                        <span><i class="fa fa-eye"></i>7417</span>
                                    </div>

                                </li>

                                <li style="width: 253.333px; float: left; display: block;">
                                    <div class="related-post-img">
                                        <span class="featured_icon_post"><i class="icon-camera"></i></span>
                                        <a href="#" class="image-link"><img src="http://placehold.it/253x181" alt="" draggable="false"></a>
                                    </div>

                                    <div class="related-post-head">
                                        <a href="#">Gadgets If Magneto Were Shopping For A Watch, He'd Buy This Omega</a>
                                        <span><i class="fa fa-clock-o"></i>March 09, 2015</span>
                                        <span><i class="fa fa-comments"></i><a href="#">21</a></span>
                                        <span><i class="fa fa-eye"></i>40</span>
                                    </div>
                                </li>

                                <li style="width: 253.333px; float: left; display: block;">
                                    <div class="related-post-img">
                                        <span class="featured_icon_post"><i class="icon-picture"></i></span>
                                        <a href="#" class="image-link"><img src="http://placehold.it/253x181" alt="" draggable="false"></a>
                                    </div>

                                    <div class="related-post-head">
                                        <a href="#">Requests For User Data Rise In Twitter’s Latest Transparency Report</a>
                                        <span><i class="fa fa-clock-o"></i>March 10, 2015</span>
                                        <span><i class="fa fa-comments"></i><a href="#">23</a></span>
                                        <span><i class="fa fa-eye"></i>56</span>
                                    </div>
                                </li>

                                <li style="width: 253.333px; float: left; display: block;">
                                    <div class="related-post-img">
                                        <span class="featured_icon_post"><i class="icon-doc"></i></span>
                                        <a href="#" class="image-link"><img src="http://placehold.it/253x181" alt="" draggable="false"></a>
                                    </div>

                                    <div class="related-post-head">
                                        <a href="#">After a Series of Setbacks, Target Chooses an Outsider as C.E.O. for the First Time.</a>
                                        <span><i class="fa fa-clock-o"></i>March 11, 2015</span>
                                        <span><i class="fa fa-comments"></i><a href="#">65</a></span>
                                        <span><i class="fa fa-eye"></i>120</span>
                                    </div>
                                </li>

                                <li style="width: 253.333px; float: left; display: block;">
                                    <div class="related-post-img">
                                        <a href="#" class="image-link"><img src="http://placehold.it/253x181" alt="" draggable="false"></a>
                                    </div>

                                    <div class="related-post-head">
                                        <a href="#">Stock Market Ends July in Dive, but Analysts Are Upbeat</a>
                                        <span><i class="fa fa-clock-o"></i>March 12, 2015</span>
                                        <span><i class="fa fa-comments"></i><a href="#">25</a></span>
                                        <span><i class="fa fa-eye"></i>2414</span>
                                    </div>
                                </li>

                                <li style="width: 253.333px; float: left; display: block;">
                                    <div class="related-post-img">
                                        <span class="featured_icon_post"><i class="icon-link"></i></span>
                                        <a href="#" class="image-link"><img src="http://placehold.it/253x181" alt="" draggable="false"></a>
                                    </div>

                                    <div class="related-post-head">
                                        <a href="#">Scientists Estimate That Laughing 100 Times Is Equivalent To A 10-Minute ...</a>
                                        <span><i class="fa fa-clock-o"></i>March 13, 2015</span>
                                        <span><i class="fa fa-comments-o"></i><a href="#">45</a></span>
                                        <span><i class="fa fa-eye"></i>136</span>
                                    </div>
                                </li>

                            </ul></div></div>

                </section>


                <!--latest articles-->		
                <section id="latest-articles">

                    <div class="block-head">
                        <h3>latest articles</h3>

                        <div id="nav_carousel_latest_articles" class="nav-carousel"><ul class="flex-direction-nav"><li><a class="flex-prev" href="#"></a></li><li><a class="flex-next" href="#"></a></li></ul></div>
                    </div>


                    <div class="carousel_latest_articles">



                        <div class="flex-viewport" style="overflow: hidden; position: relative;"><ul class="slides  clearfix" style="width: 2400%; transition-duration: 0.6s; transform: translate3d(-1640px, 0px, 0px);">

                                <li style="width: 253.333px; float: left; display: block;">

                                    <div class="related-post-img">
                                        <span class="featured_icon_post"><i class="icon-link"></i></span>
                                        <a href="#" class="image-link"><img src="http://placehold.it/253x181" alt="" draggable="false"></a>
                                    </div>

                                    <div class="related-post-head">
                                        <a href="#">Alibaba Is Investing Huge Sums In An Array Of U.S. Tech Companies</a>
                                        <span><i class="fa fa-clock-o"></i>March 08, 2015</span>
                                        <span><i class="fa fa-comments"></i><a href="#">8</a></span>
                                        <span><i class="fa fa-eye"></i>7417</span>
                                    </div>

                                </li>

                                <li style="width: 253.333px; float: left; display: block;">
                                    <div class="related-post-img">
                                        <span class="featured_icon_post"><i class="icon-camera"></i></span>
                                        <a href="#" class="image-link"><img src="http://placehold.it/253x181" alt="" draggable="false"></a>
                                    </div>

                                    <div class="related-post-head">
                                        <a href="#">Gadgets If Magneto Were Shopping For A Watch, He'd Buy This Omega</a>
                                        <span><i class="fa fa-clock-o"></i>March 09, 2015</span>
                                        <span><i class="fa fa-comments"></i><a href="#">21</a></span>
                                        <span><i class="fa fa-eye"></i>40</span>
                                    </div>
                                </li>

                                <li style="width: 253.333px; float: left; display: block;">
                                    <div class="related-post-img">
                                        <span class="featured_icon_post"><i class="icon-picture"></i></span>
                                        <a href="#" class="image-link"><img src="http://placehold.it/253x181" alt="" draggable="false"></a>
                                    </div>

                                    <div class="related-post-head">
                                        <a href="#">Requests For User Data Rise In Twitter’s Latest Transparency Report</a>
                                        <span><i class="fa fa-clock-o"></i>March 10, 2015</span>
                                        <span><i class="fa fa-comments"></i><a href="#">23</a></span>
                                        <span><i class="fa fa-eye"></i>56</span>
                                    </div>
                                </li>

                                <li style="width: 253.333px; float: left; display: block;">
                                    <div class="related-post-img">
                                        <span class="featured_icon_post"><i class="icon-doc"></i></span>
                                        <a href="#" class="image-link"><img src="http://placehold.it/253x181" alt="" draggable="false"></a>
                                    </div>

                                    <div class="related-post-head">
                                        <a href="#">After a Series of Setbacks, Target Chooses an Outsider as C.E.O. for the First Time.</a>
                                        <span><i class="fa fa-clock-o"></i>March 11, 2015</span>
                                        <span><i class="fa fa-comments"></i><a href="#">65</a></span>
                                        <span><i class="fa fa-eye"></i>120</span>
                                    </div>
                                </li>

                                <li style="width: 253.333px; float: left; display: block;">
                                    <div class="related-post-img">
                                        <a href="#" class="image-link"><img src="http://placehold.it/253x181" alt="" draggable="false"></a>
                                    </div>

                                    <div class="related-post-head">
                                        <a href="#">Stock Market Ends July in Dive, but Analysts Are Upbeat</a>
                                        <span><i class="fa fa-clock-o"></i>March 12, 2015</span>
                                        <span><i class="fa fa-comments"></i><a href="#">25</a></span>
                                        <span><i class="fa fa-eye"></i>2414</span>
                                    </div>
                                </li>

                                <li style="width: 253.333px; float: left; display: block;">
                                    <div class="related-post-img">
                                        <span class="featured_icon_post"><i class="icon-link"></i></span>
                                        <a href="#" class="image-link"><img src="http://placehold.it/253x181" alt="" draggable="false"></a>
                                    </div>

                                    <div class="related-post-head">
                                        <a href="#">Scientists Estimate That Laughing 100 Times Is Equivalent To A 10-Minute ...</a>
                                        <span><i class="fa fa-clock-o"></i>March 13, 2015</span>
                                        <span><i class="fa fa-comments-o"></i><a href="#">45</a></span>
                                        <span><i class="fa fa-eye"></i>136</span>
                                    </div>
                                </li>

                                <li style="width: 253.333px; float: left; display: block;">

                                    <div class="related-post-img">
                                        <a href="#" class="image-link"><img src="http://placehold.it/253x181" alt="" draggable="false"></a>
                                    </div>

                                    <div class="related-post-head">
                                        <a href="#">Tips for taking great city photos with your smartphone</a>
                                        <span><i class="fa fa-clock-o"></i>March 08, 2015</span>
                                        <span><i class="fa fa-comments"></i><a href="#">25</a></span>
                                        <span><i class="fa fa-eye"></i>36</span>
                                    </div>

                                </li>

                                <li style="width: 253.333px; float: left; display: block;">
                                    <div class="related-post-img">
                                        <span class="featured_icon_post"><i class="icon-camera"></i></span>
                                        <a href="#" class="image-link"><img src="http://placehold.it/253x181" alt="" draggable="false"></a>
                                    </div>

                                    <div class="related-post-head">
                                        <a href="#">Travelling With Technology – Some Tips From Master</a>
                                        <span><i class="fa fa-clock-o"></i>March 09, 2015</span>
                                        <span><i class="fa fa-comments"></i><a href="#">21</a></span>
                                        <span><i class="fa fa-eye"></i>40</span>
                                    </div>
                                </li>

                                <li style="width: 253.333px; float: left; display: block;">
                                    <div class="related-post-img">
                                        <span class="featured_icon_post"><i class="icon-picture"></i></span>
                                        <a href="#" class="image-link"><img src="http://placehold.it/253x181" alt="" draggable="false"></a>
                                    </div>

                                    <div class="related-post-head">
                                        <a href="#">Success Is a Choice – What Are You Going to Do?</a>
                                        <span><i class="fa fa-clock-o"></i>March 10, 2015</span>
                                        <span><i class="fa fa-comments"></i><a href="#">18</a></span>
                                        <span><i class="fa fa-eye"></i>10</span>
                                    </div>
                                </li>

                                <li style="width: 253.333px; float: left; display: block;">
                                    <div class="related-post-img">
                                        <span class="featured_icon_post"><i class="icon-doc"></i></span>
                                        <a href="#" class="image-link"><img src="http://placehold.it/253x181" alt="" draggable="false"></a>
                                    </div>

                                    <div class="related-post-head">
                                        <a href="#">Be a True Entrepreneur, Adopting the Right Thought Processes</a>
                                        <span><i class="fa fa-clock-o"></i>March 11, 2015</span>
                                        <span><i class="fa fa-comments"></i><a href="#">14</a></span>
                                        <span><i class="fa fa-eye"></i>05</span>
                                    </div>
                                </li>

                                <li style="width: 253.333px; float: left; display: block;">
                                    <div class="related-post-img">
                                        <a href="#" class="image-link"><img src="http://placehold.it/253x181" alt="" draggable="false"></a>
                                    </div>

                                    <div class="related-post-head">
                                        <a href="#">Staying in Fashion With the Perfect Accessory</a>
                                        <span><i class="fa fa-clock-o"></i>March 12, 2015</span>
                                        <span><i class="fa fa-comments"></i><a href="#">07</a></span>
                                        <span><i class="fa fa-eye"></i>12</span>
                                    </div>
                                </li>

                                <li style="width: 253.333px; float: left; display: block;">
                                    <div class="related-post-img">
                                        <span class="featured_icon_post"><i class="icon-link"></i></span>
                                        <a href="#" class="image-link"><img src="http://placehold.it/253x181" alt="" draggable="false"></a>
                                    </div>

                                    <div class="related-post-head">
                                        <a href="#">Pasta makes the world go round – Wheat Rotini Pasta Salad</a>
                                        <span><i class="fa fa-clock-o"></i>March 13, 2015</span>
                                        <span><i class="fa fa-comments-o"></i><a href="#">02</a></span>
                                        <span><i class="fa fa-eye"></i>23</span>
                                    </div>
                                </li>

                            </ul></div></div>

                </section>



                <section id="listing-no-comments">
                    <div class="block-head">
                        <h3>No Comments</h3>
                    </div>

                    <div class="list-comments-post">

                        <div class="no-comment">
                            <h5><i class="fa fa-comments"></i> <span class="title-comment">No Comments!</span></h5>
                            <p>There are no comments yet, but you can be first to comment this article.</p>
                        </div>

                    </div>

                </section>


                <section id="listing-comments">
                    <div class="block-head">
                        <h3>7 Comments</h3>
                    </div>


                    <ul class="comments">
                        <li class="comment">
                            <!-- comment_content -->
                            <div class="comment_content">

                                <div class="comment-avatar">
                                    <img src="http://placehold.it/61x61" alt="image">
                                </div>

                                <div class="author-comment">

                                    <div class="comment-name">Alana Desiree</div> 

                                    <span class="separate">|</span>

                                    <div class="comment-date">02, Junary, 2015</div> 

                                </div>

                                <div class="comment-content">
                                    <p>Lectus neque congue, mi mi natoque vivamus nostra. Cras enim ultricies, commodo sed vivamus. Aute vel feugiat odio in nunc mauris, nunc tortor aenean sed urna, tellus vitae duis urna nunc fringilla, tempus morbi orci vitae</p>
                                </div>

                                <div class="reply"><a class="comment-reply-link" href="#">Reply</a></div> 

                            </div> 
                            <!-- End comment_content -->

                            <ul class="children">
                                <li class="comment">
                                    <!-- comment_content -->
                                    <div class="comment_content">

                                        <div class="comment-avatar">
                                            <img src="http://placehold.it/61x61" alt="image">
                                        </div>

                                        <div class="author-comment">

                                            <div class="comment-name">Morphy Richards</div> 

                                            <span class="separate">|</span>

                                            <div class="comment-date">March 10, 2015 @ 9:57 pm</div> 

                                        </div>

                                        <div class="comment-content">
                                            <p>Lectus neque congue, mi mi natoque vivamus nostra. Cras enim ultricies, commodo sed vivamus. Aute vel feugiat odio in nunc mauris, nunc tortor aenean sed urna, tellus vitae duis urna nunc fringilla, tempus morbi orci vitae</p>
                                        </div>

                                        <div class="reply"><a class="comment-reply-link" href="#">Reply</a></div> 

                                    </div> 
                                    <!-- End comment_content -->

                                    <ul class="children">
                                        <li class="comment">
                                            <!-- comment_content -->
                                            <div class="comment_content">

                                                <div class="comment-avatar">
                                                    <img src="http://placehold.it/61x61" alt="image">
                                                </div>

                                                <div class="author-comment">

                                                    <div class="comment-name">Kiley Felicity</div>

                                                    <span class="separate">|</span>

                                                    <div class="comment-date">March 11, 2015 @ 9:57 pm</div> 

                                                </div>

                                                <div class="comment-content">
                                                    <p>Lectus neque congue, mi mi natoque vivamus nostra. Cras enim ultricies, commodo sed vivamus. Aute vel feugiat odio in nunc mauris, nunc tortor aenean sed urna, tellus vitae duis urna nunc fringilla, tempus morbi orci vitae</p>
                                                </div>

                                                <div class="reply"><a class="comment-reply-link" href="#">Reply</a></div> 

                                            </div> 
                                            <!-- End comment_content -->
                                        </li> 
                                    </ul>

                                </li> 
                            </ul>

                        </li>

                        <li class="comment">
                            <!-- comment_content -->
                            <div class="comment_content">

                                <div class="comment-avatar">
                                    <img src="http://placehold.it/61x61" alt="image">
                                </div>

                                <div class="author-comment">

                                    <div class="comment-name">Gail Kym</div> 

                                    <span class="separate">|</span>

                                    <div class="comment-date">March 12, 2015 @ 9:57 pm</div> 

                                </div>

                                <div class="comment-content">
                                    <p>Lectus neque congue, mi mi natoque vivamus nostra. Cras enim ultricies, commodo sed vivamus. Aute vel feugiat odio in nunc mauris, nunc tortor aenean sed urna, tellus vitae duis urna nunc fringilla, tempus morbi orci vitae</p>
                                </div>

                                <div class="reply"><a class="comment-reply-link" href="#">Reply</a></div> 

                            </div> 
                            <!-- End comment_content -->

                            <ul class="children">
                                <li class="comment">
                                    <!-- comment_content -->
                                    <div class="comment_content">

                                        <div class="comment-avatar">
                                            <img src="http://placehold.it/61x61" alt="image">
                                        </div>

                                        <div class="author-comment">

                                            <div class="comment-name">Chris Martina</div> 

                                            <span class="separate">|</span>

                                            <div class="comment-date">March 13, 2015 @ 9:57 pm</div>  

                                        </div>

                                        <div class="comment-content">
                                            <p>Lectus neque congue, mi mi natoque vivamus nostra. Cras enim ultricies, commodo sed vivamus. Aute vel feugiat odio in nunc mauris, nunc tortor aenean sed urna, tellus vitae duis urna nunc fringilla, tempus morbi orci vitae</p>
                                        </div>

                                        <div class="reply"><a class="comment-reply-link" href="#">Reply</a></div> 

                                    </div> 
                                    <!-- End comment_content -->

                                </li> 
                            </ul>

                        </li>

                        <li class="comment">
                            <!-- comment_content -->
                            <div class="comment_content">

                                <div class="comment-avatar">
                                    <img src="http://placehold.it/61x61" alt="image">
                                </div>

                                <div class="author-comment">

                                    <div class="comment-name">Jose Santonio</div> 

                                    <span class="separate">|</span>

                                    <div class="comment-date">March 14, 2015 @ 9:57 pm</div> 

                                </div>

                                <div class="comment-content">
                                    <p>Lectus neque congue, mi mi natoque vivamus nostra. Cras enim ultricies, commodo sed vivamus. Aute vel feugiat odio in nunc mauris, nunc tortor aenean sed urna, tellus vitae duis urna nunc fringilla, tempus morbi orci vitae</p>
                                </div>

                                <div class="reply"><a class="comment-reply-link" href="#">Reply</a></div> 

                            </div> 
                            <!-- End comment_content -->

                        </li>

                        <li class="comment">
                            <!-- comment_content -->
                            <div class="comment_content">

                                <div class="comment-avatar">
                                    <img src="http://placehold.it/61x61" alt="image">
                                </div>

                                <div class="author-comment">

                                    <div class="comment-name">Brewer Burks</div> 

                                    <span class="separate">|</span>

                                    <div class="comment-date">March 15, 2015 @ 9:57 pm</div> 

                                </div>

                                <div class="comment-content">
                                    <p>Lectus neque congue, mi mi natoque vivamus nostra. Cras enim ultricies, commodo sed vivamus. Aute vel feugiat odio in nunc mauris, nunc tortor aenean sed urna, tellus vitae duis urna nunc fringilla, tempus morbi orci vitae</p>
                                </div>

                                <div class="reply"><a class="comment-reply-link" href="#">Reply</a></div> 

                            </div> 
                            <!-- End comment_content -->

                        </li>

                    </ul><!-- .comments -->


                </section>


                <!-- leave a reply -->

                <section id="leave-reply">

                    <div class="contact-form-us">

                        <div class="block-head">
                            <h3>Leave a REPLY</h3>
                        </div>

                        <p class="comment-notes">Your email address will not be published. Required fields are marked <span class="required">*</span></p>


                        <form action="#" method="post">

                            <p>
                                <span class="your-name">
                                    <input type="text" name="your-name" value="" size="40" class="" placeholder="Your Name * (required)">
                                </span> 
                            </p>

                            <p>
                                <span class="your-email">
                                    <input type="email" name="your-email" value="" size="40" class="" placeholder="Your Email * (required)">
                                </span> 
                            </p>

                            <p><span class="your-website">
                                    <input type="text" name="your-website" value="" size="40" class="" placeholder="Your Website">
                                </span>
                            </p>

                            <p>
                                <span class="your-comment">
                                    <textarea name="your-comment" cols="40" rows="10" class="" placeholder="Your Comment"></textarea>
                                </span> 
                            </p>

                            <p>
                                <input type="submit" value="Post Comment" class="submit-button">
                            </p>

                        </form>


                    </div>

                </section>


            </div>


        </article> 

    </div>
</div>
<!-- #content-wrapper -->
<?php include_once __DIR__ . '/footer.php'; ?>