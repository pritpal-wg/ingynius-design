<?php
$title = "My Groups";
$pic_type = 'square';
$menus = array(
    'index.php' => array(
        'icon' => 'home',
        'label' => 'Home',
    ),
    'index-2.php' => array(
        'icon' => 'home',
        'label' => 'Home 2',
    ),
    'groups.php' => array(
        'icon' => 'home',
        'label' => 'Group Home',
    ),
    'groups_about.php' => array(
        'icon' => 'user',
        'label' => 'About Us',
    ),
    'groups_blog.php' => array(
        'icon' => 'comments',
        'label' => 'Blog',
    ),
    'groups_events.php' => array(
        'icon' => 'calendar',
        'label' => 'Events',
    ),
    'groups_members.php' => array(
        'icon' => 'users',
        'label' => 'Members',
    ),
    'groups_followers.php' => array(
        'icon' => 'users',
        'label' => 'Followers',
    ),
    'groups_contact.php' => array(
        'icon' => 'paper-plane',
        'label' => 'Contact',
    ),
);
$show_quick_icons = FALSE;
?>
<?php include_once __DIR__ . '/header.php' ?>
<div class="col-md-1 bhoechie-tab-menu" style="width:6%;background-image: url('images/sub_nav_menu_bg.png');">
    <div class="list-group">
        <a href="#" class="list-group-item active text-center" data-post_type="a-z">
            <h4 class="fa fa-2x fa-clock-o"></h4><br/>A-Z
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="a-z">
            <h4 class="fa fa-2x fa-user"></h4><br/>Only Mine
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="all posts">
            <h4 class="fa fa-2x fa-square"></h4><br/>Show All
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="media">
            <h4 class="fa fa-2x fa-music"></h4><br/>Media
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="collection">
            <h4 class="fa fa-2x fa-columns"></h4><br/><p style="margin-left: -10px;">Collections</p>
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="events">
            <h4 class="fa fa-2x fa-calendar"></h4><br/>Events 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="blogs">
            <h4 class="fa fa-2x fa-edit"></h4><br/>Blogs
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="Groups">
            <h4 class="fa fa-2x fa-users"></h4><br/>Groups 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="Pages">
            <h4 class="fa fa-2x fa-paste"></h4><br/>Pages 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="favorites">
            <h4 class="fa fa-2x fa-star"></h4><br/>Favorites 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="member">
            <h4 class="fa fa-2x fa-users"></h4><br/>Members
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="settings">
            <h4 class="fa fa-2x fa-cog"></h4><br/>Settings 
        </a>
    </div>
</div>
<div id="all_posts_div" class="col-md-7" style="<?php echo $f_name == 'index.php' ? '' : 'background-color: #fff;'; ?>padding: 5px 5px;width:50%">
    <div class="row-fluid" style="min-height: 600px">
        <div class="col-md-12">
            <h2 class="page-header" style="border:none;margin: 40px 0 20px 0">
                <span class="pull-right"style="margin-right: 15px;">
                    <a href="groups_create.php" class="btn btn-success"><i class="fa fa-plus"></i> Create Group</a>
                </span>
                Groups You Manage
            </h2>
        </div>
        <br/>
        <br/>
        <div class="row-fluid">
            <div class="col-md-12">
                <div id="groups_container">
                    <h3 class="page-container">You do not have any groups yet.</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        populateGroups();
    });
    $(document).on('click', '#delete_group', function () {
        var id = Number($(this).attr('data-id'));
        var groups = $.parseJSON(localStorage.groups);
        var new_groups = [];
        $.each(groups, function (l, v) {
            if (v.id !== id) {
                new_groups.push(v);
            }
        });
        localStorage.groups = JSON.stringify(new_groups);
        $(this).closest('[for="ig_group"]').hide(400, function () {
            $(this).remove();
            if (!$('#groups_container .list-group li[for="ig_group"]').length) {
                $('#groups_container').html('<h3 class="page-container">You do not have any groups yet.</h3>');
            }
        });
    });
    function populateGroups() {
        if (localStorage && localStorage.groups) {
            var groups = $.parseJSON(localStorage.groups);
            if (groups.length) {
                var groups_html = '';
                groups_html += '<ul class="list-group">';
                $.each(groups, function (l, v) {
                    groups_html += '<li class="list-group-item" style="height: 40px;" for="ig_group">';
                    groups_html += '<span class="pull-right">';
                    groups_html += '<a href="" class="text text-info"><i class="fa fa-camera"></i> Add Group Photo</a>';
                    groups_html += '<span class="btn-group" style="margin: 0 10px;">';
                    groups_html += '<button type="button" class="btn btn-default btn-xs"><i class="fa fa-plus"></i> Add to favorites</button>';
                    groups_html += '<a href="groups_setting.php" class="btn btn-default btn-xs"><i class="fa fa-cog"></i></a>';
                    groups_html += '</span>';
                    groups_html += '<button id="delete_group" data-id=' + v.id + ' type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>';
                    groups_html += '</span>';
                    groups_html += '<i class="fa fa-group"></i> ' + v.name;
                    groups_html += '</li>';
                });
                groups_html += '</ul>';
                $('#groups_container').html(groups_html);
            }
        }
    }

</script>
<!-- #content-wrapper -->
<?php include_once __DIR__ . '/footer.php'; ?>