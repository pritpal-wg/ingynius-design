<style type="text/css">
    .carousel {
        margin-bottom: 0;
        padding: 0 40px 30px 40px;
    }

    .carousel-indicators {
        right: 50%;
        top: auto;
        bottom: 0px;
        margin-right: -19px;
    }
    .carousel-indicators li {
        background: #c0c0c0;
    }
    .carousel-indicators .active {
        background: #333333;
    }
</style>
<div class="container" style="margin-bottom: -20px;">
    <div id="myCarousel" class="carousel slide" style="max-width: 40%;">
        <div class="carousel-inner">
            <div class="item active">
                <div class="row-fluid">
                    <?php for ($x = 1; $x <= 4; $x++) { ?>
                        <div class="col-md-3"><a href="#x"><img src="images/cover-1.jpg" alt="Image" style="max-width:100%;height: 50px;width: 50px;" class="img img-circle" class="img img-circle" /></a></div>
                    <?php } ?>
                </div>
            </div>
            <div class="item">
                <div class="row-fluid">
                    <?php for ($x = 1; $x <= 4; $x++) { ?>
                        <div class="col-md-3"><a href="#x"><img src="images/cover-2.jpg" alt="Image" style="max-width:100%;height: 50px;width: 50px;" class="img img-circle" /></a></div>
                    <?php } ?>
                </div>
            </div>
            <div class="item">
                <div class="row-fluid">
                    <?php for ($x = 1; $x <= 4; $x++) { ?>
                        <div class="col-md-3"><a href="#x"><img src="images/cover-3.jpg" alt="Image" style="max-width:100%;height: 50px;width: 50px;" class="img img-circle" /></a></div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <a class="left carousel-control" href="#myCarousel" data-slide="prev"><i class="fa fa-arrow-left" style="margin-top: 12px;"></i></a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next"><i class="fa fa-arrow-right" style="margin-top: 13px;"></i></a>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#myCarousel_slider').carousel({
            interval: 10000
        });
    });
</script>