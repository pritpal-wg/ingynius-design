<?php

function pr($e) {
    echo "<pre>";
    print_r($e);
    echo "</pre>";
}

$f_name = basename($_SERVER['SCRIPT_FILENAME']);
// Get Current FileName
$file = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);
if (!isset($show_quick_icons))
    $show_quick_icons = TRUE;
if (!isset($show_top_menu))
    $show_top_menu = TRUE;
if (!isset($menus)) {
    $menus = array(
        'index.php' => array(
            'icon' => 'home',
            'label' => 'Home',
        ),
        'index-2.php' => array(
            'icon' => 'home',
            'label' => 'Home 2',
        ),
        'profile_wizard.php' => array(
            'icon' => 'user',
            'label' => 'My Profile',
        ),
        'resume.php' => array(
            'icon' => 'tasks',
            'label' => 'Resume',
        ),
        'blog.php' => array(
            'icon' => 'comments',
            'label' => 'Blog',
        ),
        'interests.php' => array(
            'icon' => 'user',
            'label' => 'Interests',
        ),
        'groups_manage.php' => array(
            'icon' => 'users',
            'label' => 'Groups',
        ),
        'company_pages.php' => array(
            'icon' => 'building',
            'label' => 'Pages',
        ),
        'events.php' => array(
            'icon' => 'calendar',
            'label' => 'Events',
        ),
        'friends.php' => array(
            'icon' => 'users',
            'label' => 'Friends',
        ),
        'following.php' => array(
            'icon' => 'user',
            'label' => 'Following',
        ),
        'followers.php' => array(
            'icon' => 'user',
            'label' => 'Followers',
        ),
        'company.php' => array(
            'icon' => 'building',
            'label' => 'Company',
        ),
    );
}
?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
    <head>
        <title><?php echo isset($title) ? $title . ' - ' : '' ?>Ingynius</title>
        <!-- Basic Page Needs -->
        <meta charset="UTF-8" />
        <meta name="description" content="FlexyCodes - FlexyBlog Personal Blog Template. Creating my personal page!"/>
        <meta name="keywords" content="">
        <meta name="author" content="flexycodes">

        <!-- Mobile Specific Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- CSS | bootstrap -->
        <!-- Credits: http://getbootstrap.com/ -->
        <link  rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
        <link  rel="stylesheet" type="text/css" href="css/toastr.css" />

        <!-- CSS | font-awesome -->
        <!-- Credits: http://graphicburger.com/down/?q=simple-line-icons-webfont -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />

        <!-- CSS | simple-line-icons -->
        <!-- Credits: https://github.com/thesabbir/simple-line-icons -->
        <link rel="stylesheet" type="text/css" href="css/simple-line-icons.min.css">

        <!-- CSS | animate -->
        <!-- Credits: http://daneden.me/animate/ -->
        <link rel="stylesheet" type="text/css" href="css/animate.min.css" />

        <!-- CSS | flexslider -->
        <!-- Credits: http://www.woothemes.com/flexslider/ -->
        <link rel="stylesheet" type="text/css" href="css/flexslider.min.css" />

        <!-- CSS | prettyPhoto -->
        <!-- Credits: http://www.no-margin-for-errors.com/ -->
        <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css"/>

        <!-- CSS | Colors -->
        <link rel="stylesheet" type="text/css" href="css/switcher.css" />
        <link rel="stylesheet" type="text/css" href="plugins/file-upload/css/jquery.fileupload.css" />
        <link rel="stylesheet" type="text/css" href="css/colors/color_1.css" id="colors-style" />

        <!-- CSS | Style -->
        <!-- Credits: http://themeforest.net/user/FlexyCodes -->
        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <link rel="stylesheet" type="text/css" href="plugins/chosen/chosen.css" />

        <!-- CSS | Google Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,700,300italic,400italic,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>	
        <link href='http://fonts.googleapis.com/css?family=Russo+One' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700' rel='stylesheet' type='text/css'>
        <link href='plugins/date-picker/datetime-picker.css' rel='stylesheet' type='text/css'>
        <link href='plugins/date-picker/datetime-picker.css' rel='stylesheet' type='text/css'>
        <link href='plugins/slider/slider.css' rel='stylesheet' type='text/css'>
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon/favicon.ico">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <!--[if IE 7]>
                        <link rel="stylesheet" type="text/css" href="css/icons/font-awesome-ie7.min.css"/>
        <![endif]-->

        <!-- jquery | jQuery 1.11.0 -->
        <!-- Credits: http://jquery.com -->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <!-- Js | jquery.ui.effect.js -->

        <!-- Credits: https://jqueryui.com/effect-->
        <script type="text/javascript" src="js/jquery.ui.effect.min.js"></script>

        <!-- Js | bootstrap -->
        <!-- Credits: http://getbootstrap.com/ -->
        <script type="text/javascript" src="js/bootstrap.min.js"></script> 

        <!-- Js | masonry.pkgd.min.js -->
        <!-- Credits: http://masonry.desandro.com -->
        <script type="text/javascript" src="js/masonry.pkgd.min.js"></script>

        <!-- Js | flexslider -->
        <!-- Credits: http://www.woothemes.com/flexslider/ -->
        <script type="text/javascript" src="js/jquery.flexslider-min.js"></script>

        <!-- Js | jquery.cookie -->
        <!-- Credits: https://github.com/carhartl/jquery-cookie --> 
        <script type="text/javascript" src="js/jsSwitcher/jquery.cookie.js"></script>	

        <!-- Js | switcher -->
        <!-- Credits: http://themeforest.net/user/FlexyCodes -->
        <script type="text/javascript" src="js/jsSwitcher/switcher.js"></script>	

        <!-- Js | nicescroll.js -->
        <!-- Credits: http://areaaperta.com/nicescroll/ -->
        <script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>

        <!-- jquery | rotate and portfolio -->
        <!-- Credits: http://jquery.com -->
        <script type="text/javascript" src="js/jquery.mixitup.min.js"></script> 
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">

        <!-- Latest compiled and minified JavaScript -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
        <!-- jquery | prettyPhoto -->
        <!-- Credits: http://www.no-margin-for-errors.com/ -->
        <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
        <script type="text/javascript" src="plugins/chosen/chosen.jquery.js"></script>

        <!-- Js | gmaps -->
        <!-- Credits: http://maps.google.com/maps/api/js?sensor=true-->
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true&key=AIzaSyChvebIlEsJxZCUwiGJe3qoIFvYvWx2REA&signed_in=true&libraries=places"></script>
        <!--<script id="remove_google_js" type="text/javascript" src="js/gmaps.min.js" data-src="js/gmaps.min.js"></script>-->

        <!-- Js | Js -->
        <!-- Credits: http://themeforest.net/user/FlexyCodes -->
        <script type="text/javascript" src="js/jquery.localStorage.js"></script>
        <script type="text/javascript" src="js/toastr.js"></script>
        <script type="text/javascript" src="plugins/file-upload/js/vendor/jquery.ui.widget.js"></script>
        <script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
        <!-- The Canvas to Blob plugin is included for image resizing functionality -->
        <script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
        <!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="plugins/date-picker/moment.js"></script>
        <script type="text/javascript" src="plugins/date-picker/datetime-picker.js"></script>
        <script type="text/javascript" src="plugins/slider/slider.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker12').datetimepicker({
                    inline: true,
                    sideBySide: false
                });
            });
        </script>
        <script type="text/javascript" src="js/user_posts.js"></script>
        <script type="text/javascript" src="js/main.js"></script>
        <style>
            body{	
                background-image: url('images/cover-1.jpg');
            }

            .main-pagination {
                overflow: hidden;
                text-align: center;
                margin: 30px 0 30px 0;
                padding-right: 10px;
            }

            .masonry_load_more {
                padding: 5px 0;
                cursor: pointer;
                background: #000;
                border-radius: 5px;
            }

            .masonry_load_more:hover {
                background: #fff;
                color: #010101;
            }
            .latest_img_slider {
                width: 100% !important;
                height: 200px !important;
            }
            .events_img {
                width: 100% !important;
                height: 50px !important;
            }
            .post_title_cl {
                text-align: center;
                font-weight: bold;
            }
            .post_result form.form.form-horizontal {
                margin-top: -15px;
            }
            #post_area .col-md-2 {
                width: 18.666667% !important;
            }
            #url {width: 100%;padding: 10px;border: 1px solid #F0F0F0;}
            #output{display:none;border: 1px solid #F0F0F0;overflow:hidden;padding:10px;}
            .image-extract{max-width:580px;text-align:center;}
            .btnNav{width:26px;height:26px;border:0;cursor:pointer;margin-top:10px}
            #prev-extract {background:url('previous.jpg');}
            #next-extract {background:url('next.jpg');}
            #prev-extract:disabled {opacity:0.5;}
            #next-extract:disabled {opacity:0.5;}
        </style>
    </head>
    <body>
        <!--[if lt IE 7]>
                        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <!-- Laoding page  -->
        <div class="preloader">
            <div class="spinner">
                <div class="dot1"></div>
                <div class="dot2"></div>
            </div>
        </div>
        <!-- End Laoding page  -->
        <!-- .wrapper-content-->
        <div class="wrapper-content">
            <?php if ($show_top_menu) { ?>
                <div id="content-wrapper" class="menu_content_wrapper<?php echo isset($_COOKIE['menu_layout']) && $_COOKIE['menu_layout'] ? ' inverse' : '' ?>">
                    <!--<div class="top-menu-bg"></div>-->
                    <div id="main-content" style="margin: 10px;margin-bottom: 0">
                        <ul class="top-menu transition_200">
                            <li>
                                <a href="#"><img class="menu-icon" src="images/logout-icon.png" /></a>
                            </li>
                            <li>
                                <a href="index.php" class="<?php echo $file == 'index.php' ? 'top-menu-active' : '' ?>">Home</a>
                            </li>
                            <li>
                                <a href="company.php">Company</a>
                            </li>
                            <li>
                                <a href="company_pages.php">Pages</a>
                            </li>
                            <li>
                                <a href="groups_create.php">Groups</a>
                            </li>
                            <li>
                                <a href="#"><img class="ig-icon" src="images/logo-icon.png" /></a>
                            </li>
                            <li>
                                <a href="event_photos.php">Events</a>
                            </li>
                            <li>
                                <a href="blog.php">Blogs</a>
                            </li>
                            <li>
                                <a href="about.php">About</a>
                            </li>
                            <li>
                                <a href="profile_wizard.php">Account</a>
                            </li>
                            <li>
                                <a href="#"><img class="menu-icon" src="images/share-icon.png" /></a>
                            </li>
                            <li style="position: absolute;margin-top: 5px">
                                <a href="#" id="trigger_click" title="Color Switcher"><span class="changebutton"><i class="fa fa-2x fa-cog colors-color"></i></span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            <?php } ?>
            <div class="col-md-12" style="top:70px;">
                <div class="col-md-2" style="width: 22%">
                    <header id="header" class="header_1 content_3">
                        <div class="logo <?php echo isset($pic_type) && $pic_type == 'square' ? 'logo-square' : 'logo-circle' ?>">
                            <a href="#"><img src="http://lorempixel.com/<?php echo isset($pic_type) && $pic_type == 'square' ? '900/600' : '300/300' ?>/people" alt="Logo"></a>
                        </div> <!--/ .logo -->
                        <h3 style="text-align: center" class="colors-color">John Doe</h3>
                        <h4 class="tagline">This is a lovely area for the tagline</h4>
                        <?php if ($show_quick_icons === TRUE) { ?>
                            <div class="row profile-links">
                                <div class="col-md-1"></div>
                                <div class="col-md-2 profile-link">
                                    <a href="" title="Message"><i class="fa fa-2x fa-envelope"></i></a>
                                </div>
                                <div class="col-md-2 profile-link">
                                    <a href="" title="Follow"><i class="fa fa-2x fa-comments"></i></a>
                                </div>
                                <div class="col-md-2 profile-link">
                                    <a href="" title="Add"><i class="fa fa-2x fa-plus-circle"></i></a>
                                </div>
                                <div class="col-md-2 profile-link">
                                    <a href="" title="Video Chat"><i class="fa fa-2x fa-video-camera"></i></a>
                                </div>
                                <div class="col-md-2 profile-link">
                                    <a href="" title="Video Chat"><i class="fa fa-2x fa-gift"></i></a>
                                </div>
                                <div class="col-md-1"></div>
                            </div>
                        <?php } ?>
                        <div class="row">
                            <div class="col-md-12">
                                <h5 class="post_title_cl colors-color">
                                    About Me
                                </h5>
                            </div>
                            <div class="col-md-12" style="margin-top: 10px;margin-bottom: 10px">
                                <div class="col-md-6" style="text-align: center;">
                                    <div class="colors-color">Work:</div>
                                    <div class="colors-color">Live:</div>
                                    <div class="colors-color">Hometown:</div>
                                    <div class="colors-color">Birthday:</div>
                                </div>
                                <div class="col-md-6" style="text-align: center;color:#fff;">
                                    <div class="">XYZ Inc.</div>
                                    <div class="">Anytown, USA</div>
                                    <div class="">Foreign, EU</div>
                                    <div class="">July 8th</div>
                                </div>
                                <div class="col-md-4" style="text-align: center;color:#fff;margin-top: 10px;">
                                    <div class="colors-color-hover">
                                        <a href="interests.php">
                                            <i class="fa fa-user"></i><br/>
                                            Interests
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4" style="text-align: center;color:#fff;margin-top: 10px;">
                                    <div class="colors-color-hover">
                                        <a href="resume.php">
                                            <i class="fa fa-list"></i><br/>
                                            Resume
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4" style="text-align: center;color:#fff;margin-top: 10px;">
                                    <div class="colors-color-hover">
                                        <a href="contact.php">
                                            <i class="fa fa-paper-plane"></i><br/>
                                            Contact
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-12" style="text-align: center;color:#fff;margin-top: 10px;">
                                    <button class="btn btn-default btn-xs">More</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="margin-top: 10px">
                                <h5 class="post_title_cl colors-color">
                                    ONLINE
                                </h5>
                            </div>
                            <div class="col-md-12" style="margin-bottom: 10px">
                                <!-- Nav tabs -->
                                <div class="card" style="box-shadow: none">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#online_newest" aria-controls="home" role="tab" data-toggle="tab">Newest</a></li>
                                        <li role="presentation"><a href="#online_featured" aria-controls="profile" role="tab" data-toggle="tab">Featured</a></li>
                                        <li role="presentation"><a href="#online_active" aria-controls="messages" role="tab" data-toggle="tab">Active</a></li>
                                        <li role="presentation"><a href="#online_all" aria-controls="settings" role="tab" data-toggle="tab">See All</a></li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="online_newest">
                                            <div class="row-fluid">
                                                <?php for ($i = 0; $i < 3; $i++) { ?>
                                                    <div class="col-md-4" style="margin-bottom: 10px">
                                                        <img style="width: 50px;height: 40px" src="images/cover-1.jpg" class="img img-responsive img-circle"/>
                                                    </div>
                                                    <div class="col-md-4" style="margin-bottom: 10px">
                                                        <img style="width: 50px;height: 40px" src="images/cover-2.jpg" class="img img-responsive img-circle"/>
                                                    </div>
                                                    <div class="col-md-4" style="margin-bottom: 10px">
                                                        <img style="width: 50px;height: 40px" src="images/cover-3.jpg" class="img img-responsive img-circle"/>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="online_featured">
                                            <div class="row-fluid">
                                                <?php for ($i = 0; $i < 3; $i++) { ?>
                                                    <div class="col-md-4" style="margin-bottom: 10px">
                                                        <img style="width: 50px;height: 40px" src="images/cover-2.jpg" class="img img-responsive img-circle"/>
                                                    </div>
                                                    <div class="col-md-4" style="margin-bottom: 10px">
                                                        <img style="width: 50px;height: 40px" src="images/cover-1.jpg" class="img img-responsive img-circle"/>
                                                    </div>
                                                    <div class="col-md-4" style="margin-bottom: 10px">
                                                        <img style="width: 50px;height: 40px" src="images/cover-3.jpg" class="img img-responsive img-circle"/>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="online_active">
                                            <div class="row-fluid">
                                                <?php for ($i = 0; $i < 3; $i++) { ?>
                                                    <div class="col-md-4" style="margin-bottom: 10px">
                                                        <img style="width: 50px;height: 40px" src="images/cover-1.jpg" class="img img-responsive img-circle"/>
                                                    </div>
                                                    <div class="col-md-4" style="margin-bottom: 10px">
                                                        <img style="width: 50px;height: 40px" src="images/cover-3.jpg" class="img img-responsive img-circle"/>
                                                    </div>
                                                    <div class="col-md-4" style="margin-bottom: 10px">
                                                        <img style="width: 50px;height: 40px" src="images/cover-2.jpg" class="img img-responsive img-circle"/>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="online_all">
                                            <div class="row-fluid">
                                                <?php for ($i = 0; $i < 3; $i++) { ?>
                                                    <div class="col-md-4" style="margin-bottom: 10px">
                                                        <img style="width: 50px;height: 40px" src="images/cover-3.jpg" class="img img-responsive img-circle"/>
                                                    </div>
                                                    <div class="col-md-4" style="margin-bottom: 10px">
                                                        <img style="width: 50px;height: 40px" src="images/cover-2.jpg" class="img img-responsive img-circle"/>
                                                    </div>
                                                    <div class="col-md-4" style="margin-bottom: 10px">
                                                        <img style="width: 50px;height: 40px" src="images/cover-1.jpg" class="img img-responsive img-circle"/>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <h5 class="post_title_cl colors-color">
                                        GROUPS
                                    </h5>
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#groups_newest" aria-controls="home" role="tab" data-toggle="tab">Latest</a></li>
                                        <li role="presentation"><a href="#groups_featured" aria-controls="profile" role="tab" data-toggle="tab">Featured</a></li>
                                        <li role="presentation"><a href="#groups_popular" aria-controls="messages" role="tab" data-toggle="tab">Popular</a></li>
                                        <li role="presentation"><a href="#groups_all" aria-controls="settings" role="tab" data-toggle="tab">See All</a></li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="groups_newest">
                                            <div class="row-fluid">
                                                <div class="demo">
                                                    <ul id="temp_lightSlider1">
                                                        <li data-thumb="images/cover-1.jpg">
                                                            <img src="images/cover-1.jpg" />
                                                        </li>
                                                        <li data-thumb="images/cover-2.jpg">
                                                            <img src="images/cover-2.jpg" />
                                                        </li>
                                                        <li data-thumb="images/cover-3.jpg">
                                                            <img src="images/cover-3.jpg" />
                                                        </li>
                                                        <li data-thumb="images/cover-4.jpg">
                                                            <img src="images/cover-4.jpg" />
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="groups_featured">
                                            <div class="row-fluid">
                                                <div class="demo">
                                                    <ul id="temp_lightSlider">
                                                        <li data-thumb="images/cover-3.jpg">
                                                            <img src="images/cover-3.jpg" />
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="groups_popular">
                                            <div class="row-fluid">
                                                <div class="demo">
                                                    <ul id="temp_lightSlider">
                                                        <li data-thumb="images/cover-1.jpg">
                                                            <img src="images/cover-1.jpg" />
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="groups_all">
                                            <div class="row-fluid">
                                                <div class="demo">
                                                    <ul id="temp_lightSlider">
                                                        <li data-thumb="images/cover-2.jpg">
                                                            <img src="images/cover-2.jpg" />
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <h5 class="post_title_cl colors-color">
                                        PAGES
                                    </h5>
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#pages_newest" aria-controls="home" role="tab" data-toggle="tab">Latest</a></li>
                                        <li role="presentation"><a href="#pages_featured" aria-controls="profile" role="tab" data-toggle="tab">Featured</a></li>
                                        <li role="presentation"><a href="#pages_popular" aria-controls="messages" role="tab" data-toggle="tab">Popular</a></li>
                                        <li role="presentation"><a href="#pages_all" aria-controls="settings" role="tab" data-toggle="tab">See All</a></li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="pages_newest">
                                            <div class="row-fluid">
                                                <div class="demo">
                                                    <ul id="temp2_lightSlider1">
                                                        <li data-thumb="images/cover-1.jpg">
                                                            <img src="images/cover-1.jpg" />
                                                        </li>
                                                        <li data-thumb="images/cover-2.jpg">
                                                            <img src="images/cover-2.jpg" />
                                                        </li>
                                                        <li data-thumb="images/cover-3.jpg">
                                                            <img src="images/cover-3.jpg" />
                                                        </li>
                                                        <li data-thumb="images/cover-4.jpg">
                                                            <img src="images/cover-4.jpg" />
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="pages_featured">
                                            <div class="row-fluid">
                                                <div class="demo">
                                                    <ul id="temp2_lightSlider">
                                                        <li data-thumb="images/cover-3.jpg">
                                                            <img src="images/cover-3.jpg" />
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="pages_popular">
                                            <div class="row-fluid">
                                                <div class="demo">
                                                    <ul id="temp2_lightSlider">
                                                        <li data-thumb="images/cover-1.jpg">
                                                            <img src="images/cover-1.jpg" />
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="pages_all">
                                            <div class="row-fluid">
                                                <div class="demo">
                                                    <ul id="temp2_lightSlider">
                                                        <li data-thumb="images/cover-4.jpg">
                                                            <img src="images/cover-4.jpg" />
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="copyright">
                            <p style="margin-bottom:50px;">Copyright &copy; <?php echo date('Y') . ' - ' . date('Y', strtotime(date('Y') . '+ 1 Year')) ?>.</p>		
                        </div>
                    </header>
                </div>