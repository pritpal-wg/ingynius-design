<?php
$title = "Event Videos";
$menus = array(
    'index.php' => array(
        'icon' => 'home',
        'label' => 'Home',
    ),
    'index-2.php' => array(
        'icon' => 'home',
        'label' => 'Home 2',
    ),
    'event_details.php' => array(
        'icon' => 'file-text',
        'label' => 'Details',
    ),
    'event_register_ticket.php' => array(
        'icon' => 'edit',
        'label' => 'Register Tickets',
    ),
    'event_photos.php' => array(
        'icon' => 'image',
        'label' => 'Photos',
    ),
    'event_videos.php' => array(
        'icon' => 'video-camera',
        'label' => 'Videos',
    ),
    'event_attending.php' => array(
        'icon' => 'users',
        'label' => 'Who\'s Attending',
    ),
    'event_contact.php' => array(
        'icon' => 'paper-plane',
        'label' => 'Contact',
    ),
);
$show_quick_icons = FALSE;
?>
<?php include_once __DIR__ . '/header.php' ?>
<style>
    #main-content {
        padding: 0;
        margin: 10px;
    }
</style>
<div id="content-wrapper">
    <div id="main-content">
        <section class="blog-content-grid">
            <div class="row">
                <?php for ($i = 0; $i < 12; $i++) { ?>
                    <!-- post -->
                    <div class="col-md-3 col-sm-6">
                        <div class="item">
                            <article>
                                <div class="post-thumb responsive_video">
                                    <iframe src="http://www.youtube.com/embed/xaSH0d60Zso"></iframe>
                                </div>
                                <div class="post-body">
                                    <h3 class="post-title"><a href="blog_single.php">Event Video <?php echo $i + 1 ?></a></h3>
                                    <div class="post-meta">
                                        <ul>
                                            <li>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></li>
                                            <li class="separate_li">|</li>
                                            <li><i class="icon-clock"></i>January 18, 2015</li>
                                            <li class="separate_li">|</li>
                                            <li><i class="icon-folder"></i><a href="#" rel="category tag">WordPress</a></li>
                                        </ul>
                                    </div>
                                    <div class="post-content">
                                        <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat. Mauris sit amet sapien eget lacus...</p>
                                    </div>
                                    <a href="blog_single.php" class="read_more_but">Continue Reading</a>
                                    <div class="footer_post">
                                        <ul>
                                            <li><i class="icon-social-youtube"></i></li>
                                            <li><i class="icon-bubble"></i> <a href="#">4</a></li>
                                            <li><i class="icon-heart"></i> <a href="#">35</a></li>
                                            <li><i class="icon-eye"></i> 216</li>
                                        </ul>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                    <!-- //post -->
                <?php } ?>
            </div>
        </section>
        <div class="main-pagination">
            <span class="page-numbers current">1</span>
            <a class="page-numbers" href="#">2</a>
            <a class="page-numbers" href="#">3</a>
            <a class="page-numbers" href="#">4</a>
            <a class="next page-numbers" href="#">
                <span class="visuallyhidden">Next</span><i class="fa fa-angle-right"></i>
            </a>	
        </div>
    </div>

</div>
<?php include_once __DIR__ . '/footer.php'; ?>