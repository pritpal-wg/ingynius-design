<?php $title = "Connect" ?>
<?php include_once __DIR__ . '/header.php' ?>
<style>
    #main-content {
        padding: 0;
        margin: 10px;
    }
</style>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
    $(document).ready(function () {
        $('#profile-panel-toggle').click().hide();
        $('#btn_sidebar_wrapper').hide();
        $("#dob").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'DD, d MM, yy',
        });
    });
</script>
<!-- content -->
<div id="content-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="col-md-6" style="border-right: dashed 1px grey;height: 100%">
                    <h2 class="page-header" style="text-align: center">Signup</h2>
                    <form class="form-horizontal" method="post">
                        <div class="form-group">
                            <label for="fname" class="col-sm-2 control-label">First Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="fname" id="fname" placeholder="First Name" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="lname" class="col-sm-2 control-label">Last Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="lname" id="lname" placeholder="Last Name" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" name="email" id="email`" placeholder="Valid Email Id" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="phone" class="col-sm-2 control-label">Phone</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="phone" placeholder="Phone Number" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Gender</label>
                            <div class="col-sm-10">
                                <label class="checkbox-inline">
                                    <input type="radio" id="gender" name="gender" value="m" checked /> Male
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" id="gender" name="gender" value="f" /> Female
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="dob" class="col-sm-2 control-label">Date of Birth</label>
                            <div class="col-sm-10">
                                <input type="text" name="dob" id="dob" class="form-control" placeholder="Select Date of Birth" readonly />
                            </div>
                        </div>
                        <hr />
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <!--<button type="submit" class="btn btn-default">Sign up</button>-->
                                <a href="index.php" class="btn btn-default">Sign up</a>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-6">
                    <h2 class="page-header" style="text-align: center">Login</h2>
                    <form class="form-horizontal" method="post">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"> Remember me
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <!--<button type="submit" class="btn btn-default">Sign in</button>-->
                                <a href="index.php" class="btn btn-default">Sign in</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #content-wrapper -->
<?php include_once __DIR__ . '/footer.php'; ?>