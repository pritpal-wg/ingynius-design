<?php
$title = "My Profile";
?>
<?php include_once __DIR__ . '/header.php' ?>
<style>
    #main-content {
        padding: 0;
        margin: 0;
    }
    a.list-group-item.active, a.list-group-item.active:hover, a.list-group-item.active:focus {
        z-index: 1 !important;
    }
    a.list-group-item {
        color: #fff;
    }
    .list-group-item {
        position: relative;
        display: block;
        padding: 10px 15px;
        margin-bottom: -1px;
        background-color: transparent;
        border: 0px solid #ddd;
    }
    a.list-group-item:hover, a.list-group-item:focus {
        text-decoration: none;
        background-color: transparent;
    }
    h4.fa {
        font-size: 35px;
    }
</style>
<!-- content -->
<div class="col-md-1 bhoechie-tab-menu" style="width:6%;background-image: url('images/sub_nav_menu_bg.png');">
    <div class="list-group">
        <a href="#" class="list-group-item active text-center">
            <h4 class="fa fa-2x fa-user"></h4><br/>Import Profile
        </a>
        <a href="#" class="list-group-item text-center">
            <h4 class="fa fa-2x fa-info"></h4><br/>About Me
        </a>
        <a href="#" class="list-group-item text-center">
            <h4 class="fa fa-2x fa-book"></h4><br/>Education
        </a>
        <a href="#" class="list-group-item text-center">
            <h4 class="fa fa-2x fa-history"></h4><br/>Work History 
        </a>
        <a href="#" class="list-group-item text-center">
            <h4 class="fa fa-2x fa-users"></h4><br/>Family & Relationships 
        </a>
        <a href="#" class="list-group-item text-center">
            <h4 class="fa fa-2x fa-eye"></h4><br/>Interests 
        </a>
        <a href="#" class="list-group-item text-center">
            <h4 class="fa fa-2x fa-map-marker"></h4><br/>Invite Friends 
        </a>
        <a href="#" class="list-group-item text-center">
            <h4 class="fa fa-2x fa-cogs"></h4><br/>Privacy Settings 
        </a>
    </div>
</div>
<div id="all_posts_div" class="col-md-7" style="<?php echo $f_name == 'index.php' ? '' : 'background-color: #fff;'; ?>padding: 5px 5px;width:50%">
    <section id="service" class="layers" style="margin-left: -20px;">
        <div class="page_content">
            <!-- .container-fluid -->
            <div class="container-fluid no-marg">
                <!-- .row -->
                <div class="row row_responsive">
                    <!-- .section_general -->
                    <div class="col-lg-12">
                        <div class="row section_separate"> 
                            <div class="col-md-12 bhoechie-tab-container">
                                <div class="col-md-12 profile-bg"></div>
                                <div class="col-md-12 bhoechie-tab" style="min-height: 529px;">
                                    <!-- flight section -->
                                    <div class="bhoechie-tab-content active">
                                        <header class="section-header" style="padding: 0 0 20px 0;">
                                            <h3 class="section-title">Import Profile</h3>
                                            <p> Would you like to import your profile from other social networks. We can import information from your old profile and try to complete your profile.</p>
                                            <div class="border-divider"></div>
                                        </header>
                                        <center>
                                            <div class="row-fluid" style="margin-bottom: 20px">
                                                <div id="timelineContainer">
                                                    <center>
                                                        <form id="bgimageform" method="post" enctype="multipart/form-data" action="image_upload_ajax_bg.php">
                                                            <div id="timelineProfilePic">
                                                                <div id="show_upload_icon" style="padding:5px">
                                                                    <center>
                                                                        <div class="uploadFile timelineUploadBG" style="display:none">
                                                                            <input type="file" name="photoimg" id="bgphotoimg" class="custom-file-input" original-title="Change Cover Picture">
                                                                        </div>
                                                                        <img src="images/profile_img.jpg" style="width:100%">
                                                                    </center>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </center>



                                                    <!-- timeline title -->
                                                    <div id="timelineTitle">John Doe</div>
                                                </div>
                                            </div>
                                            <div class="row-fluid" style="margin-bottom: 20px">
                                                <div class="col-md-6">
                                                    <a href="" class="col-md-12 btn" style="padding: 15px;font-size: 35px;background-color: #3b5998;color:#fff;">
                                                        <i class="fa fa-facebook"></i>
                                                        Facebook
                                                    </a>
                                                </div>
                                                <div class="col-md-6">
                                                    <a href="" class="col-md-12 btn" style="padding: 15px;font-size: 35px;background-color: #1dcaff;color:#fff;">
                                                        <i class="fa fa-twitter"></i>
                                                        Twitter
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="row-fluid">
                                                <div class="col-md-6">
                                                    <a href="" class="col-md-12 btn btn-success" style="padding: 15px;font-size: 35px;margin-top: 5px;background-color: #dd4b39;color:#fff">
                                                        <i class="fa fa-google-plus"></i>
                                                        Google +
                                                    </a>
                                                </div>
                                                <div class="col-md-6">
                                                    <a href="" class="col-md-12 btn btn-success" style="padding: 15px;font-size: 35px;margin-top: 5px;background-color: #007bb6;color:#fff">
                                                        <i class="fa fa-linkedin"></i>
                                                        LinkedIn
                                                    </a>
                                                </div>
                                            </div>
                                        </center>
                                    </div>
                                    <!-- train section -->
                                    <div class="bhoechie-tab-content">
                                        <header class="section-header" style="padding: 0 0 20px 0;">
                                            <h3 class="section-title">About Me</h3>
                                            <div class="border-divider"></div>
                                        </header>
                                        <div class="row-fluid">
                                            <form class="form-horizontal">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label" for="exampleInputAmount">First Name</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-users"></i></div>
                                                            <input type="text" class="form-control" id="exampleInputAmount" placeholder="First Name">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group" style="margin-left: 5px">
                                                        <label class="control-label" for="exampleInputAmount">Last Name</label>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" id="exampleInputAmount" placeholder="Last Name">
                                                            <div class="input-group-addon"><i class="fa fa-users"></i></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label" for="exampleInputAmount">Email Address</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-at"></i></div>
                                                            <input type="text" class="form-control" id="exampleInputAmount" placeholder="Email@Email.Com">
                                                            <div title="Verify Email" class="input-group-addon" style="    background-color: #3b5998;color: #fff;border: 1px;"><i class="fa fa-check-circle"></i></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label" for="exampleInputAmount">Phone Number</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-phone"></i></div>
                                                            <input type="number" class="form-control" id="exampleInputAmount" placeholder="1234567890">
                                                            <div title="Verify Phone Number" class="input-group-addon" style="    background-color: #3b5998;color: #fff;border: 1px;"><i class="fa fa-check-circle"></i></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label" for="exampleInputAmount">Date</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-birthday-cake"></i></div>
                                                            <input type="number" class="form-control" id="exampleInputAmount" placeholder="DD">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group" style="margin-left: 5px">
                                                        <label class="control-label" for="exampleInputAmount">Month</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"></div>
                                                            <input type="number" class="form-control" id="exampleInputAmount" placeholder="MM">
                                                            <div class="input-group-addon"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group" style="margin-left: 5px">
                                                        <label class="control-label" for="exampleInputAmount">Year</label>
                                                        <div class="input-group">
                                                            <input type="number" class="form-control" id="exampleInputAmount" placeholder="YYYY">
                                                            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label" for="exampleInputAmount">Gender</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                                            <select class="form-control">
                                                                <option>Male</option>
                                                                <option>Female</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label" for="exampleInputAmount">About</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-info"></i></div>
                                                            <textarea class="form-control" rows="3"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label" for="exampleInputAmount">Current Location</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
                                                            <input type="text" class="form-control" id="exampleInputAmount" placeholder="Australia">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12" style="padding-left: 0;margin-bottom: 20px;">
                                                    <a href="" class="btn btn-success">
                                                        Save
                                                    </a>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                    <!-- hotel search -->
                                    <div class="bhoechie-tab-content">
                                        <header class="section-header" style="padding: 0 0 20px 0;">
                                            <h3 class="section-title">Education</h3>
                                            <div class="border-divider"></div>
                                        </header>
                                        <div class="row-fluid">
                                            <form class="form-horizontal">
                                                <div class="col-md-12">
                                                    <div class="pull-right">
                                                        <button type="button" class="btn btn-success" id="add_more_education">
                                                            <i class="fa fa-plus"></i>
                                                            Add Education
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label" for="exampleInputAmount">School</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-graduation-cap"></i></div>
                                                            <input type="text" class="form-control" id="exampleInputAmount" placeholder="School / College Name">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label" for="exampleInputAmount">Dates Attended</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                                            <input type="date" class="form-control" id="exampleInputAmount" placeholder="YYYY">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group" style="margin-left: 5px">
                                                        <label class="control-label" for="exampleInputAmount">&nbsp;</label>
                                                        <div class="input-group">
                                                            <input type="date" class="form-control" id="exampleInputAmount" placeholder="YYYY">
                                                            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label" for="exampleInputAmount">Degree</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                                            <select class="form-control">Other<option value="none" selected="">-</option>
                                                                <option value="High School">High School</option>
                                                                <option value="Associate’s Degree">Associate’s Degree</option>
                                                                <option value="Bachelor’s Degree">Bachelor’s Degree</option>
                                                                <option value="Master’s Degree">Master’s Degree</option>
                                                                <option value="Master of Business Administration (M.B.A.)">Master of Business Administration (M.B.A.)</option>
                                                                <option value="Juris Doctor (J.D.)">Juris Doctor (J.D.)</option>
                                                                <option value="Doctor of Medicine (M.D.)">Doctor of Medicine (M.D.)</option>
                                                                <option value="Doctor of Philosophy (Ph.D.)">Doctor of Philosophy (Ph.D.)</option>
                                                                <option value="Engineer’s Degree">Engineer’s Degree</option>
                                                                <option value="other">Other</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label" for="exampleInputAmount">Field Of Study</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-book"></i></div>
                                                            <input type="text" class="form-control" id="exampleInputAmount" placeholder="B.Tech">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label" for="exampleInputAmount">Grade</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-trophy"></i></div>
                                                            <input type="text" class="form-control" id="exampleInputAmount" placeholder="Grade">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label" for="exampleInputAmount">Activities & Societies</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-eye"></i></div>
                                                            <textarea placeholder="Examples: Alpha Phi Omega, Chamber Chorale, Debate Team" class="form-control" rows="3"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label" for="exampleInputAmount">Description</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-info"></i></div>
                                                            <textarea class="form-control" rows="3"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="append_fields"></div>
                                                <div class="col-md-12" style="padding-left: 0;margin-bottom: 20px;">
                                                    <a href="" class="btn btn-success">
                                                        Save
                                                    </a>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="bhoechie-tab-content">
                                        <header class="section-header" style="padding: 0 0 20px 0;">
                                            <h3 class="section-title">Work History</h3>
                                            <div class="border-divider"></div>
                                        </header>
                                        <div class="row-fluid">
                                            <form class="form-horizontal">
                                                <div class="col-md-12">
                                                    <div class="pull-right">
                                                        <button type="button" class="btn btn-success" id="add_more_work">
                                                            <i class="fa fa-plus"></i>
                                                            Add Work
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label" for="exampleInputAmount">Company Name</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-building"></i></div>
                                                            <input type="text" class="form-control" id="exampleInputAmount" placeholder="Company Name">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label" for="exampleInputAmount">Title</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-list-alt"></i></div>
                                                            <input type="text" class="form-control" id="exampleInputAmount" placeholder="Title">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label" for="exampleInputAmount">Location</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
                                                            <input type="text" class="form-control" id="exampleInputAmount" placeholder="Australia">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label" for="exampleInputAmount">Time Period</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                                            <input type="date" class="form-control" id="exampleInputAmount" placeholder="YYYY">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group" style="margin-left: 5px">
                                                        <label class="control-label" for="exampleInputAmount">&nbsp;</label>
                                                        <div class="input-group">
                                                            <input type="date" class="form-control" id="exampleInputAmount" placeholder="YYYY">
                                                            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">&nbsp;</div>
                                                <div class="col-md-6">
                                                    <div class="checkbox" style="margin-left: 5px">
                                                        <label>
                                                            <input type="checkbox" value="">
                                                            I Currently Work Here.
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label" for="exampleInputAmount">Description</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-info"></i></div>
                                                            <textarea class="form-control" rows="3"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="append_work_fields"></div>
                                                <div class="col-md-12" style="padding-left: 0;margin-bottom: 20px;">
                                                    <a href="" class="btn btn-success">
                                                        Save
                                                    </a>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="bhoechie-tab-content">
                                        <header class="section-header" style="padding: 0 0 20px 0;">
                                            <h3 class="section-title">Family & Relationships</h3>
                                            <div class="border-divider"></div>
                                        </header>
                                        <div class="row-fluid">
                                            <form class="form-horizontal">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label" for="exampleInputAmount">Relationship</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-heart-o"></i></div>
                                                            <select class="form-control">
                                                                <option value="0">---</option>
                                                                <option value="1">Single</option>
                                                                <option value="2">In a relationship</option>
                                                                <option value="5">Engaged</option><option value="4">Married</option>

                                                                <option value="8">Separated</option>
                                                                <option value="9">Divorced</option>
                                                                <option value="7">Widowed</option></select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label" for="exampleInputAmount">Family Members</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-users"></i></div>
                                                            <input type="text" name="currency" class="biginput form-control autocomplete" id="" placeholder="Type User Keyword Here..">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label" for="exampleInputAmount"></label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-retweet"></i></div>
                                                            <select class="form-control">
                                                                <option value="0">Choose Relationship:</option>
                                                                <option value="102">Mother</option>
                                                                <option value="103">Father</option>
                                                                <option value="104">Daughter</option>
                                                                <option value="105">Son</option>
                                                                <option value="100">Sister</option>
                                                                <option value="101">Brother</option>
                                                                <option value="107">Auntie</option>
                                                                <option value="106">Uncle</option>
                                                                <option value="108">Niece</option>
                                                                <option value="115">Nephew</option>
                                                                <option value="110">Cousin (female)</option>
                                                                <option value="109">Cousin (male)</option>
                                                                <option value="111">Grandmother</option>
                                                                <option value="112">Grandfather</option>
                                                                <option value="114">Granddaughter</option>
                                                                <option value="113">Grandson</option>
                                                                <option value="165">Stepsister</option>
                                                                <option value="166">Stepbrother</option>
                                                                <option value="167">Stepmother</option>
                                                                <option value="168">Stepfather</option>
                                                                <option value="169">Stepdaughter</option>
                                                                <option value="170">Stepson</option>
                                                                <option value="175">Sister-in-law</option>
                                                                <option value="176">Brother-in-law</option>
                                                                <option value="177">Mother-in-law</option>
                                                                <option value="178">Father-in-law</option>
                                                                <option value="179">Daughter-in-law</option>
                                                                <option value="180">Son-in-law</option>
                                                                <option value="225">Sibling</option>
                                                                <option value="226">Parent</option>
                                                                <option value="227">Child</option>
                                                                <option value="228">Sibling of Parent</option>
                                                                <option value="229">Child of Sibling</option>
                                                                <option value="230">Cousin</option>
                                                                <option value="231">Grandparent</option>
                                                                <option value="232">Grandchild</option>
                                                                <option value="234">Step-sibling</option>
                                                                <option value="235">Step-parent</option>
                                                                <option value="236">Stepchild</option>
                                                                <option value="237">Sibling-in-law</option>
                                                                <option value="238">Parent-in-law</option>
                                                                <option value="239">Child-in-law</option>
                                                                <option value="242">Pet</option></select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12" style="padding-left: 0;margin-bottom: 20px;">
                                                    <a href="" class="btn btn-success">
                                                        Save
                                                    </a>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="bhoechie-tab-content">
                                        <form class="form-horizontal">
                                            <header class="section-header" style="padding: 0 0 20px 0;">
                                                <h3 class="section-title">Interests</h3>
                                                <div class="border-divider"></div>
                                            </header>
                                            <div class="row-fluid">
                                                <div class="col-md-11">
                                                    <div class="form-group">
                                                        <label class="control-label" for="exampleInputAmount">Add Interests</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-eye"></i></div>
                                                            <input type="text" name="currency" class="biginput form-control autocomplete" id="" placeholder="Type User Keyword Here..">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <label class="control-label" for="exampleInputAmount">&nbsp;</label>
                                                    <div class="input-group">
                                                        <button type="button" class="btn btn-primary" id="add_more_interests"><i class="fa fa-plus"></i></button>
                                                    </div>
                                                </div>
                                                <div id="append_intrest_fields"></div>
                                                <div class="col-md-12" style="padding-left: 0;margin-bottom: 20px;">
                                                    <a href="" class="btn btn-success">
                                                        Save
                                                    </a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="bhoechie-tab-content">
                                        <header class="section-header" style="padding: 0 0 20px 0;">
                                            <h3 class="section-title">Invite Friends</h3>
                                            <p> Would you like to import your contacts from other social networks. We can import contacts from your old profile and try to complete your profile.</p>
                                            <div class="border-divider"></div>
                                        </header>
                                        <center>
                                            <div class="row-fluid" style="margin-bottom: 20px">
                                                <div class="col-md-6">
                                                    <a href="" class="col-md-12 btn" style="padding: 15px;font-size: 35px;background-color: #3b5998;color:#fff;">
                                                        <i class="fa fa-facebook"></i>
                                                        Facebook
                                                    </a>
                                                </div>
                                                <div class="col-md-6">
                                                    <a href="" class="col-md-12 btn" style="padding: 15px;font-size: 35px;background-color: #1dcaff;color:#fff;">
                                                        <i class="fa fa-twitter"></i>
                                                        Twitter
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="row-fluid">
                                                <div class="col-md-6">
                                                    <a href="" class="col-md-12 btn btn-success" style="padding: 15px;font-size: 35px;margin-top: 5px;background-color: #dd4b39;color:#fff">
                                                        <i class="fa fa-google-plus"></i>
                                                        Google +
                                                    </a>
                                                </div>
                                                <div class="col-md-6">
                                                    <a href="" class="col-md-12 btn btn-success" style="padding: 15px;font-size: 35px;margin-top: 5px;background-color: #007bb6;color:#fff">
                                                        <i class="fa fa-linkedin"></i>
                                                        LinkedIn
                                                    </a>
                                                </div>
                                            </div>
                                        </center>
                                        <div class="row-fluid">
                                            <div class="col-md-12" style="margin-top: 22px;">
                                                <div class="form-group">
                                                    <label class="control-label" for="exampleInputAmount">Invite By Typing Email Address</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="fa fa-at"></i></div>
                                                        <input type="text" class="form-control" id="exampleInputAmount" placeholder="Email@Email.Com">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12" style="margin-bottom: 20px;">
                                                <a href="" class="btn btn-success">
                                                    Invite
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="bhoechie-tab-content">
                                        <header class="section-header" style="padding: 0 0 20px 0;">
                                            <h3 class="section-title">Privacy</h3>
                                            <div class="border-divider"></div>
                                        </header>
                                        <div class="row-fluid">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label" for="exampleInputAmount">Who can post to your wall?</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="fa fa-cog"></i></div>
                                                        <select class="form-control">
                                                            <option>My Connections only</option>
                                                            <option>No one</option>
                                                            <option>All</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label" for="exampleInputAmount">Who can view my contact details?</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="fa fa-cog"></i></div>
                                                        <select class="form-control">
                                                            <option>My Connections only</option>
                                                            <option>No one</option>
                                                            <option>All</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label" for="exampleInputAmount">Connect with Social Networks</label>
                                                    <div class="input-group">
                                                        <div class="col-md-1" style="font-size: 35px;margin: 5px;"><i class="fa fa-facebook-square"></i></div>
                                                        <div class="col-md-1" style="font-size: 35px;margin: 5px;"><i class="fa fa-twitter-square"></i></div>
                                                        <div class="col-md-1" style="font-size: 35px;margin: 5px;"><i class="fa fa-google-plus-square"></i></div>
                                                        <div class="col-md-1" style="font-size: 35px;margin: 5px;"><i class="fa fa-linkedin-square"></i></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label" for="exampleInputAmount">Auto Publish From (Social networks)</label>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" value="">
                                                            <i class="fa fa-facebook-square"></i> Facebook
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" value="">
                                                            <i class="fa fa-twitter-square"></i> Twitter
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" value="">
                                                            <i class="fa fa-google-plus-square"></i> Google +
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" value="">
                                                            <i class="fa fa-linkedin-square"></i> Linkedin
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12" style="margin-bottom: 20px;">
                                                <a href="" class="btn btn-success">
                                                    Save
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End .section_general -->
                </div>
                <!-- End .row -->
            </div>
            <!-- End .container-fluid -->
        </div>
        <!-- End .page_content -->
    </section>
</div>
<script src="js/jquery.autocomplete.min.js"></script>
<script src="js/currency-autocomplete.js"></script>
<script>
    $('#add_more_education').click(function (e) {
        e.preventDefault();
        $('#append_fields').append('<div class="form-horizontal"><div class="col-md-12"><hr/><div class="pull-right"><button type="button" class="btn btn-danger" id="remove_more_education"><i class="fa fa-trash"></i> Delete</button></div></div><div class="col-md-12"><div class="form-group"><label class="control-label" for="exampleInputAmount">School</label><div class="input-group"><div class="input-group-addon"><i class="fa fa-graduation-cap"></i></div><input type="text" class="form-control" id="exampleInputAmount" placeholder="School / College Name"></div></div></div><div class="col-md-6"><div class="form-group"><label class="control-label" for="exampleInputAmount">Dates Attended</label><div class="input-group"><div class="input-group-addon"><i class="fa fa-calendar"></i></div><input type="date" class="form-control" id="exampleInputAmount" placeholder="YYYY"></div></div></div><div class="col-md-6"><div class="form-group" style="margin-left: 5px"><label class="control-label" for="exampleInputAmount">&nbsp;</label><div class="input-group"><input type="date" class="form-control" id="exampleInputAmount" placeholder="YYYY"><div class="input-group-addon"><i class="fa fa-calendar"></i></div></div></div></div><div class="col-md-12"><div class="form-group"><label class="control-label" for="exampleInputAmount">Degree</label><div class="input-group"><div class="input-group-addon"><i class="fa fa-user"></i></div><select class="form-control">Other<option value="none" selected="">-</option><option value="High School">High School</option><option value="Associate’s Degree">Associate’s Degree</option><option value="Bachelor’s Degree">Bachelor’s Degree</option><option value="Master’s Degree">Master’s Degree</option><option value="Master of Business Administration (M.B.A.)">Master of Business Administration (M.B.A.)</option><option value="Juris Doctor (J.D.)">Juris Doctor (J.D.)</option><option value="Doctor of Medicine (M.D.)">Doctor of Medicine (M.D.)</option><option value="Doctor of Philosophy (Ph.D.)">Doctor of Philosophy (Ph.D.)</option><option value="Engineer’s Degree">Engineer’s Degree</option><option value="other">Other</option></select></div></div></div><div class="col-md-12"><div class="form-group"><label class="control-label" for="exampleInputAmount">Field Of Study</label><div class="input-group"><div class="input-group-addon"><i class="fa fa-book"></i></div><input type="text" class="form-control" id="exampleInputAmount" placeholder="B.Tech"></div></div></div><div class="col-md-12"><div class="form-group"><label class="control-label" for="exampleInputAmount">Grade</label><div class="input-group"><div class="input-group-addon"><i class="fa fa-trophy"></i></div><input type="text" class="form-control" id="exampleInputAmount" placeholder="Grade"></div></div></div><div class="col-md-12"><div class="form-group"><label class="control-label" for="exampleInputAmount">Activities & Societies</label><div class="input-group"><div class="input-group-addon"><i class="fa fa-eye"></i></div><textarea placeholder="Examples: Alpha Phi Omega, Chamber Chorale, Debate Team" class="form-control" rows="3"></textarea></div></div></div><div class="col-md-12"><div class="form-group"><label class="control-label" for="exampleInputAmount">Description</label><div class="input-group"><div class="input-group-addon"><i class="fa fa-info"></i></div><textarea class="form-control" rows="3"></textarea></div></div></div></div>');
    });
    $('#add_more_interests').click(function (e) {
        e.preventDefault();
        $('#append_intrest_fields').append('<div class="form-horizontal"><div class="col-md-11"><div class="form-group"><div class="input-group"><div class="input-group-addon"><i class="fa fa-eye"></i></div><div id="searchfield"><input type="text" name="currency" class="biginput form-control autocomplete" id="" placeholder="Type User Keyword Here.."></div></div></div></div><div class="col-md-1"><div class="input-group"><button type="button" class="btn btn-danger" id="remove_more_interests"><i class="fa fa-trash"></i></button></div></div></div>');
        $(function () {
            var currencies = [
                {value: 'Afghan afghani', data: 'AFN'},
                {value: 'Albanian lek', data: 'ALL'},
                {value: 'Algerian dinar', data: 'DZD'},
                {value: 'European euro', data: 'EUR'},
                {value: 'Angolan kwanza', data: 'AOA'},
                {value: 'East Caribbean dollar', data: 'XCD'},
                {value: 'Argentine peso', data: 'ARS'},
                {value: 'Armenian dram', data: 'AMD'},
                {value: 'Aruban florin', data: 'AWG'},
                {value: 'Australian dollar', data: 'AUD'},
                {value: 'Azerbaijani manat', data: 'AZN'},
                {value: 'Bahamian dollar', data: 'BSD'},
                {value: 'Bahraini dinar', data: 'BHD'},
                {value: 'Bangladeshi taka', data: 'BDT'},
                {value: 'Barbadian dollar', data: 'BBD'},
                {value: 'Belarusian ruble', data: 'BYR'},
                {value: 'Belize dollar', data: 'BZD'},
                {value: 'West African CFA franc', data: 'XOF'},
                {value: 'Bhutanese ngultrum', data: 'BTN'},
                {value: 'Bolivian boliviano', data: 'BOB'},
                {value: 'Bosnia-Herzegovina konvertibilna marka', data: 'BAM'},
                {value: 'Botswana pula', data: 'BWP'},
                {value: 'Brazilian real', data: 'BRL'},
                {value: 'Brunei dollar', data: 'BND'},
                {value: 'Movies', data: 'M'},
                {value: 'TV Shows ', data: 'M'},
                {value: 'Music', data: 'M'},
                {value: 'Sports', data: 'M'},
                {value: 'Books', data: 'M'}
            ];
            // setup autocomplete function pulling from currencies[] array
            $('.autocomplete').autocomplete({
                lookup: currencies,
                onSelect: function (suggestion) {
                    var thehtml = '<strong>Currency Name:</strong> ' + suggestion.value + ' <br> <strong>Symbol:</strong> ' + suggestion.data;
                    $('#outputcontent').html(thehtml);
                }
            });
        });
    });
    $(document).on('click', '#remove_more_education', function (e) {
        e.preventDefault();
        $($(this).parents('.form-horizontal')[0]).hide(400, function () {
            $(this).remove();
        });
    });
    $(document).on('click', '#remove_more_interests', function (e) {
        e.preventDefault();
        $($(this).parents('.form-horizontal')[0]).hide(400, function () {
            $(this).remove();
        });
    });
    $('#add_more_work').click(function (e) {
        e.preventDefault();
        $('#append_work_fields').append('<div class="form-horizontal"><div class="col-md-12"><hr/><div class="pull-right"><button type="button" class="btn btn-danger" id="remove_more_work"><i class="fa fa-trash"></i> Delete</button></div></div><div class="col-md-12"> <div class="form-group"> <label class="control-label" for="exampleInputAmount">Company Name</label> <div class="input-group"> <div class="input-group-addon"><i class="fa fa-building"></i></div><input type="text" class="form-control" id="exampleInputAmount" placeholder="Company Name"> </div></div></div><div class="col-md-12"> <div class="form-group"> <label class="control-label" for="exampleInputAmount">Title</label> <div class="input-group"> <div class="input-group-addon"><i class="fa fa-list-alt"></i></div><input type="text" class="form-control" id="exampleInputAmount" placeholder="Title"> </div></div></div><div class="col-md-12"> <div class="form-group"> <label class="control-label" for="exampleInputAmount">Location</label> <div class="input-group"> <div class="input-group-addon"><i class="fa fa-map-marker"></i></div><input type="text" class="form-control" id="exampleInputAmount" placeholder="Australia"> </div></div></div><div class="col-md-6"> <div class="form-group"> <label class="control-label" for="exampleInputAmount">Time Period</label> <div class="input-group"> <div class="input-group-addon"><i class="fa fa-calendar"></i></div><input type="date" class="form-control" id="exampleInputAmount" placeholder="YYYY"> </div></div></div><div class="col-md-6"> <div class="form-group" style="margin-left: 5px"> <label class="control-label" for="exampleInputAmount">&nbsp;</label> <div class="input-group"> <input type="date" class="form-control" id="exampleInputAmount" placeholder="YYYY"> <div class="input-group-addon"><i class="fa fa-calendar"></i></div></div></div></div><div class="col-md-6">&nbsp;</div><div class="col-md-6"> <div class="checkbox" style="margin-left: 5px"> <label> <input type="checkbox" value=""> I Currently Work Here. </label> </div></div><div class="col-md-12"> <div class="form-group"> <label class="control-label" for="exampleInputAmount">Description</label> <div class="input-group"> <div class="input-group-addon"><i class="fa fa-info"></i></div><textarea class="form-control" rows="3"></textarea> </div></div></div></div>');
    });
    $(document).on('click', '#remove_more_work', function (e) {
        e.preventDefault();
        $($(this).parents('.form-horizontal')[0]).hide(400, function () {
            $(this).remove();
        });
    });
    $("#show_upload_icon").hover(function () {
        $('.uploadFile').show();
    }, function () {
        $('.uploadFile').hide();
    });
</script>
<?php
include_once __DIR__ . '/footer.php';
