<?php
$title = "Company Pages";
$pic_type = 'square';
$menus = array(
    'index.php' => array(
        'icon' => 'home',
        'label' => 'Home',
    ),
    'index-2.php' => array(
        'icon' => 'home',
        'label' => 'Home 2',
    ),
    'company.php' => array(
        'icon' => 'home',
        'label' => 'Company Home',
    ),
    'company_upgrade.php' => array(
        'icon' => 'wrench',
        'label' => 'Upgrade Account',
    ),
    'company_services.php' => array(
        'icon' => 'cog',
        'label' => 'Services',
    ),
    'company_blog.php' => array(
        'icon' => 'comments',
        'label' => 'Blog',
    ),
    'company_followers.php' => array(
        'icon' => 'users',
        'label' => 'Followers',
    ),
    'company_contact.php' => array(
        'icon' => 'paper-plane',
        'label' => 'Contact',
    ),
);
$show_quick_icons = FALSE;
?>
<?php include_once __DIR__ . '/header.php' ?>
<style>
    #main-content {
        padding: 0;
        margin: 10px;
    }
    .pages_box {
        cursor: pointer;
        padding: 10px 20px;
        font-size: 32px;
        font-weight: bold;
        //text-align: center;
        border: 5px solid #999;
        background-color: #fff;
    }
    div.pages_box i.fa {
        font-size: 100px;
    }
</style>
<div class="col-md-1 bhoechie-tab-menu" style="width:6%;background-image: url('images/sub_nav_menu_bg.png');">
    <div class="list-group">
        <a href="#" class="list-group-item active text-center" data-post_type="a-z">
            <h4 class="fa fa-2x fa-clock-o"></h4><br/>A-Z
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="a-z">
            <h4 class="fa fa-2x fa-user"></h4><br/>Only Mine
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="all posts">
            <h4 class="fa fa-2x fa-square"></h4><br/>Show All
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="media">
            <h4 class="fa fa-2x fa-music"></h4><br/>Media
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="collection">
            <h4 class="fa fa-2x fa-columns"></h4><br/><p style="margin-left: -10px;">Collections</p>
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="events">
            <h4 class="fa fa-2x fa-calendar"></h4><br/>Events 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="blogs">
            <h4 class="fa fa-2x fa-edit"></h4><br/>Blogs
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="Groups">
            <h4 class="fa fa-2x fa-users"></h4><br/>Groups 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="Pages">
            <h4 class="fa fa-2x fa-paste"></h4><br/>Pages 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="favorites">
            <h4 class="fa fa-2x fa-star"></h4><br/>Favorites 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="member">
            <h4 class="fa fa-2x fa-users"></h4><br/>Members
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="settings">
            <h4 class="fa fa-2x fa-cog"></h4><br/>Settings 
        </a>
    </div>
</div>
<div id="all_posts_div" class="col-md-7" style="<?php echo $f_name == 'index.php' ? '' : 'background-color: #fff;'; ?>padding: 5px 5px;width:50%">
    <section id="service" class="layers">
        <h2 style="display:none">service</h2>

        <!-- .page_content -->
        <div class="page_content">

            <!-- .container-fluid -->
            <div class="container-fluid no-marg">

                <!-- .row -->
                <div class="row row_responsive">

                    <!-- .section_general -->
                    <div class="col-lg-11 section_general">

                        <header class="section-header" style="padding: 30px 0 30px 0;">
                            <h3 class="section-title">Company Pages</h3>
                            <p>Create a Page to build a closer relationship with your audience and customers.</p>
                            <div class="border-divider"></div>
                        </header>
                    </div>
                    <!-- End .section_general -->
                    <div class="row-fluid">
                        <div class="col-md-12">
                            <div class="col-md-4" id="local_business" data-href="create_page.php?type=lb">
                                <div class="pages_box">
                                    <center>
                                        <i class="fa fa-home"></i>
                                        <br/>
                                        <span style="font-size: 16px">Local Business or Place</span>
                                    </center>
                                </div>
                            </div>
                            <div class="col-md-4" id="local_business" data-href="create_page.php?type=co">
                                <div class="pages_box">
                                    <center>
                                        <i class="fa fa-building"></i>
                                        <br/>
                                        <span style="font-size: 16px">Company or Organisation</span>
                                    </center>
                                </div>
                            </div>
                            <div class="col-md-4" id="local_business" data-href="create_page.php?type=bp">
                                <div class="pages_box">
                                    <center>
                                        <i class="fa fa-mobile"></i>
                                        <br/>
                                        <span style="font-size: 16px">Brand or Product</span>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="col-md-12" style="margin-top: 25px">
                            <div class="col-md-4" id="local_business" data-href="create_page.php?type=pf">
                                <div class="pages_box">
                                    <center>
                                        <i class="fa fa-user"></i>
                                        <br/>
                                        <span style="font-size: 16px">Artist, Band or Public Figure</span>
                                    </center>
                                </div>
                            </div>
                            <div class="col-md-4" id="local_business" data-href="create_page.php?type=ent">
                                <div class="pages_box">
                                    <center>
                                        <i class="fa fa-music"></i>
                                        <br/>
                                        <span style="font-size: 16px">Entertainment</span>
                                    </center>
                                </div>
                            </div>
                            <div class="col-md-4" id="local_business" data-href="create_page.php?type=com">
                                <div class="pages_box">
                                    <center>
                                        <i class="fa fa-heart"></i>
                                        <br/>
                                        <span style="font-size: 16px">Cause or Community</span>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End .row -->

            </div>
            <!-- End .container-fluid -->

        </div>
        <!-- End .page_content -->

    </section>
</div>
<script>
    $(document).ready(function () {
        if (!localStorage.pages) {
            localStorage.pages = JSON.stringify([]);
        }
    });
    $(document).on('click', '#local_business', function () {
        if ($(this).attr('data-form_shown') !== '1') {
            var title = $(this).find('span').html();
            var html = '';
            html += '<form><div class="form-group"><label style="font-size:16px; text-align: left">' + title + '</label><input type="text" id="p_name" class="form-control" placeholder="Page Name"/></div><div class="form-group"><label style="font-size:14px;text-align: left">By clicking Get Started, you agree to the Pages Terms.</label></div><button style="margin-top: -30px;" type="submit" class="btn btn-success" id="save_page">Get Started</button></form>';
            $(this).attr('data-form_shown', 1).find('.pages_box').html(html).hide().fadeIn();
        }
    });
    $(document).on('click', '#save_page', function (e) {
        e.preventDefault();
        toastr.remove();
        var p_name = $('#p_name').val();
        var time_id = Date.now();
        if (p_name) {
            var page = {};
            page.id = time_id;
            page.name = $('#p_name').val();
            page.description = '';
            page.photo = 'noimage.jpg';
            if (localStorage !== undefined) {
                if (!localStorage.pages) {
                    localStorage.pages = JSON.stringify([]);
                } else {
                    var pages = JSON.parse(localStorage.pages);
                    pages.push(page);
                    localStorage.pages = JSON.stringify(pages);
                    window.location.href = "create_page.php?p_id=" + time_id;
                }
            }
        } else {
            toastr.error('Please Enter Page Name!', 'Error');
        }
    });
</script>
<?php include_once __DIR__ . '/footer.php'; ?>