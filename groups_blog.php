<?php

$title = "Group Blog";
$pic_type = 'square';
$menus = array(
    'index.php' => array(
        'icon' => 'home',
        'label' => 'Home',
    ),
    'index-2.php' => array(
        'icon' => 'home',
        'label' => 'Home 2',
    ),
    'groups.php' => array(
        'icon' => 'home',
        'label' => 'Group Home',
    ),
    'groups_about.php' => array(
        'icon' => 'user',
        'label' => 'About Us',
    ),
    'groups_blog.php' => array(
        'icon' => 'comments',
        'label' => 'Blog',
    ),
    'groups_events.php' => array(
        'icon' => 'calendar',
        'label' => 'Events',
    ),
    'groups_members.php' => array(
        'icon' => 'users',
        'label' => 'Members',
    ),
    'groups_followers.php' => array(
        'icon' => 'users',
        'label' => 'Followers',
    ),
    'groups_contact.php' => array(
        'icon' => 'paper-plane',
        'label' => 'Contact',
    ),
);
$show_quick_icons = FALSE;
?>
<?php include_once __DIR__ . '/header.php' ?>
<style>
    #main-content {
        padding: 0;
        margin: 10px;
    }
</style>
<!-- content -->
<div id="content-wrapper">
    <div id="main-content">
        <div class="page-container blog-post">
            <article class="post-standard">
                <div class="post-header">
                    <div class="post-categoris">
                        <a href="#" style="background:#ca85ca">Adventures</a><a href="#" style="background:#4abe63">Sport</a><a href="#" style="background:#55ACEE">Music</a>	
                    </div>

                    <h2 class="post-title"><a href="blog_single.php">Give me all your love and I will fly across the sky</a></h2>

                    <div class="post-meta">
                        <div><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></div>
                        <div><i class="icon-clock"></i>January 18, 2015</div>
                        <div><i class="icon-bubble"></i><a href="#" title="">5 Comments</a></div>
                        <div><i class="icon-folder"></i>in : <a href="#" rel="category tag">WordPress</a></div>
                        <div><a class="post-lik " title="Love"><i class="icon-heart"></i><span>35</span></a></div>
                        <div><i class="icon-eye"></i><span>216</span></div>
                    </div>
                </div>


                <div class="post-thumb">

                    <div class="gallery-grid-columns">
                        <ul>

                            <li class="gallery-item">
                                <a href="http://placehold.it/1024x681" class="image-links-gallery" data-gal="portfolio[gallery-1]">
                                    <img src="http://placehold.it/267x267/000000" alt="">
                                </a>
                            </li>

                            <li class="gallery-item">
                                <a href="http://placehold.it/1024x681" class="image-links-gallery" data-gal="portfolio[gallery-1]">
                                    <img src="http://placehold.it/267x267" alt="">
                                </a>
                            </li>

                            <li class="gallery-item">
                                <a href="http://placehold.it/1024x681" class="image-links-gallery" data-gal="portfolio[gallery-1]">
                                    <img src="http://placehold.it/267x267/000000" alt="">
                                </a>
                            </li>

                            <li class="gallery-item">
                                <a href="http://placehold.it/1024x681" class="image-links-gallery" data-gal="portfolio[gallery-1]">
                                    <img src="http://placehold.it/267x267" alt="">
                                </a>
                            </li>

                            <li class="gallery-item">
                                <a href="http://placehold.it/1024x681" class="image-links-gallery" data-gal="portfolio[gallery-1]">
                                    <img src="http://placehold.it/267x267" alt="">
                                </a>
                            </li>

                            <li class="gallery-item">
                                <a href="http://placehold.it/1024x681" class="image-links-gallery" data-gal="portfolio[gallery-1]">
                                    <img src="http://placehold.it/267x267/000000" alt="">
                                </a>
                            </li>

                            <li class="gallery-item">
                                <a href="http://placehold.it/1024x681" class="image-links-gallery" data-gal="portfolio[gallery-1]">
                                    <img src="http://placehold.it/267x267" alt="">
                                </a>
                            </li>

                            <li class="gallery-item">
                                <a href="http://placehold.it/1024x681" class="image-links-gallery" data-gal="portfolio[gallery-1]">
                                    <img src="http://placehold.it/267x267/000000" alt="">
                                </a>
                            </li>

                        </ul>
                    </div>		



                </div>

                <div class="post-content">
                    <p>Maecenas varius massa non libero sodales malesuada. Morbi in malesuada libero. Quisque ut blandit elit, in suscipit felis. Mauris nec purus sed nisi semper blandit sed a lectus. Sed ultrices, augue at iaculis rutrum, orci diam tincidunt velit, vel condimentum eros justo sit amet sem. Fusce eget justo eu.</p> 

                    <p>Dicat ludus erroribus has ea, corpora oportere vel ne. Accusam intellegebat delicatissimi eos et, sed eirmod atomorum hendrerit id, graeco putant labitur nec et. Populo epicurei scriptorem ut per. Sensibus consectetuer ius no, has te porro verterem prodesset. Mel natum placerat in. Pro iriure expetenda disputando ut, solum verterem sapientem te eum. […]</p>

                </div>

                <div class="post-footer">

                    <div class="meta-item">
                        <a href="blog_single.php" class="read_more_button">Continue Reading<i class="icon-arrow-right"></i></a>
                    </div>


                    <div class="meta-item meta-item-share">
                        <div class="meta-item-wrapper">

                            <div class="fc_sharing">
                                <div class="share_button">
                                    <i class="icon-share"></i>
                                </div>
                                <ul class="share_items">
                                    <li><a class="fa fa-facebook" href="#"></a></li>
                                    <li><a class="fa fa-twitter" href="#"></a></li>
                                    <li><a class="fa fa-google-plus" href="#"></a></li>
                                    <li><a class="fa fa-pinterest" href="#"></a></li>
                                    <li><a class="fa fa-linkedin" href="#"></a></li>
                                </ul>
                            </div>	

                        </div>
                    </div>

                </div>

            </article> 

            <article class="post-standard">
                <div class="post-header">
                    <div class="post-categoris">
                        <a href="#" style="background:#01c181">travel</a><a href="#" style="background:#336">Web</a><a href="#" style="background:#82B53F">Envato</a>
                    </div>
                    <h2 class="post-title"><a href="blog_single.php">My Profile in Themeforest</a></h2>

                    <div class="post-meta">
                        <div><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></div>
                        <div><i class="icon-clock"></i>January 18, 2015</div>
                        <div><i class="icon-bubble"></i><a href="#" title="">5 Comments</a></div>
                        <div><i class="icon-folder"></i>in : <a href="#" rel="category tag">WordPress</a></div>
                        <div><a class="post-lik " title="Love"><i class="icon-heart"></i><span>35</span></a></div>
                        <div><i class="icon-eye"></i><span>216</span></div>
                    </div>
                </div>

                <div class="post-thumb">

                    <span class="featured_icon_post"><i class="icon-link"></i></span>

                    <a href="http://themeforest.net/user/flexycodes" class="link_format_post">
                        <div class="post-format-link">
                            <div class="media-overlay e-bg"></div>
                            <div class="post-content-format">
                                <div class="post-format-link-content">http://themeforest.net/user/flexycodes</div>
                            </div>
                        </div><!--.post-content-->
                    </a>

                </div>

                <div class="post-content">
                    <p>Maecenas varius massa non libero sodales malesuada. Morbi in malesuada libero. Quisque ut blandit elit, in suscipit felis. Mauris nec purus sed nisi semper blandit sed a lectus. Sed ultrices, augue at iaculis rutrum, orci diam tincidunt velit, vel condimentum eros justo sit amet sem. Fusce eget justo eu.</p> 

                    <p>Dicat ludus erroribus has ea, corpora oportere vel ne. Accusam intellegebat delicatissimi eos et, sed eirmod atomorum hendrerit id, graeco putant labitur nec et. Populo epicurei scriptorem ut per. Sensibus consectetuer ius no, has te porro verterem prodesset. Mel natum placerat in. Pro iriure expetenda disputando ut, solum verterem sapientem te eum. […]</p>				
                </div>

                <div class="post-footer">

                    <div class="meta-item">
                        <a href="blog_single.php" class="read_more_button">Continue Reading<i class="icon-arrow-right"></i></a>
                    </div>


                    <div class="meta-item meta-item-share">
                        <div class="meta-item-wrapper">

                            <div class="fc_sharing">
                                <div class="share_button">
                                    <i class="icon-share"></i>
                                </div>
                                <ul class="share_items">
                                    <li><a class="fa fa-facebook" href="#"></a></li>
                                    <li><a class="fa fa-twitter" href="#"></a></li>
                                    <li><a class="fa fa-google-plus" href="#"></a></li>
                                    <li><a class="fa fa-pinterest" href="#"></a></li>
                                    <li><a class="fa fa-linkedin" href="#"></a></li>
                                </ul>
                            </div>	

                        </div>
                    </div>

                </div>

            </article> 

            <article class="post-standard">

                <div class="post-header">

                    <div class="post-categoris">
                        <a href="#" style="background:#7DC561">Food</a><a href="#" style="background:#E91B23">health</a><a href="#" style="background:#f4b23f">Gallery</a>	
                    </div>

                    <h2 class="post-title"><a href="blog_single.php">Stunning Health Benefits of Eating Chocolates</a></h2>

                    <div class="post-meta">
                        <div><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></div>
                        <div><i class="icon-clock"></i>January 18, 2015</div>
                        <div><i class="icon-bubble"></i><a href="#" title="">5 Comments</a></div>
                        <div><i class="icon-folder"></i>in : <a href="#" rel="category tag">WordPress</a></div>
                        <div><a class="post-lik " title="Love"><i class="icon-heart"></i><span>35</span></a></div>
                        <div><i class="icon-eye"></i><span>216</span></div>
                    </div>

                </div>

                <div class="post-thumb">

                    <span class="featured_icon_post"><i class="icon-camera"></i></span>

                    <div class="flexslider flexslider-blog">

                        <!--/ .slides -->

                        <div class="flex-viewport" style="overflow: hidden; position: relative; height: 437px;"><ul class="slides" style="width: 2000%; transition-duration: 1.4s; transform: translate3d(-6040px, 0px, 0px);"><li class="clone" aria-hidden="true" style="width: 755px; float: left; display: block;">
                                    <a href="blog_single.php" class="image-links">
                                        <img src="http://placehold.it/796x461" alt="Gallery Post Example" title="" draggable="false">
                                    </a>
                                </li>


                                <li class="" style="width: 755px; float: left; display: block;">
                                    <a href="blog_single.php" class="image-links">
                                        <img src="http://placehold.it/796x461" alt="Gallery Post Example" title="" draggable="false">
                                    </a>
                                </li>

                                <li class="" style="width: 755px; float: left; display: block;">
                                    <a href="blog_single.php" class="image-links">
                                        <img src="http://placehold.it/796x461" alt="Gallery Post Example" title="" draggable="false">
                                    </a>
                                </li>

                                <li class="" style="width: 755px; float: left; display: block;">
                                    <a href="blog_single.php" class="image-links">
                                        <img src="http://placehold.it/796x461" alt="Gallery Post Example" title="" draggable="false">
                                    </a>
                                </li>

                                <li class="" style="width: 755px; float: left; display: block;">
                                    <a href="blog_single.php" class="image-links">
                                        <img src="http://placehold.it/796x461" alt="Gallery Post Example" title="" draggable="false">
                                    </a>
                                </li>

                                <li class="" style="width: 755px; float: left; display: block;">
                                    <a href="blog_single.php" class="image-links">
                                        <img src="http://placehold.it/796x461" alt="Gallery Post Example" title="" draggable="false">
                                    </a>
                                </li>

                                <li class="" style="width: 755px; float: left; display: block;">
                                    <a href="blog_single.php" class="image-links">
                                        <img src="http://placehold.it/796x461" alt="Gallery Post Example" title="" draggable="false">
                                    </a>
                                </li>

                                <li class="" style="width: 755px; float: left; display: block;">
                                    <a href="blog_single.php" class="image-links">
                                        <img src="http://placehold.it/796x461" alt="Gallery Post Example" title="" draggable="false">
                                    </a>
                                </li>


                                <li class="flex-active-slide" style="width: 755px; float: left; display: block;">
                                    <a href="blog_single.php" class="image-links">
                                        <img src="http://placehold.it/796x461" alt="Gallery Post Example" title="" draggable="false">
                                    </a>
                                </li>


                                <li class="clone" aria-hidden="true" style="width: 755px; float: left; display: block;">
                                    <a href="blog_single.php" class="image-links">
                                        <img src="http://placehold.it/796x461" alt="Gallery Post Example" title="" draggable="false">
                                    </a>
                                </li></ul></div><ol class="flex-control-nav flex-control-paging"><li><a class="">1</a></li><li><a class="">2</a></li><li><a class="">3</a></li><li><a class="">4</a></li><li><a class="">5</a></li><li><a class="">6</a></li><li><a class="">7</a></li><li><a class="flex-active">8</a></li></ol><ul class="flex-direction-nav"><li><a class="flex-prev" href="#"><i class="fa fa-angle-left"></i></a></li><li><a class="flex-next" href="#"><i class="fa fa-angle-right"></i></a></li></ul></div> <!--/ .flexslider -->

                </div>

                <div class="post-content">
                    <p>Maecenas varius massa non libero sodales malesuada. Morbi in malesuada libero. Quisque ut blandit elit, in suscipit felis. Mauris nec purus sed nisi semper blandit sed a lectus. Sed ultrices, augue at iaculis rutrum, orci diam tincidunt velit, vel condimentum eros justo sit amet sem. Fusce eget justo eu.</p> 

                    <p>Dicat ludus erroribus has ea, corpora oportere vel ne. Accusam intellegebat delicatissimi eos et, sed eirmod atomorum hendrerit id, graeco putant labitur nec et. Populo epicurei scriptorem ut per. Sensibus consectetuer ius no, has te porro verterem prodesset. Mel natum placerat in. Pro iriure expetenda disputando ut, solum verterem sapientem te eum. […]</p>

                </div>

                <!--<a class="more-link" href="#">Continue Reading</a>-->


                <div class="post-footer">

                    <div class="meta-item">
                        <a href="blog_single.php" class="read_more_button">Continue Reading<i class="icon-arrow-right"></i></a>
                    </div>


                    <div class="meta-item meta-item-share">
                        <div class="meta-item-wrapper">

                            <div class="fc_sharing">
                                <div class="share_button">
                                    <i class="icon-share"></i>
                                </div>
                                <ul class="share_items">
                                    <li><a class="fa fa-facebook" href="#"></a></li>
                                    <li><a class="fa fa-twitter" href="#"></a></li>
                                    <li><a class="fa fa-google-plus" href="#"></a></li>
                                    <li><a class="fa fa-pinterest" href="#"></a></li>
                                    <li><a class="fa fa-linkedin" href="#"></a></li>
                                </ul>
                            </div>	

                        </div>
                    </div>

                </div>

            </article> 

            <article class="post-standard">
                <div class="post-header">

                    <div class="post-categoris">
                        <a href="#" style="background:#32742c">Movies</a><a href="#" style="background:#d70060">Quote</a><a href="#" style="background:#ca85ca">Environment</a>
                    </div>

                    <h2 class="post-title"><a href="blog_single.php">Awesome Quote Post</a></h2>
                    <div class="post-meta">
                        <div><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></div>
                        <div><i class="icon-clock"></i>January 18, 2015</div>
                        <div><i class="icon-bubble"></i><a href="#" title="">5 Comments</a></div>
                        <div><i class="icon-folder"></i>in : <a href="#" rel="category tag">WordPress</a></div>
                        <div><a class="post-lik " title="Love"><i class="icon-heart"></i><span>35</span></a></div>
                        <div><i class="icon-eye"></i><span>216</span></div>
                    </div>
                </div>


                <div class="post-content post-quote">
                    <div class="post-inner">
                        <p>“ You have brains in your head. You have feet in your shoes. You can steer yourself in any direction you choose. You’re on your own, and you know what you know. And you are the guy who’ll decide where to go. ”</p>
                        <span class="author">- Dr. Seuss -</span>
                    </div><!-- End post-inner -->
                </div>


                <div class="post-footer">

                    <div class="meta-item">
                        <a href="blog_single.php" class="read_more_button">Continue Reading<i class="icon-arrow-right"></i></a>
                    </div>


                    <div class="meta-item meta-item-share">
                        <div class="meta-item-wrapper">

                            <div class="fc_sharing">
                                <div class="share_button">
                                    <i class="icon-share"></i>
                                </div>
                                <ul class="share_items">
                                    <li><a class="fa fa-facebook" href="#"></a></li>
                                    <li><a class="fa fa-twitter" href="#"></a></li>
                                    <li><a class="fa fa-google-plus" href="#"></a></li>
                                    <li><a class="fa fa-pinterest" href="#"></a></li>
                                    <li><a class="fa fa-linkedin" href="#"></a></li>
                                </ul>
                            </div>	

                        </div>
                    </div>

                </div>

            </article> 

            <article class="post-standard">
                <div class="post-header">
                    <div class="post-categoris">
                        <a href="#" style="background:#55ACEE">Music</a><a href="#" style="background:#607ec7">Technology</a><a href="#" style="background:#e54e7e">Fashion</a>	
                    </div>

                    <h2 class="post-title"><a href="blog_single.php">Post with grid gallery</a></h2>

                    <div class="post-meta">
                        <div><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></div>
                        <div><i class="icon-clock"></i>January 18, 2015</div>
                        <div><i class="icon-bubble"></i><a href="#" title="">5 Comments</a></div>
                        <div><i class="icon-folder"></i>in : <a href="#" rel="category tag">WordPress</a></div>
                        <div><a class="post-lik " title="Love"><i class="icon-heart"></i><span>35</span></a></div>
                        <div><i class="icon-eye"></i><span>216</span></div>
                    </div>
                </div>

                <div class="post-thumb">

                    <span class="featured_icon_post"><i class="icon-camera"></i></span>

                    <div class="gallery-grid grid-even">
                        <ul>
                            <li class="grid-item-1">
                                <a href="http://placehold.it/796x531" class="image-links-gallery" data-gall="portfolio[gallery-2]">
                                    <img src="http://placehold.it/796x531" alt="">
                                </a>
                            </li>

                            <li class="grid-item-2">
                                <a href="http://placehold.it/796x531" class="image-links-gallery" data-gall="portfolio[gallery-2]">
                                    <img src="http://placehold.it/372x241" alt="">
                                </a>
                            </li>

                            <li class="grid-item-3">
                                <a href="http://placehold.it/796x531" class="image-links-gallery" data-gall="portfolio[gallery-2]">
                                    <img src="http://placehold.it/372x241" alt="">
                                </a>
                            </li>

                            <li class="grid-item-4 last-item-class">
                                <a href="http://placehold.it/796x531" class="image-links-gallery" data-gall="portfolio[gallery-2]">
                                    <img src="http://placehold.it/245x159" alt="">
                                </a>
                            </li>

                            <li class="grid-item-5 last-item-class">
                                <a href="http://placehold.it/796x531" class="image-links-gallery" data-gall="portfolio[gallery-2]">
                                    <img src="http://placehold.it/245x159" alt="">
                                </a>
                            </li>

                            <li class="grid-item-6 last-item-class grid-last-item">
                                <a href="http://placehold.it/796x531" class="image-links-gallery" data-gall="portfolio[gallery-2]">
                                    <img src="http://placehold.it/245x159" alt="">
                                </a>
                            </li>

                        </ul>
                    </div>		

                </div>

                <div class="post-content">
                    <p>Maecenas varius massa non libero sodales malesuada. Morbi in malesuada libero. Quisque ut blandit elit, in suscipit felis. Mauris nec purus sed nisi semper blandit sed a lectus. Sed ultrices, augue at iaculis rutrum, orci diam tincidunt velit, vel condimentum eros justo sit amet sem. Fusce eget justo eu.</p> 

                    <p>Dicat ludus erroribus has ea, corpora oportere vel ne. Accusam intellegebat delicatissimi eos et, sed eirmod atomorum hendrerit id, graeco putant labitur nec et. Populo epicurei scriptorem ut per. Sensibus consectetuer ius no, has te porro verterem prodesset. Mel natum placerat in. Pro iriure expetenda disputando ut, solum verterem sapientem te eum. […]</p>

                </div>

                <div class="post-footer">

                    <div class="meta-item">
                        <a href="blog_single.php" class="read_more_button">Continue Reading<i class="icon-arrow-right"></i></a>
                    </div>


                    <div class="meta-item meta-item-share">
                        <div class="meta-item-wrapper">

                            <div class="fc_sharing">
                                <div class="share_button">
                                    <i class="icon-share"></i>
                                </div>
                                <ul class="share_items">
                                    <li><a class="fa fa-facebook" href="#"></a></li>
                                    <li><a class="fa fa-twitter" href="#"></a></li>
                                    <li><a class="fa fa-google-plus" href="#"></a></li>
                                    <li><a class="fa fa-pinterest" href="#"></a></li>
                                    <li><a class="fa fa-linkedin" href="#"></a></li>
                                </ul>
                            </div>	

                        </div>
                    </div>

                </div>

            </article> 

            <article class="post-standard">
                <div class="post-header">
                    <div class="post-categoris">
                        <a href="#" style="background:#32742c">Movies</a><a href="#" style="background:#d70060">Quote</a><a href="#" style="background:#ca85ca">Environment</a>
                    </div>
                    <h2 class="post-title"><a href="blog_single.php">Awesome Quote Post with image</a></h2>
                    <div class="post-meta">
                        <div><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></div>
                        <div><i class="icon-clock"></i>January 18, 2015</div>
                        <div><i class="icon-bubble"></i><a href="#" title="">5 Comments</a></div>
                        <div><i class="icon-folder"></i>in : <a href="#" rel="category tag">WordPress</a></div>
                        <div><a class="post-lik " title="Love"><i class="icon-heart"></i><span>35</span></a></div>
                        <div><i class="icon-eye"></i><span>216</span></div>
                    </div>

                </div>


                <div class="post-thumb">

                    <span class="featured_icon_post"><i class="fa fa-quote-left"></i></span>

                    <a href="#" class="image-links">
                        <img src="http://placehold.it/796x461" alt="">  
                    </a>			
                </div>

                <div class="post-content post-quote-2">
                    <div class="post-inner">
                        <p>“ Only one thing is impossible for God: To find any sense in any copyright law on the planet. ”</p>
                        <span class="author">- Mark Twain -</span>
                    </div><!-- End post-inner -->
                </div>

                <div class="post-footer">

                    <div class="meta-item">
                        <a href="blog_single.php" class="read_more_button">Continue Reading<i class="icon-arrow-right"></i></a>
                    </div>


                    <div class="meta-item meta-item-share">
                        <div class="meta-item-wrapper">

                            <div class="fc_sharing">
                                <div class="share_button">
                                    <i class="icon-share"></i>
                                </div>
                                <ul class="share_items">
                                    <li><a class="fa fa-facebook" href="#"></a></li>
                                    <li><a class="fa fa-twitter" href="#"></a></li>
                                    <li><a class="fa fa-google-plus" href="#"></a></li>
                                    <li><a class="fa fa-pinterest" href="#"></a></li>
                                    <li><a class="fa fa-linkedin" href="#"></a></li>
                                </ul>
                            </div>	

                        </div>
                    </div>

                </div>

            </article> 

            <article class="post-standard">
                <div class="post-header">
                    <div class="post-categoris">
                        <a href="#" style="background:#5EA9DD">twitter</a><a href="#" style="background:#336">Web</a><a href="#" style="background:#00aaad">BUSINESS</a>	
                    </div>
                    <h2 class="post-title"><a href="blog_single.php">AWESOME TWITTER POST</a></h2>

                    <div class="post-meta">
                        <div><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></div>
                        <div><i class="icon-clock"></i>January 18, 2015</div>
                        <div><i class="icon-bubble"></i><a href="#" title="">5 Comments</a></div>
                        <div><i class="icon-folder"></i>in : <a href="#" rel="category tag">WordPress</a></div>
                        <div><a class="post-lik " title="Love"><i class="icon-heart"></i><span>35</span></a></div>
                        <div><i class="icon-eye"></i><span>216</span></div>
                    </div>
                </div>

                <div class="post-thumb">

                    <span class="featured_icon_post"><i class="icon-social-twitter"></i></span>

                    <div class="post-wrap-twitter">
                        <div class="post-img" data-twttr-id="twttr-sandbox-0">
                            <blockquote class="twitter-tweet"><a href="https://twitter.com/YounesEzzanz/status/568908478082916352"></a></blockquote>
                            <script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                        </div>
                    </div>

                </div>

                <div class="post-content">
                    <p>Maecenas varius massa non libero sodales malesuada. Morbi in malesuada libero. Quisque ut blandit elit, in suscipit felis. Mauris nec purus sed nisi semper blandit sed a lectus. Sed ultrices, augue at iaculis rutrum, orci diam tincidunt velit, vel condimentum eros justo sit amet sem. Fusce eget justo eu.</p> 

                    <p>Dicat ludus erroribus has ea, corpora oportere vel ne. Accusam intellegebat delicatissimi eos et, sed eirmod atomorum hendrerit id, graeco putant labitur nec et. Populo epicurei scriptorem ut per. Sensibus consectetuer ius no, has te porro verterem prodesset. Mel natum placerat in. Pro iriure expetenda disputando ut, solum verterem sapientem te eum. […]</p>

                </div>

                <div class="post-footer">

                    <div class="meta-item">
                        <a href="blog_single.php" class="read_more_button">Continue Reading<i class="icon-arrow-right"></i></a>
                    </div>


                    <div class="meta-item meta-item-share">
                        <div class="meta-item-wrapper">

                            <div class="fc_sharing">
                                <div class="share_button">
                                    <i class="icon-share"></i>
                                </div>
                                <ul class="share_items">
                                    <li><a class="fa fa-facebook" href="#"></a></li>
                                    <li><a class="fa fa-twitter" href="#"></a></li>
                                    <li><a class="fa fa-google-plus" href="#"></a></li>
                                    <li><a class="fa fa-pinterest" href="#"></a></li>
                                    <li><a class="fa fa-linkedin" href="#"></a></li>
                                </ul>
                            </div>	

                        </div>
                    </div>

                </div>

            </article> 

            <article class="post-standard">
                <div class="post-header">
                    <div class="post-categoris">
                        <a href="#" style="background:#E22D24">youtube</a><a href="#" style="background:#f4b23f">Gallery</a><a href="#" style="background:#e54e7e">Fashion</a>		
                    </div>
                    <h2 class="post-title"><a href="blog_single.php">Ways To Make Sure You Are in A Right Path</a></h2>
                    <div class="post-meta">
                        <div><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></div>
                        <div><i class="icon-clock"></i>January 18, 2015</div>
                        <div><i class="icon-bubble"></i><a href="#" title="">5 Comments</a></div>
                        <div><i class="icon-folder"></i>in : <a href="#" rel="category tag">WordPress</a></div>
                        <div><a class="post-lik " title="Love"><i class="icon-heart"></i><span>35</span></a></div>
                        <div><i class="icon-eye"></i><span>216</span></div>
                    </div>
                </div>

                <div class="post-thumb videoWrapper">		
                    <iframe src="http://www.youtube.com/embed/JuyB7NO0EYY"></iframe>
                </div>

                <div class="post-content">
                    <p>Maecenas varius massa non libero sodales malesuada. Morbi in malesuada libero. Quisque ut blandit elit, in suscipit felis. Mauris nec purus sed nisi semper blandit sed a lectus. Sed ultrices, augue at iaculis rutrum, orci diam tincidunt velit, vel condimentum eros justo sit amet sem. Fusce eget justo eu.</p> 

                    <p>Dicat ludus erroribus has ea, corpora oportere vel ne. Accusam intellegebat delicatissimi eos et, sed eirmod atomorum hendrerit id, graeco putant labitur nec et. Populo epicurei scriptorem ut per. Sensibus consectetuer ius no, has te porro verterem prodesset. Mel natum placerat in. Pro iriure expetenda disputando ut, solum verterem sapientem te eum. […]</p>

                </div>

                <div class="post-footer">

                    <div class="meta-item">
                        <a href="blog_single.php" class="read_more_button">Continue Reading<i class="icon-arrow-right"></i></a>
                    </div>


                    <div class="meta-item meta-item-share">
                        <div class="meta-item-wrapper">

                            <div class="fc_sharing">
                                <div class="share_button">
                                    <i class="icon-share"></i>
                                </div>
                                <ul class="share_items">
                                    <li><a class="fa fa-facebook" href="#"></a></li>
                                    <li><a class="fa fa-twitter" href="#"></a></li>
                                    <li><a class="fa fa-google-plus" href="#"></a></li>
                                    <li><a class="fa fa-pinterest" href="#"></a></li>
                                    <li><a class="fa fa-linkedin" href="#"></a></li>
                                </ul>
                            </div>	

                        </div>
                    </div>

                </div>

            </article> 

            <article class="post-standard">
                <div class="post-header">
                    <div class="post-categoris">
                        <a href="#" style="background:#FF9015">Audio</a><a href="#" style="background:#e54e7e">Fashion</a>
                    </div>
                    <h2 class="post-title"><a href="blog_single.php">Awesome Audio post</a></h2>
                    <div class="post-meta">
                        <div><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></div>
                        <div><i class="icon-clock"></i>January 18, 2015</div>
                        <div><i class="icon-bubble"></i><a href="#" title="">5 Comments</a></div>
                        <div><i class="icon-folder"></i>in : <a href="#" rel="category tag">WordPress</a></div>
                        <div><a class="post-lik " title="Love"><i class="icon-heart"></i><span>35</span></a></div>
                        <div><i class="icon-eye"></i><span>216</span></div>
                    </div>
                </div>


                <div class="post-thumb audio-post">

                    <span class="featured_icon_post"><i class="icon-music-tone-alt"></i></span>

                    <div class="post-format-link" style="padding: 85px 50px;">
                        <div class="media-overlay e-bg"></div>
                    </div><!--.post-content-->

                    <div class="player-container">		
                        <audio controls="controls" style="width: 100%;">
                            <source type="audio/mpeg" src="http://demo.vanthemes.com/pratico/wp-content/uploads/audio.mp3">
                        </audio>
                    </div>

                </div>

                <div class="post-content">
                    <p>Maecenas varius massa non libero sodales malesuada. Morbi in malesuada libero. Quisque ut blandit elit, in suscipit felis. Mauris nec purus sed nisi semper blandit sed a lectus. Sed ultrices, augue at iaculis rutrum, orci diam tincidunt velit, vel condimentum eros justo sit amet sem. Fusce eget justo eu.</p> 

                    <p>Dicat ludus erroribus has ea, corpora oportere vel ne. Accusam intellegebat delicatissimi eos et, sed eirmod atomorum hendrerit id, graeco putant labitur nec et. Populo epicurei scriptorem ut per. Sensibus consectetuer ius no, has te porro verterem prodesset. Mel natum placerat in. Pro iriure expetenda disputando ut, solum verterem sapientem te eum. […]</p>

                </div>

                <div class="post-footer">

                    <div class="meta-item">
                        <a href="blog_single.php" class="read_more_button">Continue Reading<i class="icon-arrow-right"></i></a>
                    </div>


                    <div class="meta-item meta-item-share">
                        <div class="meta-item-wrapper">

                            <div class="fc_sharing">
                                <div class="share_button">
                                    <i class="icon-share"></i>
                                </div>
                                <ul class="share_items">
                                    <li><a class="fa fa-facebook" href="#"></a></li>
                                    <li><a class="fa fa-twitter" href="#"></a></li>
                                    <li><a class="fa fa-google-plus" href="#"></a></li>
                                    <li><a class="fa fa-pinterest" href="#"></a></li>
                                    <li><a class="fa fa-linkedin" href="#"></a></li>
                                </ul>
                            </div>	

                        </div>
                    </div>

                </div>

            </article> 

            <article class="post-standard">
                <div class="post-header">
                    <div class="post-categoris">
                        <a href="#" style="background:#FF3401">soundcloud</a><a href="#" style="background:#e54e7e">Fashion</a>
                    </div>
                    <h2 class="post-title"><a href="blog_single.php">Awesome soundcloud post</a></h2>
                    <div class="post-meta">
                        <div><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></div>
                        <div><i class="icon-clock"></i>January 18, 2015</div>
                        <div><i class="icon-bubble"></i><a href="#" title="">5 Comments</a></div>
                        <div><i class="icon-folder"></i>in : <a href="#" rel="category tag">WordPress</a></div>
                        <div><a class="post-lik " title="Love"><i class="icon-heart"></i><span>35</span></a></div>
                        <div><i class="icon-eye"></i><span>216</span></div>
                    </div>
                </div>


                <div class="post-thumb post-soundcloud">

                    <span class="featured_icon_post"><i class="icon-music-tone-alt"></i></span>

                    <img height="399" src="images/cover-4.jpg" alt="">			

                    <div class="media-overlay e-bg"></div>

                    <div class="player-container">
                        <iframe src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/82304720&amp;color=cea525&amp;auto_play=false&amp;show_artwork=true" width="468" height="166" allowfullscreen="" style="border:0 !important;width:100%!important;overflow:hidden;"></iframe>
                    </div>

                </div>

                <div class="post-content">
                    <p>Maecenas varius massa non libero sodales malesuada. Morbi in malesuada libero. Quisque ut blandit elit, in suscipit felis. Mauris nec purus sed nisi semper blandit sed a lectus. Sed ultrices, augue at iaculis rutrum, orci diam tincidunt velit, vel condimentum eros justo sit amet sem. Fusce eget justo eu.</p> 

                    <p>Dicat ludus erroribus has ea, corpora oportere vel ne. Accusam intellegebat delicatissimi eos et, sed eirmod atomorum hendrerit id, graeco putant labitur nec et. Populo epicurei scriptorem ut per. Sensibus consectetuer ius no, has te porro verterem prodesset. Mel natum placerat in. Pro iriure expetenda disputando ut, solum verterem sapientem te eum. […]</p>

                </div>

                <div class="post-footer">

                    <div class="meta-item">
                        <a href="blog_single.php" class="read_more_button">Continue Reading<i class="icon-arrow-right"></i></a>
                    </div>


                    <div class="meta-item meta-item-share">
                        <div class="meta-item-wrapper">

                            <div class="fc_sharing">
                                <div class="share_button">
                                    <i class="icon-share"></i>
                                </div>
                                <ul class="share_items">
                                    <li><a class="fa fa-facebook" href="#"></a></li>
                                    <li><a class="fa fa-twitter" href="#"></a></li>
                                    <li><a class="fa fa-google-plus" href="#"></a></li>
                                    <li><a class="fa fa-pinterest" href="#"></a></li>
                                    <li><a class="fa fa-linkedin" href="#"></a></li>
                                </ul>
                            </div>	

                        </div>
                    </div>

                </div>

            </article> 

            <article class="post-standard">
                <div class="post-header">
                    <div class="post-categoris">
                        <a href="#" style="background:#ca85ca">Environment</a><a href="#" style="background:#f4b23f">Gallery</a><a href="#" style="background:#e54e7e">Fashion</a>
                    </div>
                    <h2 class="post-title"><a href="blog_single.php">Things You Need To Know Before Hiking</a></h2>
                    <div class="post-meta">
                        <div><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></div>
                        <div><i class="icon-clock"></i>January 18, 2015</div>
                        <div><i class="icon-bubble"></i><a href="#" title="">5 Comments</a></div>
                        <div><i class="icon-folder"></i>in : <a href="#" rel="category tag">WordPress</a></div>
                        <div><a class="post-lik " title="Love"><i class="icon-heart"></i><span>35</span></a></div>
                        <div><i class="icon-eye"></i><span>216</span></div>
                    </div>
                </div>

                <div class="post-thumb">

                    <span class="featured_icon_post"><i class="icon-picture"></i></span>

                    <a href="blog_single.php" class="image-links">
                        <img src="http://placehold.it/796x461" alt="">    
                    </a>
                </div>

                <div class="post-content">
                    <p>Maecenas varius massa non libero sodales malesuada. Morbi in malesuada libero. Quisque ut blandit elit, in suscipit felis. Mauris nec purus sed nisi semper blandit sed a lectus. Sed ultrices, augue at iaculis rutrum, orci diam tincidunt velit, vel condimentum eros justo sit amet sem. Fusce eget justo eu.</p> 

                    <p>Dicat ludus erroribus has ea, corpora oportere vel ne. Accusam intellegebat delicatissimi eos et, sed eirmod atomorum hendrerit id, graeco putant labitur nec et. Populo epicurei scriptorem ut per. Sensibus consectetuer ius no, has te porro verterem prodesset. Mel natum placerat in. Pro iriure expetenda disputando ut, solum verterem sapientem te eum. […]</p>				
                </div>

                <div class="post-footer">

                    <div class="meta-item">
                        <a href="blog_single.php" class="read_more_button">Continue Reading<i class="icon-arrow-right"></i></a>
                    </div>


                    <div class="meta-item meta-item-share">
                        <div class="meta-item-wrapper">

                            <div class="fc_sharing">
                                <div class="share_button">
                                    <i class="icon-share"></i>
                                </div>
                                <ul class="share_items">
                                    <li><a class="fa fa-facebook" href="#"></a></li>
                                    <li><a class="fa fa-twitter" href="#"></a></li>
                                    <li><a class="fa fa-google-plus" href="#"></a></li>
                                    <li><a class="fa fa-pinterest" href="#"></a></li>
                                    <li><a class="fa fa-linkedin" href="#"></a></li>
                                </ul>
                            </div>	

                        </div>
                    </div>

                </div>

            </article>

            <div class="main-pagination">
                <span class="page-numbers current">1</span>
                <a class="page-numbers" href="#">2</a>
                <a class="page-numbers" href="#">3</a>
                <a class="page-numbers" href="#">4</a>
                <a class="next page-numbers" href="#">
                    <span class="visuallyhidden">Next</span><i class="fa fa-angle-right"></i>
                </a>	
            </div>

        </div>
    </div>
</div>
<!-- #content-wrapper -->
<?php include_once __DIR__ . '/footer.php'; ?>