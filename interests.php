<?php
$title = "Interests";
?>
<?php include_once __DIR__ . '/header.php' ?>
<div class="col-md-1 bhoechie-tab-menu" style="width:6%;background-image: url('images/sub_nav_menu_bg.png');">
    <div class="list-group">
        <a href="#" class="list-group-item active text-center" data-post_type="a-z">
            <h4 class="fa fa-2x fa-clock-o"></h4><br/>A-Z
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="a-z">
            <h4 class="fa fa-2x fa-user"></h4><br/>Only Mine
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="all posts">
            <h4 class="fa fa-2x fa-square"></h4><br/>Show All
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="media">
            <h4 class="fa fa-2x fa-music"></h4><br/>Media
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="collection">
            <h4 class="fa fa-2x fa-columns"></h4><br/><p style="margin-left: -10px;">Collections</p>
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="events">
            <h4 class="fa fa-2x fa-calendar"></h4><br/>Events 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="blogs">
            <h4 class="fa fa-2x fa-edit"></h4><br/>Blogs
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="Groups">
            <h4 class="fa fa-2x fa-users"></h4><br/>Groups 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="Pages">
            <h4 class="fa fa-2x fa-paste"></h4><br/>Pages 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="favorites">
            <h4 class="fa fa-2x fa-star"></h4><br/>Favorites 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="member">
            <h4 class="fa fa-2x fa-users"></h4><br/>Members
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="settings">
            <h4 class="fa fa-2x fa-cog"></h4><br/>Settings 
        </a>
    </div>
</div>
<div id="all_posts_div" class="col-md-7" style="<?php echo $f_name == 'index.php' ? '' : 'background-color: #fff;'; ?>padding: 5px 5px;width:50%">
    <div class="row-fluid" style="min-height: 600px">
        <div id="resume" class="container-fluid no-marg">

            <!-- .row -->
            <div class="row row_responsive">

                <!-- .section_general -->
                <div class="col-lg-11 section_general">

                    <header class="section-header" style="padding: 30px 0 0px 0;">
                        <h3 class="section-title">Interests</h3>
                        <div class="border-divider"></div>
                    </header>

                    <div class="row section_separate">
                        <div class="col-md-12">
                            <hr/>
                        </div>
                        <!-- .resume-right -->
                        <div class="col-md-6">

                            <div class="title-section">
                                <h2 class="section_title">Designs skills</h2>
                                <div class="sep2"></div>
                            </div>

                            <div class="skills">
                                <!-- .skillbar -->
                                <div class="skillbar clearfix" data-percent="95%">
                                    <div class="skillbar-title"><span>Photoshop</span></div>
                                    <div class="skillbar-bar" style="width: 95%;"></div>
                                    <div class="skill-bar-percent">95%</div>
                                </div>

                                <!-- /.skillbar -->

                                <!-- .skillbar -->
                                <div class="skillbar clearfix" data-percent="90%">
                                    <div class="skillbar-title"><span>Illustrateur</span></div>
                                    <div class="skillbar-bar" style="width: 90%;"></div>
                                    <div class="skill-bar-percent">90%</div>
                                </div>
                                <!-- /.skillbar -->

                                <!-- .skillbar -->
                                <div class="skillbar clearfix" data-percent="65%">
                                    <div class="skillbar-title"><span>Indesign</span></div>
                                    <div class="skillbar-bar" style="width: 65%;"></div>
                                    <div class="skill-bar-percent">65%</div>
                                </div>
                                <!-- /.skillbar -->

                                <!-- .skillbar -->
                                <div class="skillbar clearfix" data-percent="35%">
                                    <div class="skillbar-title"><span>Flash</span></div>
                                    <div class="skillbar-bar" style="width: 35%;"></div>
                                    <div class="skill-bar-percent">35%</div>
                                </div>
                                <!-- /.skillbar -->
                            </div>
                        </div>
                        <div class="col-md-6">

                            <!-- .title_content -->
                            <div class="title-section">
                                <h2 class="section_title">Programming skills</h2>
                                <div class="sep2"></div>
                            </div>
                            <!-- /.title_content -->

                            <div class="skills">
                                <!-- .skillbar -->
                                <div class="skillbar clearfix" data-percent="85%">
                                    <div class="skillbar-title"><span>Wordpress</span></div>
                                    <div class="skillbar-bar" style="width: 85%;"></div>
                                    <div class="skill-bar-percent">85%</div>
                                </div>
                                <!-- /.skillbar -->

                                <!-- .skillbar -->
                                <div class="skillbar clearfix" data-percent="75%">
                                    <div class="skillbar-title"><span>Joomla</span></div>
                                    <div class="skillbar-bar" style="width: 75%;"></div>
                                    <div class="skill-bar-percent">75%</div>
                                </div>
                                <!-- /.skillbar -->

                                <!-- .skillbar -->
                                <div class="skillbar clearfix" data-percent="60%">
                                    <div class="skillbar-title"><span>Drupal</span></div>
                                    <div class="skillbar-bar" style="width: 60%;"></div>
                                    <div class="skill-bar-percent">60%</div>
                                </div>
                                <!-- /.skillbar -->

                                <!-- .skillbar -->
                                <div class="skillbar clearfix" data-percent="89%">
                                    <div class="skillbar-title"><span>Php</span></div>
                                    <div class="skillbar-bar" style="width: 89%;"></div>
                                    <div class="skill-bar-percent">89%</div>
                                </div>
                                <!-- /.skillbar --> 
                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr/>
                        </div>
                        <div class="col-md-6">
                            <!-- .title_content -->
                            <div class="title-section">
                                <h2 class="section_title">Office skills</h2>
                                <div class="sep2"></div>
                            </div>
                            <!-- /.title_content -->

                            <div class="skills">       
                                <!-- .skillbar -->
                                <div class="skillbar clearfix" data-percent="85%">
                                    <div class="skillbar-title"><span>MS Excel</span></div>
                                    <div class="skillbar-bar" style="width: 85%;"></div>
                                    <div class="skill-bar-percent">85%</div>
                                </div>
                                <!-- /.skillbar -->

                                <!-- .skillbar -->
                                <div class="skillbar clearfix " data-percent="95%">
                                    <div class="skillbar-title"><span>MS Word</span></div>
                                    <div class="skillbar-bar" style="width: 95%;"></div>
                                    <div class="skill-bar-percent">95%</div>
                                </div>
                                <!-- /.skillbar -->

                                <!-- .skillbar -->
                                <div class="skillbar clearfix " data-percent="60%">
                                    <div class="skillbar-title"><span>Powerpoint</span></div>
                                    <div class="skillbar-bar" style="width: 60%;"></div>
                                    <div class="skill-bar-percent">60%</div>
                                </div>
                                <!-- /.skillbar -->

                                <!-- .skillbar -->
                                <div class="skillbar clearfix " data-percent="40%">
                                    <div class="skillbar-title"><span>SharePoint</span></div>
                                    <div class="skillbar-bar" style="width: 40%;"></div>
                                    <div class="skill-bar-percent">40%</div>
                                </div>
                                <!-- /.skillbar -->   
                            </div>
                        </div>
                        <div class="col-md-6">
                            <!-- .title_content -->
                            <div class="title-section">
                                <h2 class="section_title">Language skills</h2>
                                <div class="sep2"></div>
                            </div>
                            <!-- /.title_content -->

                            <div class="skills">
                                <!-- .skillbar -->
                                <div class="skillbar clearfix" data-percent="90%">
                                    <div class="skillbar-title"><span>English</span></div>
                                    <div class="skillbar-bar" style="width: 90%;"></div>
                                    <div class="skill-bar-percent">90%</div>
                                </div>
                                <!-- /.skillbar -->

                                <!-- .skillbar -->
                                <div class="skillbar clearfix " data-percent="80%">
                                    <div class="skillbar-title"><span>French</span></div>
                                    <div class="skillbar-bar" style="width: 80%;"></div>
                                    <div class="skill-bar-percent">80%</div>
                                </div>
                                <!-- /.skillbar -->

                                <!-- .skillbar -->
                                <div class="skillbar clearfix " data-percent="50%">
                                    <div class="skillbar-title"><span>Spanish</span></div>
                                    <div class="skillbar-bar" style="width: 50%;"></div>
                                    <div class="skill-bar-percent">50%</div>
                                </div>
                                <!-- /.skillbar -->

                                <!-- .skillbar -->
                                <div class="skillbar clearfix " data-percent="60%">
                                    <div class="skillbar-title"><span>Swiss</span></div>
                                    <div class="skillbar-bar" style="width: 60%;"></div>
                                    <div class="skill-bar-percent">60%</div>
                                </div>
                                <!-- /.skillbar -->

                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr/>
                        </div>
                        <div class="col-md-6">
                            <!-- .title_content -->
                            <div class="title-section">
                                <h2 class="section_title">Hobbies and interests</h2>
                                <div class="sep2"></div>
                            </div>
                            <!-- /.title_content -->

                            <div class="skills">
                                <!-- .skillbar -->
                                <div class="skillbar clearfix" data-percent="90%">
                                    <div class="skillbar-title"><span>Reading</span></div>
                                    <div class="skillbar-bar" style="width: 90%;"></div>
                                    <div class="skill-bar-percent">95%</div>
                                </div>
                                <!-- /.skillbar -->

                                <!-- .skillbar -->
                                <div class="skillbar clearfix " data-percent="80%">
                                    <div class="skillbar-title"><span>Biking</span></div>
                                    <div class="skillbar-bar" style="width: 80%;"></div>
                                    <div class="skill-bar-percent">85%</div>
                                </div>
                                <!-- /.skillbar -->

                                <!-- .skillbar -->
                                <div class="skillbar clearfix " data-percent="50%">
                                    <div class="skillbar-title"><span>football</span></div>
                                    <div class="skillbar-bar" style="width: 50%;"></div>
                                    <div class="skill-bar-percent">90%</div>
                                </div>
                                <!-- /.skillbar -->

                                <!-- .skillbar -->
                                <div class="skillbar clearfix " data-percent="60%">
                                    <div class="skillbar-title"><span>travel</span></div>
                                    <div class="skillbar-bar" style="width: 60%;"></div>
                                    <div class="skill-bar-percent">80%</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>	
            </div>
        </div>
    </div>
</div>
<!-- #content-wrapper -->
<?php include_once __DIR__ . '/footer.php'; ?>