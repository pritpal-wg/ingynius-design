<?php
$title = "Creat Company Page";
$pic_type = 'square';
$menus = array(
    'index.php' => array(
        'icon' => 'home',
        'label' => 'Home',
    ),
    'index-2.php' => array(
        'icon' => 'home',
        'label' => 'Home 2',
    ),
    'company.php' => array(
        'icon' => 'home',
        'label' => 'Company Home',
    ),
    'company_upgrade.php' => array(
        'icon' => 'wrench',
        'label' => 'Upgrade Account',
    ),
    'company_services.php' => array(
        'icon' => 'cog',
        'label' => 'Services',
    ),
    'company_blog.php' => array(
        'icon' => 'comments',
        'label' => 'Blog',
    ),
    'company_followers.php' => array(
        'icon' => 'users',
        'label' => 'Followers',
    ),
    'company_contact.php' => array(
        'icon' => 'paper-plane',
        'label' => 'Contact',
    ),
);
$show_quick_icons = FALSE;
?>
<?php include_once __DIR__ . '/header.php' ?>
<script>
    function getPage(page_id) {
        page_id = Number(page_id);
        var ret = false;
        var pages = $.parseJSON(localStorage.pages);
        $.each(pages, function (k, page) {
            if (page.id === page_id) {
                ret = page;
            }
        });
        return ret;
    }
</script>
<style>
    #main-content {
        padding: 0;
        margin: 10px;
    }
    .preview
    {
        width:400px;
        border:solid 1px #dedede;
        padding:10px;
        color:#cc0000;

    }
    #preview
    {
        color:#cc0000;
        font-size:12px
    }
</style>
<div class="col-md-1 bhoechie-tab-menu" style="width:6%;background-image: url('images/sub_nav_menu_bg.png');">
    <div class="list-group">
        <a href="#" class="list-group-item active text-center" data-post_type="a-z">
            <h4 class="fa fa-2x fa-clock-o"></h4><br/>A-Z
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="a-z">
            <h4 class="fa fa-2x fa-user"></h4><br/>Only Mine
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="all posts">
            <h4 class="fa fa-2x fa-square"></h4><br/>Show All
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="media">
            <h4 class="fa fa-2x fa-music"></h4><br/>Media
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="collection">
            <h4 class="fa fa-2x fa-columns"></h4><br/><p style="margin-left: -10px;">Collections</p>
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="events">
            <h4 class="fa fa-2x fa-calendar"></h4><br/>Events 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="blogs">
            <h4 class="fa fa-2x fa-edit"></h4><br/>Blogs
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="Groups">
            <h4 class="fa fa-2x fa-users"></h4><br/>Groups 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="Pages">
            <h4 class="fa fa-2x fa-paste"></h4><br/>Pages 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="favorites">
            <h4 class="fa fa-2x fa-star"></h4><br/>Favorites 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="member">
            <h4 class="fa fa-2x fa-users"></h4><br/>Members
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="settings">
            <h4 class="fa fa-2x fa-cog"></h4><br/>Settings 
        </a>
    </div>
</div>
<div id="all_posts_div" class="col-md-7" style="<?php echo $f_name == 'index.php' ? '' : 'background-color: #fff;'; ?>padding: 5px 5px;width:50%">
    <div class="row-fluid">
        <div class="col-md-12">
            <h2 class="page-header" style="margin: 40px 0 20px 15px">
                <span class="pull-right"style="margin-right: 15px;">
                    <a href="company_pages.php" class="btn btn-success"><i class="fa fa-list"></i> My Pages</a>
                </span>
                Set Up <span class="page_name"></span>
            </h2>
            <div class="row section_separate"> 
                <div class="col-md-12">
                    <div role="tabpanel">
                        <ul class="nav nav-tabs" role="tablist">
                            <li id="head_desktop" role="presentation" class="active"><a href="#desktop" aria-controls="desktop" role="tab" data-toggle="tab"><i class="fa fa-info-circle"></i> About</a></li>
                            <li id="head_mobile" role="presentation"><a href="#mobile" aria-controls="mobile" role="tab" data-toggle="tab"><i class="fa fa-camera"></i> Profile Picture</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">

                            <div role="tabpanel" class="tab-pane fade active in" id="desktop">
                                <p>
                                <div class="row-fluid">
                                    <div class="col-md-12">
                                        <form class="form form-horizontal">
                                            <div class="form-group">
                                                <label style="margin-bottom: 10px" class="control-label">Add Description About <span class="page_name"></span></label>
                                                <textarea id="page_info" class="form-control" rows="3"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <button type="button" id="update_pages" class="btn btn-success">Save Info</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                </p>
                            </div>

                            <div role="tabpanel" class="tab-pane fade" id="mobile">
                                <p style="margin-top: 15px">
                                <div class="row-fluid">
                                    <div class="col-md-4">
                                        <div id="targetLayer">
                                            <span id="page_photo"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <form class="form form-horizontal" id="uploadForm" action="upload.php" method="post">

                                            <div id="uploadFormLayer">
                                                <label>Upload Image File:</label><br/>
                                                <input name="userImage" type="file" class="inputFile" />
                                                <br/>
                                                <input type="submit" value="Submit" class="btnSubmit btn btn-success" />
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                            </p>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>
</div>
<script type="text/javascript" >
    $(document).ready(function (e) {
        $("#uploadForm").on('submit', (function (e) {
            e.preventDefault();
            $("#targetLayer").html('<center><img src="images/loader.gif"/></center>');
            $.ajax({
                url: "upload.php",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (data)
                {
                    $("#targetLayer").html(data);
                    var id = Number("<?php echo $_GET['p_id']; ?>");
                    var photo = $('#img_name').attr('data-name');
                    var pages = $.parseJSON(localStorage.pages);
                    var new_pages = [];
                    $.each(pages, function (l, v) {
                        if (v.id === id) {
                            v.photo = photo;
                        }
                        new_pages.push(v);
                    });
                    localStorage.pages = JSON.stringify(new_pages);
                },
                error: function ()
                {
                }
            });
        }));
    });
</script>
<script>
    $(document).on('click', '#update_pages', function () {
        var id = Number("<?php echo $_GET['p_id']; ?>");
        var info = $('#page_info').val();
        var pages = $.parseJSON(localStorage.pages);
        var new_pages = [];
        $.each(pages, function (l, v) {
            if (v.id === id) {
                v.description = info;
            }
            new_pages.push(v);
        });
        localStorage.pages = JSON.stringify(new_pages);
        $('#desktop').removeClass('active in');
        $('#mobile').addClass('active in');
        $('#head_desktop').removeClass('active');
        $('#head_mobile').addClass('active');
    });
    $(document).ready(function () {
        var key = "<?php echo $_GET['p_id']; ?>";
        var page = getPage(key);
        $('.page_name').html(page.name);
        $('#page_info').val(page.description);
        var photo_path = '<img class="img img-thumbnail" style="min-height: 150px" src="uploads/' + page.photo + '"/>';
        $('#page_photo').html(photo_path);
    });
</script>
<?php include_once __DIR__ . '/footer.php'; ?>