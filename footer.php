<div class="col-md-2" style="width:22%">
    <div id="custumize-style" style="right: -293px">
        <h1>Layout Color Switcher</h1>
        <a href="#" class="switcher"><span class="changebutton"><i class="fa fa-times"></i></span></a>
        <div class="switcher_content">
            <p>Choose one color below to change main layout color. You can apply more styles in theme options.</p>
            <div>
                <h3>Skins Colors</h3>
                <ul class="colors-style" id="color1">
                    <li><a href="#" class="color_1"></a></li>
                    <li><a href="#" class="color_2"></a></li>
                    <li><a href="#" class="color_3"></a></li>
                    <li><a href="#" class="color_4"></a></li>
                    <li><a href="#" class="color_5"></a></li>
                    <li><a href="#" class="color_6"></a></li>
                    <li><a href="#" class="color_7"></a></li>
                    <li><a href="#" class="color_8"></a></li>
                    <li><a href="#" class="color_9"></a></li>
                    <li><a href="#" class="color_10"></a></li>
                    <li><a href="#" class="color_11"></a></li>
                    <li><a href="#" class="color_12"></a></li>
                    <li><a href="#" class="color_13"></a></li>
                    <li><a href="#" class="color_14"></a></li>
                    <li><a href="#" class="color_15"></a></li>
                    <li><a href="#" class="color_16"></a></li>
                    <li><a href="#" class="color_17"></a></li>
                    <li><a href="#" class="color_18"></a></li>
                    <li><a href="#" class="color_19"></a></li>
                    <li><a href="#" class="color_20"></a></li>
                    <li><a href="#" class="color_21"></a></li>
                    <li><a href="#" class="color_22"></a></li>
                    <li><a href="#" class="color_23"></a></li>
                    <li><a href="#" class="color_24"></a></li>
                    <li><a href="#" class="color_25"></a></li>
                    <li><a href="#" class="color_26"></a></li>
                    <li><a href="#" class="color_27"></a></li>
                    <li><a href="#" class="color_28"></a></li>
                    <li><a href="#" class="color_29"></a></li>
                    <li><a href="#" class="color_30"></a></li>
                    <li><a href="#" class="color_31"></a></li>
                    <li><a href="#" class="color_32"></a></li>
                    <li><a href="#" class="color_33"></a></li>
                    <li><a href="#" class="color_34"></a></li>
                    <li><a href="#" class="color_35"></a></li>
                    <li><a href="#" class="color_36"></a></li>
                    <li><a href="#" class="color_37"></a></li>
                    <li><a href="#" class="color_38"></a></li>
                    <li><a href="#" class="color_39"></a></li>
                    <li><a href="#" class="color_40"></a></li>
                </ul>
            </div>
            <div>
                <div class="pull-right" style="padding-top: 2px">
                    <button class=" btn btn-primary btn-xs" id="menu_layout_toggle_button">
                        <i class="fa fa-refresh"></i>
                    </button>
                </div>
                <h3>Toggle Menu Layout</h3>
            </div>
            <div id="button-reset"><a href="#" class="button color green boxed">Reset</a></div>
        </div>
    </div>
    <div id="sidebar-wrapper" class="content_2 active">
        <div class="sidebar_closed" id="sidebar_closed" title="Close Sidebar" data-toggle="tooltip">
            <i class="icon-close"></i>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <h5 class="post_title_cl colors-color">
                        LATEST IMAGES
                    </h5>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#images_newest" aria-controls="home" role="tab" data-toggle="tab">Latest</a></li>
                        <li role="presentation"><a href="#images_featured" aria-controls="profile" role="tab" data-toggle="tab">Featured</a></li>
                        <li role="presentation"><a href="#images_popular" aria-controls="messages" role="tab" data-toggle="tab">Popular</a></li>
                        <li role="presentation"><a href="#images_all" aria-controls="settings" role="tab" data-toggle="tab">See All</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="images_newest">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="lightSlider">
                                        <li data-thumb="images/cover-1.jpg">
                                            <img class="img_options" src="images/cover-1.jpg" />
                                        </li>
                                        <li data-thumb="images/cover-2.jpg">
                                            <img class="img_options" src="images/cover-2.jpg" />
                                        </li>
                                        <li data-thumb="images/cover-3.jpg">
                                            <img class="img_options" src="images/cover-3.jpg" />
                                        </li>
                                        <li data-thumb="images/cover-4.jpg">
                                            <img class="img_options" src="images/cover-4.jpg" />
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="images_featured">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="lightSlider">
                                        <li data-thumb="images/cover-2.jpg">
                                            <img class="img_options" src="images/cover-2.jpg" />
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="images_popular">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="lightSlider">
                                        <li data-thumb="images/cover-1.jpg">
                                            <img class="img_options" src="images/cover-1.jpg" />
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="images_all">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="lightSlider">
                                        <li data-thumb="images/cover-4.jpg">
                                            <img class="img_options" src="images/cover-4.jpg" />
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <h5 class="post_title_cl colors-color">
                        LATEST VIDEOS
                    </h5>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#videos_newest" aria-controls="home" role="tab" data-toggle="tab">Latest</a></li>
                        <li role="presentation"><a href="#videos_featured" aria-controls="profile" role="tab" data-toggle="tab">Featured</a></li>
                        <li role="presentation"><a href="#videos_popular" aria-controls="messages" role="tab" data-toggle="tab">Popular</a></li>
                        <li role="presentation"><a href="#videos_all" aria-controls="settings" role="tab" data-toggle="tab">See All</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="videos_newest">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="lightSlider1">
                                        <li data-thumb="images/E6KwXYmMiak-play.jpg">
                                            <img class="video_options" data-video="E6KwXYmMiak" src="images/E6KwXYmMiak-play.jpg"/>
                                        </li>
                                        <li data-thumb="images/OargwriB8ns-play.jpg">
                                            <img class="video_options" data-video="OargwriB8ns" src="images/OargwriB8ns-play.jpg"/>
                                        </li>
                                        <li data-thumb="images/XZ4X1wcZ1GE-play.jpg">
                                            <img class="video_options" data-video="XZ4X1wcZ1GE" src="images/XZ4X1wcZ1GE-play.jpg"/>
                                        </li>
                                        <li data-thumb="images/xaSH0d60Zso-play.jpg">
                                            <img class="video_options" data-video="xaSH0d60Zso" src="images/xaSH0d60Zso-play.jpg"/>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="videos_featured">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="lightSlider1">
                                        <li data-thumb="images/XZ4X1wcZ1GE-play.jpg">
                                            <img class="video_options" data-video="XZ4X1wcZ1GE" src="images/XZ4X1wcZ1GE-play.jpg"/>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="videos_popular">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="lightSlider1">
                                        <li data-thumb="images/xaSH0d60Zso-play.jpg">
                                            <img class="video_options" data-video="xaSH0d60Zso" src="images/xaSH0d60Zso-play.jpg"/>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="videos_all">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="lightSlider1">
                                        <li data-thumb="images/E6KwXYmMiak-play.jpg">
                                            <img class="video_options" data-video="E6KwXYmMiak" src="images/E6KwXYmMiak-play.jpg"/>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5px;margin-top: 15px">
                <h5 class="post_title_cl colors-color">
                    UPCOMING EVENTS
                </h5>
            </div>
            <div class="col-md-12" style="margin-bottom: 10px;margin-top: 10px">
                <div id="datetimepicker12" style="background-color: #fff"></div>
            </div>
            <div class="col-md-12" style="margin-bottom: 10px;border-bottom: 1px solid #ddd">
                <div class="col-md-2">
                    <center>
                        <div style="color: orange;">OCT</div>
                        <div style="font-size: 30px;margin-top: -5px;color: #999999;">16</div>
                    </center>
                </div>
                <div class="col-md-6" style="color:#fff">
                    <div style="color: #999;">09:00 PM</div>
                    Architectire Conference
                </div>
                <div class="col-md-4">
                    <img src="images/cover-1.jpg" class="img img-responsive events_img"/>
                </div>
            </div>
            <div class="col-md-12" style="margin-bottom: 10px;border-bottom: 1px solid #ddd">
                <div class="col-md-2">
                    <center>
                        <div style="color: orange;">OCT</div>
                        <div style="font-size: 30px;margin-top: -5px;color: #999999;">18</div>
                    </center>
                </div>
                <div class="col-md-6" style="color:#fff">
                    <div style="color: #999;">11:00 AM</div>
                    Test Event Conference
                </div>
                <div class="col-md-4">
                    <img src="images/cover-2.jpg" class="img img-responsive events_img"/>
                </div>
            </div>
            <div class="col-md-12" style="margin-bottom: 10px;border-bottom: 1px solid #ddd">
                <div class="col-md-2">
                    <center>
                        <div style="color: orange;">NOV</div>
                        <div style="font-size: 30px;margin-top: -5px;color: #999999;">11</div>
                    </center>
                </div>
                <div class="col-md-6" style="color:#fff">
                    <div style="color: #999;">06:00 PM</div>
                    Diwali Festival
                </div>
                <div class="col-md-4">
                    <img src="images/cover-3.jpg" class="img img-responsive events_img"/>
                </div>
            </div>
            <div class="clearfix" style="margin-bottom: 50px"></div>
        </div>

        <!-- #sidebar -->

        <a id="go-top-button" class="test" href="#"><i class="fa fa-chevron-up"></i></a>
        <!--go-top-button-->

    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="z-index:10000000">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Sample Post Title</h4>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                    <div class="col-md-8">
                        <div class="row" style="margin-bottom: 10px">
                            <div class="col-md-12">
                                <div id="content_data"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <span style="font-size: 15px"><i style="color:darkred" class="fa fa-user"></i> John Doe</span>
                                <span style="color:#999;margin-left: 10px"><i style="color:darkred" class="fa fa-clock-o"></i> Posted on 2015-10-02</span>
                                <span style="color:#999;margin-left: 10px"><i style="color:darkred" class="fa fa-map-marker"></i> Sydney, Australia</span>
                                <span style="color:#999;margin-left: 10px"><i style="color:darkred" class="fa fa-arrows-alt"></i> <a id="post_link_attr" style="color:#3b5998" href="">View More</a></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-12" style="max-height: 350px; overflow: auto;background-color: #eee;padding-top: 10px">
                            <?php for ($i = 0; $i < 6; $i++) { ?>
                                <div class="row" style="margin-bottom: 10px;padding-bottom: 5px;border-bottom: 1px #fff solid;">
                                    <div class="col-md-4">
                                        <img class="img img-circle" src="images/cover-1.jpg" alt="Logo">
                                    </div>
                                    <div class="col-md-8">
                                        <div style="margin-left: -20px;">
                                            <span style="color:grey">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry.</span>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="row">
                                <div class="col-md-4">
                                    <img class="img img-circle" src="images/cover-2.jpg" alt="Logo">
                                </div>
                                <div class="col-md-8">
                                    <div style="margin-left: -20px;">
                                        <div class="form-group">
                                            <input placeholder="Post Comment..." type="text" class="form-control"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="border:none"></div>
        </div>
    </div>
</div>
<div class="modal fade" id="post_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body" style="height: auto;overflow: visible !important">
                <div id="post_content"></div>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
</div>
</body>
</html>