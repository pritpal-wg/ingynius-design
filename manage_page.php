<?php
$title = "Page Info";
?>
<?php include_once __DIR__ . '/header.php' ?>
<script>
    function getPage(page_id) {
        page_id = Number(page_id);
        var ret = false;
        var pages = $.parseJSON(localStorage.pages);
        $.each(pages, function (k, page) {
            if (page.id === page_id) {
                ret = page;
            }
        });
        return ret;
    }
</script>
<style>
    #main-content {
        padding: 0;
        margin: 0;
    }
    a.list-group-item.active, a.list-group-item.active:hover, a.list-group-item.active:focus {
        z-index: 1 !important;
    }
    a.list-group-item {
        color: #fff;
    }
    .list-group-item {
        position: relative;
        display: block;
        padding: 10px 15px;
        margin-bottom: -1px;
        background-color: transparent;
        border: 0px solid #ddd;
    }
    a.list-group-item:hover, a.list-group-item:focus {
        text-decoration: none;
        background-color: transparent;
    }
    h4.fa {
        font-size: 35px;
    }
    div.bhoechie-tab-content {
        margin-top: 0;
    }
</style>
<div class="col-md-1 bhoechie-tab-menu" style="width:6%;background-image: url('images/sub_nav_menu_bg.png');">
    <div class="list-group">
        <a href="#" class="list-group-item active text-center">
            <h4 class="fa fa-2x fa-cog"></h4><br/>General
        </a>
        <a href="#" class="list-group-item text-center">
            <h4 class="fa fa-2x fa-envelope"></h4><br/>Messaging
        </a>
        <a href="#" class="list-group-item text-center">
            <h4 class="fa fa-2x fa-info-circle"></h4><br/>Page Info
        </a>
        <a href="#" class="list-group-item text-center">
            <h4 class="fa fa-2x fa-globe"></h4><br/>Notifications
        </a>
        <a href="#" class="list-group-item text-center">
            <h4 class="fa fa-2x fa-user"></h4><br/>Page Roles
        </a>
        <a href="#" class="list-group-item text-center">
            <h4 class="fa fa-2x fa-star"></h4><br/>Featured 
        </a>
        <a href="#" class="list-group-item text-center">
            <h4 class="fa fa-2x fa-list-ul"></h4><br/>Activity Log
        </a>
    </div>
</div>
<div id="all_posts_div" class="col-md-7" style="<?php echo $f_name == 'index.php' ? '' : 'background-color: #fff;'; ?>padding: 5px 5px;width:50%">
    <section id="service" class="layers">
        <!-- .page_content -->
        <div class="page_content">
            <!-- .container-fluid -->
            <div class="container-fluid no-marg">
                <!-- .row -->
                <div class="row row_responsive" style="margin-left: -6%;">
                    <!-- .section_general -->
                    <div class="col-lg-12">
                        <div class="row section_separate"> 
                            <div class="col-md-12 bhoechie-tab-container" style="margin-top: -20px">
                                <div class="col-md-12 profile-bg"></div>
                                <div class="col-md-12 bhoechie-tab" style="min-height: 504px;margin-top: 20px;">
                                    <!-- flight section -->
                                    <div class="bhoechie-tab-content active">
                                        <h2 class="page-header" style="margin: 0 0 20px 15px">
                                            <span class="pull-right" style="margin-right: 15px;">
                                                <a href="company_pages.php" class="btn btn-success"><i class="fa fa-list"></i> My Pages</a>
                                            </span>
                                            General
                                        </h2>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label" for="exampleInputAmount">Favorites</label>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="">
                                                        Add Page to your favorites
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label" for="exampleInputAmount">Page Visibility</label>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="">
                                                        Unpublish Page
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label" for="exampleInputAmount">Visitor Posts</label>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" value="" checked="">
                                                        Allow visitors to the Page to publish posts
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="">
                                                        Allow photo and video posts
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="">
                                                        Review posts by other people before they are published to the Page
                                                    </label>
                                                </div>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" value="">
                                                        Disable posts by other people on the Page
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label" for="exampleInputAmount">Messages</label>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="" checked="">
                                                        Allow people to contact my Page privately by showing the Message button
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label" for="exampleInputAmount">Age Restrictions</label>
                                                <div class="input-group">
                                                    <select class="form-control">
                                                        <option value="0">Select Age Restrictions:</option>
                                                        <option value="0" selected="1">Anyone (13+)</option>
                                                        <option value="17">People 17 and over</option>
                                                        <option value="18">People 18 and over</option>
                                                        <option value="19">People 19 and over</option>
                                                        <option value="21">People 21 and over</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label" for="exampleInputAmount">Tagging Ability</label>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="">
                                                        Allow others to tag photos and videos published by Test Company
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label" for="exampleInputAmount">Page Moderation</label>
                                                <div class="input-group">
                                                    <textarea placeholder="Add words to block, seperated by commas" class="form-control" rows="3"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label" for="exampleInputAmount">Similar Page Suggestions</label>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="">
                                                        Include Test Company when recommending similar Pages people might like on a Page timeline.
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label" for="exampleInputAmount">Comment Ranking</label>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="">
                                                        See most relevant comments by default.
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label" for="exampleInputAmount">Remove Page</label>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" value="" checked="">
                                                        <a href="" style="color: #3b5998">Permanently delete <span class="page_name"></span></a>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" style="margin-bottom: 20px;">
                                            <a href="" class="btn btn-success">
                                                Save
                                            </a>
                                        </div>
                                    </div>
                                    <!-- train section -->
                                    <div class="bhoechie-tab-content">
                                        <h2 class="page-header" style="margin: 0 0 20px 15px">
                                            <span class="pull-right" style="margin-right: 15px;">
                                                <a href="company_pages.php" class="btn btn-success"><i class="fa fa-list"></i> My Pages</a>
                                            </span>
                                            Messaging
                                        </h2>
                                        <div class="row-fluid">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label" for="exampleInputAmount">Your Response Time</label>
                                                    <div class="input-group">
                                                        Your response time will only be visible on your Page if you visit your Page at least once a week and answer 90% or more of your messages. You can choose the option you think best represents how quickly you reply to messages, or have your response time updated automatically.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" style="margin-left: 5px">
                                                    <label class="control-label" for="exampleInputAmount">Response Time Display</label>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" value="" checked="">
                                                            Automatically show your average response time for messages (recommended)
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" value="">
                                                            Typically replies in minutes
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" value="">
                                                            Typically replies within an hour
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" value="">
                                                            Typically replies in a few hours
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" value="">
                                                            Typically replies in a day
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12" style="margin-bottom: 20px;">
                                                <div class="col-md-6"></div>
                                                <div class="col-md-6">
                                                    <a href="" class="btn btn-success">
                                                        Save
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- hotel search -->
                                    <div class="bhoechie-tab-content">
                                        <h2 class="page-header" style="margin: 0 0 20px 15px">
                                            <span class="pull-right" style="margin-right: 15px;">
                                                <a href="company_pages.php" class="btn btn-success"><i class="fa fa-list"></i> My Pages</a>
                                            </span>
                                            Page Info
                                        </h2>
                                        <div class="row-fluid">
                                            <form class="form-horizontal">
                                                <div class="col-md-6">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label" for="exampleInputAmount">Name</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon"><i class="fa fa-heart-o"></i></div>
                                                                <input id="page_name" type="text" class="form-control" id="exampleInputAmount" placeholder="Page Name">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label" for="exampleInputAmount">Start Date</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                                                <input type="date" class="form-control" id="exampleInputAmount" placeholder="YYYY">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label" for="exampleInputAmount">Description</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon"><i class="fa fa-info-circle"></i></div>
                                                                <textarea id="page_info" placeholder="Page Info" rows="3" class="form-control"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label" for="exampleInputAmount">Phone</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon"><i class="fa fa-phone"></i></div>
                                                                <input type="number" class="form-control" id="exampleInputAmount" placeholder="9876543210">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label" for="exampleInputAmount">Email</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon"><i class="fa fa-at"></i></div>
                                                                <input type="text" class="form-control" id="exampleInputAmount" placeholder="email@email.com">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label" for="exampleInputAmount">Website</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon"><i class="fa fa-globe"></i></div>
                                                                <input type="text" class="form-control" id="exampleInputAmount" placeholder="http://www.wxample.com">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label" for="exampleInputAmount">Address</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
                                                                <input type="text" class="form-control" id="exampleInputAmount" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label" for="exampleInputAmount">City/Town</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
                                                                <input type="text" class="form-control" id="exampleInputAmount" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label" for="exampleInputAmount">Postal Code</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
                                                                <input type="number" class="form-control" id="exampleInputAmount" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label" for="exampleInputAmount">Page ID</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon"><i class="fa fa-info"></i></div>
                                                                <input type="text" class="form-control" id="exampleInputAmount" value="<?php echo $_GET['p_id']; ?>" disabled>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12" style="margin-bottom: 20px;">
                                                    <a href="" class="btn btn-success">
                                                        Save
                                                    </a>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="bhoechie-tab-content">
                                        <h2 class="page-header" style="margin: 0 0 20px 15px">
                                            <span class="pull-right" style="margin-right: 15px;">
                                                <a href="company_pages.php" class="btn btn-success"><i class="fa fa-list"></i> My Pages</a>
                                            </span>
                                            Notifications
                                        </h2>
                                        <div class="row-fluid">
                                            <div class="col-md-12">
                                                <div class="form-group" style="margin-left: 5px">
                                                    <label class="control-label" for="exampleInputAmount">Messages</label>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" value="" checked="">
                                                            Get a notification each time your Page receives a message.
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" value="">
                                                            Off
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group" style="margin-left: 5px">
                                                    <label class="control-label" for="exampleInputAmount">Email</label>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" value="" checked="">
                                                            Get an email each time there is activity on your Page or an important Page update.
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" value="">
                                                            Off
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group" style="margin-left: 5px">
                                                    <label class="control-label" for="exampleInputAmount">Text Messages</label>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" value="" checked="">
                                                            On
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" value="">
                                                            Off
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12" style="margin-bottom: 20px;">
                                                <a href="" class="btn btn-success">
                                                    Save
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="bhoechie-tab-content">
                                        <h2 class="page-header" style="margin: 0 0 20px 15px">
                                            <span class="pull-right" style="margin-right: 15px;">
                                                <a href="company_pages.php" class="btn btn-success"><i class="fa fa-list"></i> My Pages</a>
                                            </span>
                                            Page Roles
                                        </h2>
                                        <div class="row-fluid">
                                            <form class="form-horizontal">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label" for="exampleInputAmount">Select User</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-heart-o"></i></div>
                                                            <select class="form-control">
                                                                <option value="0">---</option>
                                                                <option value="1">User 1</option>
                                                                <option value="1">User 2</option>
                                                                <option value="1">User 3</option>
                                                                <option value="1">User 4</option>
                                                                <option value="1">User 5</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label" for="exampleInputAmount">Select Role</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-heart-o"></i></div>
                                                            <select class="form-control">
                                                                <option value="1">Admin</option>
                                                                <option value="1">Editor</option>
                                                                <option value="1">Moderator</option>
                                                                <option value="1">Advertiser</option>
                                                                <option value="1">Analyst</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12" style="padding-left: 0;margin-bottom: 20px;">
                                                    <a href="" class="btn btn-success">
                                                        Save
                                                    </a>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="bhoechie-tab-content">
                                        <form class="form-horizontal">
                                            <h2 class="page-header" style="margin: 0 0 20px 15px">
                                                <span class="pull-right" style="margin-right: 15px;">
                                                    <a href="company_pages.php" class="btn btn-success"><i class="fa fa-list"></i> My Pages</a>
                                                </span>
                                                Featured
                                            </h2>
                                            <div class="row-fluid">
                                            </div>
                                        </form>
                                    </div>
                                    <div class="bhoechie-tab-content">
                                        <h2 class="page-header" style="margin: 0 0 20px 15px">
                                            <span class="pull-right" style="margin-right: 15px;">
                                                <a href="company_pages.php" class="btn btn-success"><i class="fa fa-list"></i> My Pages</a>
                                            </span>
                                            Activity Log
                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End .section_general -->
                </div>
                <!-- End .row -->
            </div>
            <!-- End .container-fluid -->
        </div>
        <!-- End .page_content -->
    </section>
</div>

<script>
    $(document).ready(function () {
        var key = "<?php echo $_GET['p_id']; ?>";
        var page = getPage(key);
        $('#page_name').val(page.name);
        $('.page_name').html(page.name);
        $('#page_info').val(page.description);
        var photo_path = '<img class="img img-thumbnail" width="250px" height="250px" src="uploads/' + page.photo + '"/>';
        $('#page_photo').html(photo_path);
    });
</script>
<?php
include_once __DIR__ . '/footer.php';
