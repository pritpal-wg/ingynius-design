<?php
$title = "Following";
?>
<?php include_once __DIR__ . '/header.php' ?>
<style>
    #main-content {
        padding: 0;
        margin: 10px;
    }
    .blog-content-grid article .post-body {
        padding: 5px 0;
    }
    .blog-content-grid article h3.post-title {
        text-align: center;
    }
</style>
<div id="content-wrapper">
    <div id="main-content">


        <section class="blog-content-grid">
            <div class="row">
                <?php for ($i = 0; $i < 20; $i++) { ?>
                    <!-- post -->
                    <div class="col-md-3 col-sm-6">

                        <article>

                            <div class="post-thumb">
                                <a href="blog_single.php" class="image-link">
                                    <img src="http://placehold.it/333x222">  
                                </a>					
                            </div>

                            <div class="post-body">
                                <h3 class="post-title"><a href="blog_single.php">Following <?php echo $i + 1 ?></a></h3>
                                <div class="post-meta" hidden="">
                                    <ul>
                                        <li><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></li>
                                        <li class="separate_li">|</li>
                                        <li><i class="icon-clock"></i>January 18, 2015</li>
                                        <li class="separate_li">|</li>
                                        <li><i class="icon-folder"></i><a href="#" rel="category tag">WordPress</a></li>
                                    </ul>
                                </div>
                                <div class="post-content" hidden>
                                    <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat. Mauris sit amet sapien eget lacus...</p>
                                </div>
                                <!--<a href="blog_single.php" class="read_more_but">Continue Reading</a>-->
                                <div class="footer_post" hidden>
                                    <ul>
                                        <li><i class="icon-picture"></i></li>
                                        <li><i class="icon-bubble"></i> <a href="#">4</a></li>
                                        <li><i class="icon-heart"></i> <a href="#">35</a></li>
                                        <li><i class="icon-eye"></i> 216</li>
                                    </ul>
                                </div>
                            </div>
                        </article>
                    </div>
                    <!-- //post -->
                <?php } ?>
            </div>
        </section>

        <div class="main-pagination">
            <span class="page-numbers current">1</span>
            <a class="page-numbers" href="#">2</a>
            <a class="page-numbers" href="#">3</a>
            <a class="page-numbers" href="#">4</a>
            <a class="next page-numbers" href="#">
                <span class="visuallyhidden">Next</span><i class="fa fa-angle-right"></i>
            </a>	
        </div>
    </div>

</div>
<?php include_once __DIR__ . '/footer.php'; ?>