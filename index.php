<?php
$title = "Home 1";
include_once __DIR__ . '/header.php'
?>
<style>
    .bhoechie-tab-content {
        margin-top:0px !important;
    }
</style>
<div class="col-md-1 bhoechie-tab-menu" style="width:6%;background-image: url('images/sub_nav_menu_bg.png');">
    <div class="list-group">
        <a href="#" class="list-group-item active text-center" data-post_type="a-z">
            <h4 class="fa fa-2x fa-clock-o"></h4><br/>A-Z
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="a-z">
            <h4 class="fa fa-2x fa-user"></h4><br/>Only Mine
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="all posts">
            <h4 class="fa fa-2x fa-square"></h4><br/>Show All
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="media">
            <h4 class="fa fa-2x fa-music"></h4><br/>Media
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="collection">
            <h4 class="fa fa-2x fa-columns"></h4><br/><p style="margin-left: -10px;">Collections</p>
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="events">
            <h4 class="fa fa-2x fa-calendar"></h4><br/>Events 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="blogs">
            <h4 class="fa fa-2x fa-edit"></h4><br/>Blogs
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="Groups">
            <h4 class="fa fa-2x fa-users"></h4><br/>Groups 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="Pages">
            <h4 class="fa fa-2x fa-paste"></h4><br/>Pages 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="favorites">
            <h4 class="fa fa-2x fa-star"></h4><br/>Favorites 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="member">
            <h4 class="fa fa-2x fa-users"></h4><br/>Members
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="settings">
            <h4 class="fa fa-2x fa-cog"></h4><br/>Settings 
        </a>
    </div>
</div>
<div id="all_posts_div" class="col-md-7" style="<?php echo $f_name == 'index.php' ? '' : 'background-color: #fff;'; ?>padding: 0px 5px;width:50%">
    <div class="col-md-12" style="padding:2.5px">
        <div class="col-md-4">
            <div class="row" >
                <div style="margin-top:10px">
                    <h5 class="post_title_cl colors colors-color">
                        SEARCH
                    </h5>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                            <button style="margin-right: 10px;" class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <div style="margin-top:10px">
                    <h5 class="post_title_cl colors colors-color">
                        ADD POST
                    </h5>
                    <div style="text-align: center;">
                        <div class="col-md-2" id="post_text" style="cursor:pointer;padding: 1px;">
                            <i class="fa fa-font fa-2x  colors-color-hover" style="color:#FFFFFF"></i><br/>
                            <span style="display:none">Text</span>
                        </div>
                        <div class="col-md-2" id="post_photo" style="cursor:pointer;padding: 1px;">
                            <i class="fa fa-camera fa-2x  colors-color-hover" style="color:#FFFFFF"></i><br/>
                            <span style="display:none">Photo</span>
                        </div>
                        <div class="col-md-2" id="post_link" style="cursor:pointer;padding: 1px;">
                            <i class="fa fa-link fa-2x  colors-color-hover" style="color:#FFFFFF"></i><br/>
                            <span style="display:none">Link</span>
                        </div>
                        <div class="col-md-2" id="post_audio" style="cursor:pointer;padding: 1px;">
                            <i class="fa fa-volume-up fa-2x  colors-color-hover" style="color:#FFFFFF"></i><br/>
                            <span style="display:none">Audio</span>
                        </div>
                        <div class="col-md-2" id="post_video" style="cursor:pointer;padding: 1px;">
                            <i class="fa fa-film fa-2x  colors-color-hover" style="color:#FFFFFF"></i><br/>
                            <span style="display:none">Video</span>
                        </div>
                        <div class="col-md-2" id="post_event" style="cursor:pointer;padding: 1px;">
                            <i class="fa fa-calendar fa-2x  colors-color-hover" style="color:#FFFFFF"></i><br/>
                            <span style="display:none">Add Event</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-12" style="margin-left: -10px;">
                    <div class="card" style="box-shadow: none">
                        <h5 class="post_title_cl colors-color">
                            SELECT CLIQUE
                        </h5>
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#only_me" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-user fa-2x"></i><br/>Only Me</a></li>
                            <li role="presentation"><a href="#friends" aria-controls="profile" role="tab" data-toggle="tab"><i class="fa fa-users fa-2x"></i><br/>Friends</a></li>
                            <li role="presentation"><a href="#acq" aria-controls="messages" role="tab" data-toggle="tab"><i class="fa fa-paw fa-2x"></i><br/>Acq</a></li>
                            <li role="presentation"><a href="#prof" aria-controls="settings" role="tab" data-toggle="tab"><i class="fa fa-suitcase fa-2x"></i><br/>Prof</a></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="padding: 0">
        <div class="bhoechie-tab" style="min-height: 856px">
            <div class="bhoechie-tab-content active">
                <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                    <div class="item" data-type="text">
                        <div class="row item_content">
                            <div class="col-md-2" style="width:65px">
                                <img src="images/cover-3.jpg" class="img img-circle" style="height:35px"/>
                            </div>
                            <div class="col-md-8" style="margin-left: -20px;">
                                <div class="post_user_detail">
                                    <span class="p_bold">John Doe</span>said<br/><span class="p_bold">15 mins ago</span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="post_content">
                                    Closed That Deal I've been waiting on..
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" data-type="media">
                        <div class="row item_content">
                            <div class="col-md-2" style="width:65px">
                                <img src="images/cover-1.jpg" class="img img-circle" style="height:35px"/>
                            </div>
                            <div class="col-md-8" style="margin-left: -20px;">
                                <i><b>John Doe</b></i>  <font style="color:#999">said<br/>15 mins ago</font>
                            </div>
                            <div class="col-md-12" style="padding: 0px;">
                                <div class="post_content" style="padding:5px">
                                    Visit with sis and mon and dad..
                                </div>
                                <br/>
                                <div class="img-list">
                                    <img class="img post_photo_img img_options post_video_size" src="images/cover-1.jpg" />
                                    <span class="text-content">
                                        <span>
                                            <i class="fa fa-arrows-alt  img_options" src="images/cover-1.jpg" style="padding:10px"></i>
                                            <i class="fa fa-edit  img_options" src="images/cover-1.jpg"></i>
                                            <i class="fa fa-star  later" style="margin-top:-10px;margin-left: 18%;position: absolute;margin-top: -10px;">&nbsp;88</i>
                                            <i class="fa fa-thumb-tack  later" src="images/cover-1.jpg" style="padding: 8%;margin-left: 11%;position: absolute;">&nbsp;77</i>
                                            <i class="fa fa-arrow-down  later" src="images/cover-1.jpg" style="margin-top:10px;padding: 15%;margin-left: 3%;position: absolute;margin-top: 10px;">&nbsp;137</i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-6 p_padding_left">
                                <div class="photo_content">
                                    <span class="p_bold">Wisterio</span><span class="p_bold"></span><br />@username
                                </div>
                            </div>
                            <div class="col-md-6 p_padding_right">
                                <span class="smiley">&#9786;</span>10 
                                <span class="smiley2">&#9785;</span>0 
                                <span class="smiley2">&#9825;</span>18
                            </div>
                            <div class="col-md-12 photo_comments">
                                <div class="col-md-2 comments_col_width">
                                    <img src="images/cover-1.jpg" class="img img-circle" style="height:35px"/>
                                </div>
                                <div class="col-md-9" style="margin-left: -20px;">
                                    <div class="pull-right"><i class="fa fa-comment"></i> 3</div>
                                    <div class="post_user_detail">
                                        <span class="p_bold">Brenda Smith</span><br/>
                                        <span class="photo_content">Hey Aunt @Wis, I wish I could have been there</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" data-type="text">
                        <div class="row item_content">
                            <div class="col-md-2" style="width:65px">
                                <img src="images/cover-3.jpg" class="img img-circle" style="height:35px"/>
                            </div>
                            <div class="col-md-8" style="margin-left: -20px;">
                                <div class="post_user_detail">
                                    <span class="p_bold">John Doe</span>  said<br/><span class="p_bold">15 mins ago</span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="post_content">
                                    Closed That Deal I've been waiting on..
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" data-type="media">
                        <div class="row item_content">
                            <div style="padding:5px;">
                                <div class="col-md-2" style="width:65px">
                                    <img src="images/cover-3.jpg" class="img img-circle" style="height:35px"/>
                                </div>
                                <div class="col-md-8" style="margin-left: -20px;">
                                    <i><b>John Doe</b></i>  <font style="color:#999">said<br/>15 mins ago</font>
                                </div>
                            </div>
                            <div class="col-md-12" style="padding:0px;">
                                <div class="post_content" style="padding:5px">
                                    Visit with sis and mon and dad..
                                </div>
                                <br/>
                                <div class="img-list">
                                    <img class="video_options post_video_size" data-video="E6KwXYmMiak" src="images/E6KwXYmMiak-play.jpg" />
                                    <span class="text-content">
                                        <span>
                                            <i class="fa fa-arrows-alt  video_options" data-video="E6KwXYmMiak" src="images/E6KwXYmMiak-play.jpg" style="padding:10px"></i>
                                            <i class="fa fa-edit video_options" src="images/cover-1.jpg"></i>
                                            <i class="fa fa-star  later" style="margin-top:-10px;margin-left: 18%;position: absolute;margin-top: -10px;">&nbsp;88</i>
                                            <i class="fa fa-thumb-tack  later" src="images/cover-1.jpg" style="padding: 8%;margin-left: 11%;position: absolute;">&nbsp;77</i>
                                            <i class="fa fa-arrow-down  later" src="images/cover-1.jpg" style="margin-top:10px;padding: 15%;margin-left: 3%;position: absolute;margin-top: 10px;">&nbsp;137</i>
                                        </span>
                                    </span>

                                </div>
                            </div>
                            <div class="col-md-6 p_padding_left">
                                <div class="photo_content">
                                    <span class="p_bold">Wisterio</span><span class="p_bold"></span><br />@username
                                </div>
                            </div>
                            <div class="col-md-6 p_padding_right">
                                <span class="smiley">&#9786;</span>10 
                                <span class="smiley2">&#9785;</span>0 
                                <span class="smiley2">&#9825;</span>18
                            </div>
                            <div class="col-md-12 photo_comments">
                                <div class="col-md-2 comments_col_width">
                                    <img src="images/cover-3.jpg" class="img img-circle" style="height:35px"/>
                                </div>
                                <div class="col-md-9" style="margin-left: -20px;">
                                    <div class="pull-right"><i class="fa fa-comment"></i> 3</div>
                                    <div class="post_user_detail">
                                        <span class="p_bold">Ivan Moore</span><br/>
                                        <span class="photo_content">Hey Aunt @Wis, I wish I could have been there</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" data-type="text">
                        <div class="row item_content">
                            <div class="col-md-2" style="width:65px">
                                <img src="images/cover-3.jpg" class="img img-circle" style="height:35px"/>
                            </div>
                            <div class="col-md-8" style="margin-left: -20px;">
                                <div class="post_user_detail">
                                    <span class="p_bold">John Doe</span>said<br/><span class="p_bold">15 mins ago</span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="post_content">
                                    Closed That Deal I've been waiting on..
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" data-type="media">
                        <div class="row item_content">
                            <div style="padding:5px">
                                <div class="col-md-2" style="width:65px">
                                    <img src="images/cover-3.jpg" class="img img-circle" style="height:35px"/>
                                </div>
                                <div class="col-md-8" style="margin-left: -20px;">
                                    <i><b>John Doe</b></i>  <font style="color:#999">said<br/>15 mins ago</font>
                                </div>
                            </div>
                            <div class="col-md-12" style="padding:0px">
                                <div class="post_content" style="padding:5px">
                                    Visit with sis and mon and dad..
                                </div>
                                <br/>
                                <div class="img-list">
                                    <img class="video_options post_video_size" data-video="OargwriB8ns" src="images/OargwriB8ns-play.jpg" />
                                    <span class="text-content">
                                        <span>
                                            <i class="fa fa-arrows-alt  video_options" data-video="OargwriB8ns" src="images/OargwriB8ns-play.jpg" style="padding:10px"></i>
                                            <i class="fa fa-edit video_options" src="images/cover-1.jpg"></i>
                                            <i class="fa fa-star  later" style="margin-top:-10px;margin-left: 18%;position: absolute;margin-top: -10px;">&nbsp;88</i>
                                            <i class="fa fa-thumb-tack  later" src="images/cover-1.jpg" style="padding: 8%;margin-left: 11%;position: absolute;">&nbsp;77</i>
                                            <i class="fa fa-arrow-down  later" src="images/cover-1.jpg" style="margin-top:10px;padding: 15%;margin-left: 3%;position: absolute;margin-top: 10px;">&nbsp;137</i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-6 p_padding_left">
                                <div class="photo_content">
                                    <span class="p_bold">Wisterio</span><span class="p_bold"></span><br />@username
                                </div>
                            </div>
                            <div class="col-md-6 p_padding_right">
                                <span class="smiley">&#9786;</span>10 
                                <span class="smiley2">&#9785;</span>0 
                                <span class="smiley2">&#9825;</span>18
                            </div>
                            <div class="col-md-12 photo_comments">
                                <div class="col-md-2 comments_col_width">
                                    <img src="images/cover-3.jpg" class="img img-circle" style="height:35px"/>
                                </div>
                                <div class="col-md-9" style="margin-left: -20px;">
                                    <div class="pull-right"><i class="fa fa-comment"></i> 3</div>
                                    <div class="post_user_detail">
                                        <span class="p_bold">Ivan Moore</span><br/>
                                        <span class="photo_content">Hey Aunt @Wis, I wish I could have been there</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" data-type="text">
                        <div class="row item_content">
                            <div class="col-md-2" style="width:65px">
                                <img src="images/cover-3.jpg" class="img img-circle" style="height:35px"/>
                            </div>
                            <div class="col-md-8" style="margin-left: -20px;">
                                <div class="post_user_detail">
                                    <span class="p_bold">John Doe</span>  said<br/><span class="p_bold">15 mins ago</span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="post_content">
                                    Closed That Deal I've been waiting on..
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" data-type="media">
                        <div class="row item_content">
                            <div style="padding:5px;">
                                <div class="col-md-2" style="width:65px">
                                    <img src="images/cover-3.jpg" class="img img-circle" style="height:35px"/>
                                </div>
                                <div class="col-md-8" style="margin-left: -20px;">
                                    <i><b>John Doe</b></i>  <font style="color:#999">said<br/>15 mins ago</font>
                                </div>
                            </div>
                            <div class="col-md-12" style="padding:0px">
                                <div class="post_content" style="padding:5px">
                                    Visit with sis and mon and dad..
                                </div>
                                <br/>
                                <div class="img-list">
                                    <img class="img post_photo_img img_options post_video_size" src="images/cover-3.jpg" />
                                    <span class="text-content">
                                        <span>
                                            <i class="fa fa-arrows-alt  img_options" src="images/cover-3.jpg" style="padding:10px"></i>
                                            <i class="fa fa-edit  img_options" src="images/cover-3.jpg"></i>
                                            <i class="fa fa-star  later" style="margin-top:-10px;margin-left: 18%;position: absolute;margin-top: -10px;">&nbsp;88</i>
                                            <i class="fa fa-thumb-tack  later" src="images/cover-1.jpg" style="padding: 8%;margin-left: 11%;position: absolute;">&nbsp;77</i>
                                            <i class="fa fa-arrow-down  later" src="images/cover-1.jpg" style="margin-top:10px;padding: 15%;margin-left: 3%;position: absolute;margin-top: 10px;">&nbsp;137</i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-6 p_padding_left">
                                <div class="photo_content">
                                    <span class="p_bold">Wisterio</span><span class="p_bold"></span><br />@username
                                </div>
                            </div>
                            <div class="col-md-6 p_padding_right">
                                <span class="smiley">&#9786;</span>10 
                                <span class="smiley2">&#9785;</span>0 
                                <span class="smiley2">&#9825;</span>18
                            </div>
                            <div class="col-md-12 photo_comments">
                                <div class="col-md-2 comments_col_width">
                                    <img src="images/cover-3.jpg" class="img img-circle" style="height:35px"/>
                                </div>
                                <div class="col-md-9" style="margin-left: -20px;">
                                    <div class="pull-right"><i class="fa fa-comment"></i> 3</div>
                                    <div class="post_user_detail">
                                        <span class="p_bold">Ivan Moore</span><br/>
                                        <span class="photo_content">Hey Aunt @Wis, I wish I could have been there</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" data-type="text">
                        <div class="row item_content">
                            <div class="col-md-2" style="width:65px">
                                <img src="images/cover-3.jpg" class="img img-circle" style="height:35px"/>
                            </div>
                            <div class="col-md-8" style="margin-left: -20px;">
                                <div class="post_user_detail">
                                    <span class="p_bold">John Doe</span>  said<br/><span class="p_bold">15 mins ago</span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="post_content">
                                    Closed That Deal I've been waiting on..
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" data-type="media">
                        <div class="row item_content">
                            <div style="padding:5px;">
                                <div class="col-md-2" style="width:65px">
                                    <img src="images/cover-4.jpg" class="img img-circle" style="height:35px"/>
                                </div>
                                <div class="col-md-8" style="margin-left: -20px;">
                                    <i><b>John Doe</b></i>  <font style="color:#999">said<br/>15 mins ago</font>
                                </div>
                            </div>
                            <div class="col-md-12" style="padding:0px">
                                <div class="post_content" style="padding:5px">
                                    Visit with sis and mon and dad..
                                </div>
                                <br/>
                                <div class="img-list">
                                    <img class="img post_photo_img img_options post_video_size" src="images/cover-4.jpg"  style="height:131px" />
                                    <span class="text-content">
                                        <span>
                                            <i class="fa fa-arrows-alt  img_options" src="images/cover-4.jpg" style="padding:10px"></i>
                                            <i class="fa fa-edit  img_options" src="images/cover-4.jpg"></i>
                                            <i class="fa fa-star  later" style="margin-top:-10px;margin-left: 18%;position: absolute;margin-top: -10px;">&nbsp;88</i>
                                            <i class="fa fa-thumb-tack  later" src="images/cover-1.jpg" style="padding: 8%;margin-left: 11%;position: absolute;">&nbsp;77</i>
                                            <i class="fa fa-arrow-down  later" src="images/cover-1.jpg" style="margin-top:10px;padding: 15%;margin-left: 3%;position: absolute;margin-top: 10px;">&nbsp;137</i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-6 p_padding_left">
                                <div class="photo_content">
                                    <span class="p_bold">Wisterio</span><span class="p_bold"></span><br />@username
                                </div>
                            </div>
                            <div class="col-md-6 p_padding_right">
                                <span class="smiley">&#9786;</span>10 
                                <span class="smiley2">&#9785;</span>0 
                                <span class="smiley2">&#9825;</span>18
                            </div>
                            <div class="col-md-12 photo_comments">
                                <div class="col-md-2 comments_col_width">
                                    <img src="images/cover-4.jpg" class="img img-circle" style="height:35px"/>
                                </div>
                                <div class="col-md-9" style="margin-left: -20px;">
                                    <div class="pull-right"><i class="fa fa-comment"></i> 3</div>
                                    <div class="post_user_detail">
                                        <span class="p_bold">Brenda Smith</span><br/>
                                        <span class="photo_content">Hey Aunt @Wis, I wish I could have been there</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item" data-type="media">
                        <div class="row item_content">
                            <div style="padding:5px">
                                <div class="col-md-2" style="width:65px">
                                    <img src="images/cover-3.jpg" class="img img-circle" style="height:35px"/>
                                </div>
                                <div class="col-md-8" style="margin-left: -20px;">
                                    <i><b>John Doe</b></i>  <font style="color:#999">said<br/>15 mins ago</font>
                                </div>
                            </div>
                            <div class="col-md-12" style="padding:0px">
                                <div class="post_content" style="padding:5px">
                                    Visit with sis and mon and dad..
                                </div>
                                <br/>
                                <div class="img-list">
                                    <img class="video_options post_video_size" data-video="XZ4X1wcZ1GE" src="images/XZ4X1wcZ1GE-play.jpg" />
                                    <span class="text-content">
                                        <span>
                                            <i class="fa fa-arrows-alt  video_options"  data-video="XZ4X1wcZ1GE" src="images/XZ4X1wcZ1GE-play.jpg" style="padding:10px"></i>
                                            <i class="fa fa-edit video_options" src="images/cover-1.jpg"></i>
                                            <i class="fa fa-star  later" style="margin-top:-10px;margin-left: 18%;position: absolute;margin-top: -10px;">&nbsp;88</i>
                                            <i class="fa fa-thumb-tack  later" src="images/cover-1.jpg" style="padding: 8%;margin-left: 11%;position: absolute;">&nbsp;77</i>
                                            <i class="fa fa-arrow-down  later" src="images/cover-1.jpg" style="margin-top:10px;padding: 15%;margin-left: 3%;position: absolute;margin-top: 10px;">&nbsp;137</i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-6 p_padding_left">
                                <div class="photo_content">
                                    <span class="p_bold">Wisterio</span><span class="p_bold"></span><br />@username
                                </div>
                            </div>
                            <div class="col-md-6 p_padding_right">
                                <span class="smiley">&#9786;</span>10 
                                <span class="smiley2">&#9785;</span>0 
                                <span class="smiley2">&#9825;</span>18
                            </div>
                            <div class="col-md-12 photo_comments">
                                <div class="col-md-2 comments_col_width">
                                    <img src="images/cover-3.jpg" class="img img-circle" style="height:35px"/>
                                </div>
                                <div class="col-md-9" style="margin-left: -20px;">
                                    <div class="pull-right"><i class="fa fa-comment"></i> 3</div>
                                    <div class="post_user_detail">
                                        <span class="p_bold">Ivan Moore</span><br/>
                                        <span class="photo_content">Hey Aunt @Wis, I wish I could have been there</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bhoechie-tab-content">
                <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                    <div class="default_content"></div>
                </div>
            </div>
            <div class="bhoechie-tab-content">
                <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                    <div class="default_content"></div>
                </div>
            </div>
            <div class="bhoechie-tab-content">
                <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                    <div class="default_content"></div>
                </div>
            </div>
            <div class="bhoechie-tab-content">
                <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                    <div class="default_content"></div>
                </div>
            </div>
            <div class="bhoechie-tab-content">
                <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                    <div class="default_content"></div>
                </div>
            </div>
            <div class="bhoechie-tab-content">
                <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                    <div class="default_content"></div>
                </div>
            </div>
            <div class="bhoechie-tab-content">
                <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                    <div class="default_content"></div>
                </div>
            </div>
            <div class="bhoechie-tab-content">
                <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                    <div class="default_content"></div>
                </div>
            </div>
            <div class="bhoechie-tab-content">
                <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                    <div class="default_content"></div>
                </div>
            </div>
            <div class="bhoechie-tab-content">
                <div class="col-md-12" style="margin-bottom: 10px;">                
                    <div class="col-md-1"></div>
                    <div class="col-md-10" style="background-color:#000;padding: 0px">
                        <div class="col-md-12" style="padding:0px">
                            <div class="row item_content" style="background-color: #000;">
                                <div class="col-md-5" style="padding:0px">
                                    <img class="img_options"  src="images/cover-1.jpg" style="margin-top: -5px;height: 200px;" />
                                    <h2 class="post_title_cl" style="color:#9CD759;margin-top: 30px;">
                                        <i class="fa fa-circle"></i> Online
                                    </h2>
                                </div>
                                <div class="col-md-7" style="padding:5px">
                                    <div class="post_user_detail">
                                        <h4 style="text-align: center" class="colors-color">Name</h4>
                                        <h4 style="text-align: center" class="colors-color">@username</h4>
                                        <p style="color:#fff;text-align: center">
                                            <i>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</i>
                                        </p>

                                        <div class="col-md-12">
                                            <h5 class="post_title_cl colors-color">
                                                About Me
                                            </h5>
                                        </div>
                                        <div class="col-md-12" style="margin-top: 10px;margin-bottom: 10px">
                                            <div class="col-md-6" style="text-align: center;">
                                                <div class="colors-color">Work:</div>
                                                <div class="colors-color">Live:</div>
                                                <div class="colors-color">Hometown:</div>
                                                <div class="colors-color">Birthday:</div>
                                            </div>
                                            <div class="col-md-6" style="text-align: center;color:#fff;">
                                                <div class="">XYZ Inc.</div>
                                                <div class="">Anytown, USA</div>
                                                <div class="">Foreign, EU</div>
                                                <div class="">July 8th</div>
                                            </div>
                                            <div class="col-md-4" style="text-align: center;color:#fff;margin-top: 10px;">
                                                <div class="colors-color-hover">
                                                    <a href="interests.php">
                                                        <i class="fa fa-user"></i><br/>
                                                        Interests
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-4" style="text-align: center;color:#fff;margin-top: 10px;">
                                                <div class="colors-color-hover">
                                                    <a href="resume.php">
                                                        <i class="fa fa-list"></i><br/>
                                                        Resume
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-4" style="text-align: center;color:#fff;margin-top: 10px;">
                                                <div class="colors-color-hover">
                                                    <a href="contact.php">
                                                        <i class="fa fa-paper-plane"></i><br/>
                                                        Contact
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-12" style="text-align: center;color:#fff;margin-top: 10px;">
                                                <button class="btn btn-default btn-xs">More</button>
                                            </div>
                                        </div>
                                        <div class="col-md-12 colors-color" style="padding: 0px;margin-left: -8px;">
                                            <div class="col-md-2">
                                                <i class="fa fa-inbox fa fa-2x"></i>
                                            </div>
                                            <div class="col-md-2">
                                                <i class="fa fa-cog  fa fa-2x"></i>
                                            </div>
                                            <div class="col-md-2">
                                                <i class="fa fa-plus  fa fa-2x"></i>
                                            </div>
                                            <div class="col-md-2">
                                                <i class="fa fa-eye fa fa-2x"></i>
                                            </div>
                                            <div class="col-md-2">
                                                <i class="fa fa-video-camera fa fa-2x"></i>
                                            </div>
                                            <div class="col-md-2">
                                                <i class="fa fa-gift fa fa-2x"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <div class="clearfix"></div>
                <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                    <?php for ($i = 1; $i <= 12; $i++) { ?>
                        <div class="item" data-type="member">
                            <div class="row item_content">
                                <div class="col-md-12" style="padding:0px;margin-top: -5px">
                                    <img class="img_options post_video_size" data-video="XZ4X1wcZ1GE" src="images/cover-1.jpg" />
                                </div>
                                <div class="col-md-12 photo_comments">
                                    <div class="col-md-2 comments_col_width">
                                        <img src="images/cover-3.jpg" class="img img-circle" style="height:35px;margin-top: -30px;"/>
                                    </div>
                                    <div class="col-md-9 members_details">
                                        <div class="post_user_detail">
                                            <span class="p_bold">Ivan Moore</span>
                                            <span class="pull-right" style="color:#9CD759">
                                                <i class="fa fa-circle"></i> Online
                                            </span>
                                            <br/>
                                            <span class="p_bold">@username</span><br/>
                                            <span class="photo_content">loream ipsum....</span><br />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12" style="color:#FF7101;padding: 0px;margin-left: -8px;">
                                    <div class="col-md-2">
                                        <i class="fa fa-inbox fa fa-2x"></i>
                                    </div>
                                    <div class="col-md-2">
                                        <i class="fa fa-cog  fa fa-2x"></i>
                                    </div>
                                    <div class="col-md-2">
                                        <i class="fa fa-plus  fa fa-2x"></i>
                                    </div>
                                    <div class="col-md-2">
                                        <i class="fa fa-eye fa fa-2x"></i>
                                    </div>
                                    <div class="col-md-2">
                                        <i class="fa fa-video-camera fa fa-2x"></i>
                                    </div>
                                    <div class="col-md-2">
                                        <i class="fa fa-gift fa fa-2x"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="bhoechie-tab-content">
                <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                    <div class="default_content"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="main-pagination">
            <div class="masonry_load_more colors-color">
                <h3 id="load_more_text">Load More</h3>
            </div>
        </div>
    </div>
</div>
<?php include_once __DIR__ . '/footer.php'; ?>