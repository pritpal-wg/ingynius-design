<?php

$title = "Home 2";
?>
<?php include_once __DIR__ . '/header.php' ?>
<style>
    #main-content {
        padding: 0;
        margin: 10px;
    }
</style>
<div id="content-wrapper">
    <div id="main-content">
        <div id="post_area" class="row" style="margin-bottom: 10px;padding:10px">
            <div class="col-md-3"></div>
            <div class="col-md-1" id="post_text">
                <i class="fa fa-font"></i><br/>
                <span>Text</span>
            </div>
            <div class="col-md-1" id="post_photo">
                <i class="fa fa-camera" style="color:#DC6E53"></i><br/>
                <span>Photo</span>
            </div>
            <div class="col-md-1" id="post_link">
                <i class="fa fa-link" style="color:#67C295"></i><br/>
                <span>Link</span>
            </div>
            <div class="col-md-1" id="post_chat">
                <i class="fa fa-comment" style="color:#63A7D1"></i><br/>
                <span>Chat</span>
            </div>
            <div class="col-md-1" id="post_audio">
                <i class="fa fa-volume-up" style="color:#B08AC8"></i><br/>
                <span>Audio</span>
            </div>
            <div class="col-md-1" id="post_video">
                <i class="fa fa-film" style="color:#828D95"></i><br/>
                <span>Video</span>
            </div>
            <div class="col-md-3"></div>
        </div>

        <section class="blog-content-grid">

            <div class="row">

                <!-- post -->
                <div class="col-md-3 col-sm-6">

                    <article>

                        <div class="post-thumb">
                            <a href="blog_single.php" class="image-link">
                                <img src="http://placehold.it/333x222">  
                            </a>					
                        </div>

                        <div class="post-body">

                            <h3 class="post-title"><a href="blog_single.php">Awesome Post Title</a></h3>

                            <div class="post-meta">

                                <ul>
                                    <li><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-clock"></i>January 18, 2015</li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-folder"></i><a href="#" rel="category tag">WordPress</a></li>
                                </ul>

                            </div>

                            <div class="post-content">
                                <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat. Mauris sit amet sapien eget lacus...</p>
                            </div>

                            <a href="blog_single.php" class="read_more_but">Continue Reading</a>

                            <div class="footer_post">
                                <ul>
                                    <li><i class="icon-picture"></i></li>
                                    <li><i class="icon-bubble"></i> <a href="#">4</a></li>
                                    <li><i class="icon-heart"></i> <a href="#">35</a></li>
                                    <li><i class="icon-eye"></i> 216</li>
                                </ul>
                            </div>

                        </div>

                    </article>

                </div>
                <!-- //post -->

                <!-- post -->
                <div class="col-md-3 col-sm-6">

                    <article>

                        <div class="post-thumb">
                            <a href="blog_single.php" class="image-link">
                                <img src="http://placehold.it/333x222">  
                            </a>					
                        </div>

                        <div class="post-body">

                            <h3 class="post-title"><a href="blog_single.php">Awesome Post Title</a></h3>

                            <div class="post-meta">

                                <ul>
                                    <li><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-clock"></i>January 18, 2015</li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-folder"></i><a href="#" rel="category tag">WordPress</a></li>
                                </ul>

                            </div>

                            <div class="post-content">
                                <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat. Mauris sit amet sapien eget lacus...</p>
                            </div>

                            <a href="blog_single.php" class="read_more_but">Continue Reading</a>

                            <div class="footer_post">
                                <ul>
                                    <li><i class="icon-music-tone-alt"></i></li>
                                    <li><i class="icon-bubble"></i> <a href="#">4</a></li>
                                    <li><i class="icon-heart"></i> <a href="#">35</a></li>
                                    <li><i class="icon-eye"></i> 216</li>
                                </ul>
                            </div>

                        </div>

                    </article>

                </div>
                <!-- //post -->


                <!-- post -->
                <div class="col-md-3 col-sm-6">

                    <article>

                        <div class="post-thumb">
                            <a href="blog_single.php" class="image-link">
                                <img src="http://placehold.it/333x222">  
                            </a>					
                        </div>

                        <div class="post-body">

                            <h3 class="post-title"><a href="blog_single.php">Awesome Post Title</a></h3>

                            <div class="post-meta">

                                <ul>
                                    <li><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-clock"></i>January 18, 2015</li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-folder"></i><a href="#" rel="category tag">WordPress</a></li>
                                </ul>

                            </div>

                            <div class="post-content">
                                <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat. Mauris sit amet sapien eget lacus...</p>
                            </div>

                            <a href="blog_single.php" class="read_more_but">Continue Reading</a>

                            <div class="footer_post">
                                <ul>
                                    <li><i class="icon-social-youtube"></i></li>
                                    <li><i class="icon-bubble"></i> <a href="#">4</a></li>
                                    <li><i class="icon-heart"></i> <a href="#">35</a></li>
                                    <li><i class="icon-eye"></i> 216</li>
                                </ul>
                            </div>

                        </div>

                    </article>

                </div>
                <!-- //post -->

                <!-- post -->
                <div class="col-md-3 col-sm-6">

                    <article>

                        <div class="post-thumb">
                            <a href="blog_single.php" class="image-link">
                                <img src="http://placehold.it/333x222">  
                            </a>					
                        </div>

                        <div class="post-body">

                            <h3 class="post-title"><a href="blog_single.php">Awesome Post Title</a></h3>

                            <div class="post-meta">

                                <ul>
                                    <li><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-clock"></i>January 18, 2015</li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-folder"></i><a href="#" rel="category tag">WordPress</a></li>
                                </ul>

                            </div>

                            <div class="post-content">
                                <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat. Mauris sit amet sapien eget lacus...</p>
                            </div>

                            <a href="blog_single.php" class="read_more_but">Continue Reading</a>

                            <div class="footer_post">
                                <ul>
                                    <li><i class="icon-picture"></i></li>
                                    <li><i class="icon-bubble"></i> <a href="#">4</a></li>
                                    <li><i class="icon-heart"></i> <a href="#">35</a></li>
                                    <li><i class="icon-eye"></i> 216</li>
                                </ul>
                            </div>

                        </div>

                    </article>

                </div>
                <!-- //post -->

                <!-- post -->
                <div class="col-md-3 col-sm-6">

                    <article>

                        <div class="post-thumb">
                            <a href="blog_single.php" class="image-link">
                                <img src="http://placehold.it/333x222">  
                            </a>					
                        </div>

                        <div class="post-body">

                            <h3 class="post-title"><a href="blog_single.php">Awesome Post Title</a></h3>

                            <div class="post-meta">

                                <ul>
                                    <li><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-clock"></i>January 18, 2015</li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-folder"></i><a href="#" rel="category tag">WordPress</a></li>
                                </ul>

                            </div>

                            <div class="post-content">
                                <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat. Mauris sit amet sapien eget lacus...</p>
                            </div>

                            <a href="blog_single.php" class="read_more_but">Continue Reading</a>

                            <div class="footer_post">
                                <ul>
                                    <li><i class="icon-music-tone-alt"></i></li>
                                    <li><i class="icon-bubble"></i> <a href="#">4</a></li>
                                    <li><i class="icon-heart"></i> <a href="#">35</a></li>
                                    <li><i class="icon-eye"></i> 216</li>
                                </ul>
                            </div>

                        </div>

                    </article>

                </div>
                <!-- //post -->

                <!-- post -->
                <div class="col-md-3 col-sm-6">

                    <article>

                        <div class="post-thumb">
                            <a href="blog_single.php" class="image-link">
                                <img src="http://placehold.it/333x222">  
                            </a>					
                        </div>

                        <div class="post-body">

                            <h3 class="post-title"><a href="blog_single.php">Awesome Post Title</a></h3>

                            <div class="post-meta">

                                <ul>
                                    <li><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-clock"></i>January 18, 2015</li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-folder"></i><a href="#" rel="category tag">WordPress</a></li>
                                </ul>

                            </div>

                            <div class="post-content">
                                <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat. Mauris sit amet sapien eget lacus...</p>
                            </div>

                            <a href="blog_single.php" class="read_more_but">Continue Reading</a>

                            <div class="footer_post">
                                <ul>
                                    <li><i class="icon-picture"></i></li>
                                    <li><i class="icon-bubble"></i> <a href="#">4</a></li>
                                    <li><i class="icon-heart"></i> <a href="#">35</a></li>
                                    <li><i class="icon-eye"></i> 216</li>
                                </ul>
                            </div>

                        </div>

                    </article>

                </div>
                <!-- //post -->

                <!-- post -->
                <div class="col-md-3 col-sm-6">

                    <article>

                        <div class="post-thumb">
                            <a href="blog_single.php" class="image-link">
                                <img src="http://placehold.it/333x222">  
                            </a>					
                        </div>

                        <div class="post-body">

                            <h3 class="post-title"><a href="blog_single.php">Awesome Post Title</a></h3>

                            <div class="post-meta">

                                <ul>
                                    <li><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-clock"></i>January 18, 2015</li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-folder"></i><a href="#" rel="category tag">WordPress</a></li>
                                </ul>

                            </div>

                            <div class="post-content">
                                <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat. Mauris sit amet sapien eget lacus...</p>
                            </div>

                            <a href="blog_single.php" class="read_more_but">Continue Reading</a>

                            <div class="footer_post">
                                <ul>
                                    <li><i class="icon-picture"></i></li>
                                    <li><i class="icon-bubble"></i> <a href="#">4</a></li>
                                    <li><i class="icon-heart"></i> <a href="#">35</a></li>
                                    <li><i class="icon-eye"></i> 216</li>
                                </ul>
                            </div>

                        </div>

                    </article>

                </div>
                <!-- //post -->

                <!-- post -->
                <div class="col-md-3 col-sm-6">

                    <article>

                        <div class="post-thumb">
                            <a href="blog_single.php" class="image-link">
                                <img src="http://placehold.it/333x222">  
                            </a>					
                        </div>

                        <div class="post-body">

                            <h3 class="post-title"><a href="blog_single.php">Awesome Post Title</a></h3>

                            <div class="post-meta">

                                <ul>
                                    <li><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-clock"></i>January 18, 2015</li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-folder"></i><a href="#" rel="category tag">WordPress</a></li>
                                </ul>

                            </div>

                            <div class="post-content">
                                <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat. Mauris sit amet sapien eget lacus...</p>
                            </div>

                            <a href="blog_single.php" class="read_more_but">Continue Reading</a>

                            <div class="footer_post">
                                <ul>
                                    <li><i class="icon-social-youtube"></i></li>
                                    <li><i class="icon-bubble"></i> <a href="#">4</a></li>
                                    <li><i class="icon-heart"></i> <a href="#">35</a></li>
                                    <li><i class="icon-eye"></i> 216</li>
                                </ul>
                            </div>

                        </div>

                    </article>

                </div>
                <!-- //post -->

                <!-- post -->
                <div class="col-md-3 col-sm-6">

                    <article>

                        <div class="post-thumb">
                            <a href="blog_single.php" class="image-link">
                                <img src="http://placehold.it/333x222">  
                            </a>					
                        </div>

                        <div class="post-body">

                            <h3 class="post-title"><a href="blog_single.php">Awesome Post Title</a></h3>

                            <div class="post-meta">

                                <ul>
                                    <li><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-clock"></i>January 18, 2015</li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-folder"></i><a href="#" rel="category tag">WordPress</a></li>
                                </ul>

                            </div>

                            <div class="post-content">
                                <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat. Mauris sit amet sapien eget lacus...</p>
                            </div>

                            <a href="blog_single.php" class="read_more_but">Continue Reading</a>

                            <div class="footer_post">
                                <ul>
                                    <li><i class="icon-music-tone-alt"></i></li>
                                    <li><i class="icon-bubble"></i> <a href="#">4</a></li>
                                    <li><i class="icon-heart"></i> <a href="#">35</a></li>
                                    <li><i class="icon-eye"></i> 216</li>
                                </ul>
                            </div>

                        </div>

                    </article>

                </div>
                <!-- //post -->

                <!-- post -->
                <div class="col-md-3 col-sm-6">

                    <article>

                        <div class="post-thumb">
                            <a href="blog_single.php" class="image-link">
                                <img src="http://placehold.it/333x222">  
                            </a>					
                        </div>

                        <div class="post-body">

                            <h3 class="post-title"><a href="blog_single.php">Awesome Post Title</a></h3>

                            <div class="post-meta">

                                <ul>
                                    <li><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-clock"></i>January 18, 2015</li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-folder"></i><a href="#" rel="category tag">WordPress</a></li>
                                </ul>

                            </div>

                            <div class="post-content">
                                <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat. Mauris sit amet sapien eget lacus...</p>
                            </div>

                            <a href="blog_single.php" class="read_more_but">Continue Reading</a>

                            <div class="footer_post">
                                <ul>
                                    <li><i class="icon-music-tone-alt"></i></li>
                                    <li><i class="icon-bubble"></i> <a href="#">4</a></li>
                                    <li><i class="icon-heart"></i> <a href="#">35</a></li>
                                    <li><i class="icon-eye"></i> 216</li>
                                </ul>
                            </div>

                        </div>

                    </article>

                </div>
                <!-- //post -->

                <!-- post -->
                <div class="col-md-3 col-sm-6">

                    <article>

                        <div class="post-thumb">
                            <a href="blog_single.php" class="image-link">
                                <img src="http://placehold.it/333x222">  
                            </a>					
                        </div>

                        <div class="post-body">

                            <h3 class="post-title"><a href="blog_single.php">Awesome Post Title</a></h3>

                            <div class="post-meta">

                                <ul>
                                    <li><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-clock"></i>January 18, 2015</li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-folder"></i><a href="#" rel="category tag">WordPress</a></li>
                                </ul>

                            </div>

                            <div class="post-content">
                                <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat. Mauris sit amet sapien eget lacus...</p>
                            </div>

                            <a href="blog_single.php" class="read_more_but">Continue Reading</a>

                            <div class="footer_post">
                                <ul>
                                    <li><i class="icon-music-tone-alt"></i></li>
                                    <li><i class="icon-bubble"></i> <a href="#">4</a></li>
                                    <li><i class="icon-heart"></i> <a href="#">35</a></li>
                                    <li><i class="icon-eye"></i> 216</li>
                                </ul>
                            </div>

                        </div>

                    </article>

                </div>
                <!-- //post -->

                <!-- post -->
                <div class="col-md-3 col-sm-6">

                    <article>

                        <div class="post-thumb">
                            <a href="blog_single.php" class="image-link">
                                <img src="http://placehold.it/333x222">  
                            </a>					
                        </div>

                        <div class="post-body">

                            <h3 class="post-title"><a href="blog_single.php">Awesome Post Title</a></h3>

                            <div class="post-meta">

                                <ul>
                                    <li><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-clock"></i>January 18, 2015</li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-folder"></i><a href="#" rel="category tag">WordPress</a></li>
                                </ul>

                            </div>

                            <div class="post-content">
                                <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat. Mauris sit amet sapien eget lacus...</p>
                            </div>

                            <a href="blog_single.php" class="read_more_but">Continue Reading</a>

                            <div class="footer_post">
                                <ul>
                                    <li><i class="icon-picture"></i></li>
                                    <li><i class="icon-bubble"></i> <a href="#">4</a></li>
                                    <li><i class="icon-heart"></i> <a href="#">35</a></li>
                                    <li><i class="icon-eye"></i> 216</li>
                                </ul>
                            </div>

                        </div>

                    </article>

                </div>
                <!-- //post -->

                <!-- post -->
                <div class="col-md-3 col-sm-6">

                    <article>

                        <div class="post-thumb">
                            <a href="blog_single.php" class="image-link">
                                <img src="http://placehold.it/333x222">  
                            </a>					
                        </div>

                        <div class="post-body">

                            <h3 class="post-title"><a href="blog_single.php">Awesome Post Title</a></h3>

                            <div class="post-meta">

                                <ul>
                                    <li><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-clock"></i>January 18, 2015</li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-folder"></i><a href="#" rel="category tag">WordPress</a></li>
                                </ul>

                            </div>

                            <div class="post-content">
                                <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat. Mauris sit amet sapien eget lacus...</p>
                            </div>

                            <a href="blog_single.php" class="read_more_but">Continue Reading</a>

                            <div class="footer_post">
                                <ul>
                                    <li><i class="icon-picture"></i></li>
                                    <li><i class="icon-bubble"></i> <a href="#">4</a></li>
                                    <li><i class="icon-heart"></i> <a href="#">35</a></li>
                                    <li><i class="icon-eye"></i> 216</li>
                                </ul>
                            </div>

                        </div>

                    </article>

                </div>
                <!-- //post -->

                <!-- post -->
                <div class="col-md-3 col-sm-6">

                    <article>

                        <div class="post-thumb">
                            <a href="blog_single.php" class="image-link">
                                <img src="http://placehold.it/333x222">  
                            </a>					
                        </div>

                        <div class="post-body">

                            <h3 class="post-title"><a href="blog_single.php">Awesome Post Title</a></h3>

                            <div class="post-meta">

                                <ul>
                                    <li><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-clock"></i>January 18, 2015</li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-folder"></i><a href="#" rel="category tag">WordPress</a></li>
                                </ul>

                            </div>

                            <div class="post-content">
                                <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat. Mauris sit amet sapien eget lacus...</p>
                            </div>

                            <a href="blog_single.php" class="read_more_but">Continue Reading</a>

                            <div class="footer_post">
                                <ul>
                                    <li><i class="icon-music-tone-alt"></i></li>
                                    <li><i class="icon-bubble"></i> <a href="#">4</a></li>
                                    <li><i class="icon-heart"></i> <a href="#">35</a></li>
                                    <li><i class="icon-eye"></i> 216</li>
                                </ul>
                            </div>

                        </div>

                    </article>

                </div>
                <!-- //post -->

                <!-- post -->
                <div class="col-md-3 col-sm-6">

                    <article>

                        <div class="post-thumb">
                            <a href="blog_single.php" class="image-link">
                                <img src="http://placehold.it/333x222">  
                            </a>					
                        </div>

                        <div class="post-body">

                            <h3 class="post-title"><a href="blog_single.php">Awesome Post Title</a></h3>

                            <div class="post-meta">

                                <ul>
                                    <li><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-clock"></i>January 18, 2015</li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-folder"></i><a href="#" rel="category tag">WordPress</a></li>
                                </ul>

                            </div>

                            <div class="post-content">
                                <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat. Mauris sit amet sapien eget lacus...</p>
                            </div>

                            <a href="blog_single.php" class="read_more_but">Continue Reading</a>

                            <div class="footer_post">
                                <ul>
                                    <li><i class="icon-pencil"></i></li>
                                    <li><i class="icon-bubble"></i> <a href="#">4</a></li>
                                    <li><i class="icon-heart"></i> <a href="#">35</a></li>
                                    <li><i class="icon-eye"></i> 216</li>
                                </ul>
                            </div>

                        </div>

                    </article>

                </div>
                <!-- //post -->

                <!-- post -->
                <div class="col-md-3 col-sm-6">

                    <article>

                        <div class="post-thumb">
                            <a href="blog_single.php" class="image-link">
                                <img src="http://placehold.it/333x222">  
                            </a>					
                        </div>

                        <div class="post-body">

                            <h3 class="post-title"><a href="blog_single.php">Awesome Post Title</a></h3>

                            <div class="post-meta">

                                <ul>
                                    <li><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-clock"></i>January 18, 2015</li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-folder"></i><a href="#" rel="category tag">WordPress</a></li>
                                </ul>

                            </div>

                            <div class="post-content">
                                <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat. Mauris sit amet sapien eget lacus...</p>
                            </div>

                            <a href="blog_single.php" class="read_more_but">Continue Reading</a>

                            <div class="footer_post">
                                <ul>
                                    <li><i class="icon-picture"></i></li>
                                    <li><i class="icon-bubble"></i> <a href="#">4</a></li>
                                    <li><i class="icon-heart"></i> <a href="#">35</a></li>
                                    <li><i class="icon-eye"></i> 216</li>
                                </ul>
                            </div>

                        </div>

                    </article>

                </div>
                <!-- //post -->

                <!-- post -->
                <div class="col-md-3 col-sm-6">

                    <article>

                        <div class="post-thumb">
                            <a href="blog_single.php" class="image-link">
                                <img src="http://placehold.it/333x222">  
                            </a>					
                        </div>

                        <div class="post-body">

                            <h3 class="post-title"><a href="blog_single.php">Awesome Post Title</a></h3>

                            <div class="post-meta">

                                <ul>
                                    <li><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-clock"></i>January 18, 2015</li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-folder"></i><a href="#" rel="category tag">WordPress</a></li>
                                </ul>

                            </div>

                            <div class="post-content">
                                <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat. Mauris sit amet sapien eget lacus...</p>
                            </div>

                            <a href="blog_single.php" class="read_more_but">Continue Reading</a>

                            <div class="footer_post">
                                <ul>
                                    <li><i class="icon-picture"></i></li>
                                    <li><i class="icon-bubble"></i> <a href="#">4</a></li>
                                    <li><i class="icon-heart"></i> <a href="#">35</a></li>
                                    <li><i class="icon-eye"></i> 216</li>
                                </ul>
                            </div>

                        </div>

                    </article>

                </div>
                <!-- //post -->

                <!-- post -->
                <div class="col-md-3 col-sm-6">

                    <article>

                        <div class="post-thumb">
                            <a href="blog_single.php" class="image-link">
                                <img src="http://placehold.it/333x222">  
                            </a>					
                        </div>

                        <div class="post-body">

                            <h3 class="post-title"><a href="blog_single.php">Awesome Post Title</a></h3>

                            <div class="post-meta">

                                <ul>
                                    <li><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-clock"></i>January 18, 2015</li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-folder"></i><a href="#" rel="category tag">WordPress</a></li>
                                </ul>

                            </div>

                            <div class="post-content">
                                <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat. Mauris sit amet sapien eget lacus...</p>
                            </div>

                            <a href="blog_single.php" class="read_more_but">Continue Reading</a>

                            <div class="footer_post">
                                <ul>
                                    <li><i class="icon-pencil"></i></li>
                                    <li><i class="icon-bubble"></i> <a href="#">4</a></li>
                                    <li><i class="icon-heart"></i> <a href="#">35</a></li>
                                    <li><i class="icon-eye"></i> 216</li>
                                </ul>
                            </div>

                        </div>

                    </article>

                </div>
                <!-- //post -->

                <!-- post -->
                <div class="col-md-3 col-sm-6">

                    <article>

                        <div class="post-thumb">
                            <a href="blog_single.php" class="image-link">
                                <img src="http://placehold.it/333x222">  
                            </a>					
                        </div>

                        <div class="post-body">

                            <h3 class="post-title"><a href="blog_single.php">Awesome Post Title</a></h3>

                            <div class="post-meta">

                                <ul>
                                    <li><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-clock"></i>January 18, 2015</li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-folder"></i><a href="#" rel="category tag">WordPress</a></li>
                                </ul>

                            </div>

                            <div class="post-content">
                                <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat. Mauris sit amet sapien eget lacus...</p>
                            </div>

                            <a href="blog_single.php" class="read_more_but">Continue Reading</a>

                            <div class="footer_post">
                                <ul>
                                    <li><i class="icon-social-youtube"></i></li>
                                    <li><i class="icon-bubble"></i> <a href="#">4</a></li>
                                    <li><i class="icon-heart"></i> <a href="#">35</a></li>
                                    <li><i class="icon-eye"></i> 216</li>
                                </ul>
                            </div>

                        </div>

                    </article>

                </div>
                <!-- //post -->

                <!-- post -->
                <div class="col-md-3 col-sm-6">

                    <article>

                        <div class="post-thumb">
                            <a href="blog_single.php" class="image-link">
                                <img src="http://placehold.it/333x222">  
                            </a>					
                        </div>

                        <div class="post-body">

                            <h3 class="post-title"><a href="blog_single.php">Awesome Post Title</a></h3>

                            <div class="post-meta">

                                <ul>
                                    <li><i class="icon-user"></i>by : <a href="#" title="Posts by Youness" rel="author">Youness</a></li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-clock"></i>January 18, 2015</li>
                                    <li class="separate_li">|</li>
                                    <li><i class="icon-folder"></i><a href="#" rel="category tag">WordPress</a></li>
                                </ul>

                            </div>

                            <div class="post-content">
                                <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat. Mauris sit amet sapien eget lacus...</p>
                            </div>

                            <a href="blog_single.php" class="read_more_but">Continue Reading</a>

                            <div class="footer_post">
                                <ul>
                                    <li><i class="icon-picture"></i></li>
                                    <li><i class="icon-bubble"></i> <a href="#">4</a></li>
                                    <li><i class="icon-heart"></i> <a href="#">35</a></li>
                                    <li><i class="icon-eye"></i> 216</li>
                                </ul>
                            </div>

                        </div>

                    </article>

                </div>
                <!-- //post -->

            </div>

        </section>

        <div class="main-pagination">
            <span class="page-numbers current">1</span>
            <a class="page-numbers" href="#">2</a>
            <a class="page-numbers" href="#">3</a>
            <a class="page-numbers" href="#">4</a>
            <a class="next page-numbers" href="#">
                <span class="visuallyhidden">Next</span><i class="fa fa-angle-right"></i>
            </a>	
        </div>
    </div>

</div>
<?php include_once __DIR__ . '/footer.php'; ?>