<?php
$title = "Our Services";
$pic_type = 'square';
$menus = array(
    'index.php' => array(
        'icon' => 'home',
        'label' => 'Home',
    ),
    'index-2.php' => array(
        'icon' => 'home',
        'label' => 'Home 2',
    ),
    'company.php' => array(
        'icon' => 'home',
        'label' => 'Company Home',
    ),
    'company_services.php' => array(
        'icon' => 'cog',
        'label' => 'Services',
    ),
    'company_upgrade.php' => array(
        'icon' => 'wrench',
        'label' => 'Upgrade Account',
    ),
    'company_blog.php' => array(
        'icon' => 'comments',
        'label' => 'Blog',
    ),
    'company_followers.php' => array(
        'icon' => 'users',
        'label' => 'Followers',
    ),
    'company_contact.php' => array(
        'icon' => 'paper-plane',
        'label' => 'Contact',
    ),
);
$show_quick_icons = FALSE;
?>
<?php include_once __DIR__ . '/header.php' ?>
<link href="css/main2.css" rel="stylesheet" type="text/css" />
<style>
    #main-content {
        padding: 0;
        margin: 0;
    }
</style>
<!-- content -->
<div id="content-wrapper">
    <div id="main-content">
        <section id="service" class="layers">
            <h2 style="display:none">service</h2>

            <!-- .page_content -->
            <div class="page_content">

                <!-- .container-fluid -->
                <div class="container-fluid no-marg">

                    <!-- .row -->
                    <div class="row row_responsive">

                        <!-- .section_general -->
                        <div class="col-lg-11 section_general">

                            <header class="section-header">
                                <h3 class="section-title">Upgrade Account</h3>
                                <p>Go Premium - Unlock More Features</p>
                                <div class="border-divider"></div>
                            </header>
                            <div class="row section_separate"> 
                                <div class="col-md-12">

                                    <div role="tabpanel">

                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active"><a href="#desktop" aria-controls="desktop" role="tab" data-toggle="tab"><i class="fa fa-desktop"></i>Import Profile</a></li>
                                            <li role="presentation"><a href="#mobile" aria-controls="mobile" role="tab" data-toggle="tab"><i class="fa fa-globe"></i>About Me</a></li>
                                            <li role="presentation"><a href="#heart" aria-controls="heart" role="tab" data-toggle="tab"><i class="fa fa-heart-o"></i>Education</a></li>
                                            <li role="presentation"><a href="#users" aria-controls="users" role="tab" data-toggle="tab"><i class="fa fa-users"></i>Work History</a></li>
                                            <li role="presentation"><a href="#users" aria-controls="users" role="tab" data-toggle="tab"><i class="fa fa-users"></i>Family & Relationship</a></li>
                                            <li role="presentation"><a href="#users" aria-controls="users" role="tab" data-toggle="tab"><i class="fa fa-users"></i>Interests</a></li>
                                            <li role="presentation"><a href="#users" aria-controls="users" role="tab" data-toggle="tab"><i class="fa fa-users"></i>Invite Friends</a></li>
                                        </ul>

                                        <!-- Tab panes -->
                                        <div class="tab-content">

                                            <div role="tabpanel" class="tab-pane fade active in" id="desktop">
                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. Lorem ipsum dolor color sit amet.</p>

                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet est non massa aliquet dictum eget eu sem. Cras ornare quam et lorem luctus, et dapibus lectus porttitor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse potenti. Mauris luctus imperdiet purus vel bibendum. Phasellus ultrices hendrerit mauris nec gravida.</p>
                                            </div>

                                            <div role="tabpanel" class="tab-pane fade" id="mobile">
                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. Lorem ipsum dolor color sit amet.</p>
                                            </div>

                                            <div role="tabpanel" class="tab-pane fade" id="heart">
                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. Lorem ipsum dolor color sit amet.</p>

                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet est non massa aliquet dictum eget eu sem. Cras ornare quam et lorem luctus, et dapibus lectus porttitor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse potenti. Mauris luctus imperdiet purus vel bibendum. Phasellus ultrices hendrerit mauris nec gravida.</p>
                                            </div>

                                            <div role="tabpanel" class="tab-pane fade" id="users">
                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. Lorem ipsum dolor color sit amet.</p>
                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>
                            <!-- End service-5 -->

                            <!-- pricing table -->
                            <div class="row pricing-zoom section_separate"> 

                                <div class="title-section">
                                    <h2 class="section_title" style="text-align: center">Our Pricing</h2>
                                    <div class="sep2"></div>
                                </div>

                                <div class="col-md-4 col-sm-6">
                                    <div class="pricing">
                                        <div class="pricing-head">
                                            <h3>Standard <span>Ideal for Personal Websites </span></h3>
                                            <h4><i>$</i>19<i>.99</i> <span>Per Month</span></h4>
                                        </div>
                                        <ul class="pricing-content list-unstyled">
                                            <li><i class="fa fa-gift"></i> Free customisation </li>
                                            <li><i class="fa fa-inbox"></i> 24 hour support</li>
                                            <li><i class="fa fa-globe"></i> 10 GB Disckspace</li>
                                            <li><i class="fa fa-cloud-upload"></i> Cloud Storage</li>
                                            <li><i class="fa fa-umbrella"></i> Online Protection</li>
                                        </ul>
                                        <div class="pricing-footer">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cloud Storage magna psum olor .</p>
                                            <a class="btn-u" href="#"><i class="fa fa-shopping-cart"></i> Purchase Now</a>
                                        </div>                    
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-6">
                                    <div class="pricing active">
                                        <div class="pricing-head">
                                            <h3>Premium <span>Ideal for Personal Websites</span></h3>
                                            <h4><i>$</i>29<i>.99</i> <span>Per Month</span></h4>
                                        </div>
                                        <ul class="pricing-content list-unstyled">
                                            <li><i class="fa fa-gift"></i> Free customisation </li>
                                            <li><i class="fa fa-inbox"></i> 24 hour support</li>
                                            <li><i class="fa fa-globe"></i> 10 GB Disckspace</li>
                                            <li><i class="fa fa-cloud-upload"></i> Cloud Storage</li>
                                            <li><i class="fa fa-umbrella"></i> Online Protection</li>
                                        </ul>
                                        <div class="pricing-footer">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cloud Storage magna psum olor .</p>
                                            <a class="btn-u" href="#"><i class="fa fa-shopping-cart"></i> Purchase Now</a>
                                        </div>                    
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-6">
                                    <div class="pricing">
                                        <div class="pricing-head">
                                            <h3>Professional <span>Ideal for Personal Websites </span></h3>
                                            <h4><i>$</i>39<i>.99</i><span>Per Month</span></h4>
                                        </div>
                                        <ul class="pricing-content list-unstyled">
                                            <li><i class="fa fa-gift"></i> Free customisation </li>
                                            <li><i class="fa fa-inbox"></i> 24 hour support</li>
                                            <li><i class="fa fa-globe"></i> 10 GB Disckspace</li>
                                            <li><i class="fa fa-cloud-upload"></i> Cloud Storage</li>
                                            <li><i class="fa fa-umbrella"></i> Online Protection</li>
                                        </ul>
                                        <div class="pricing-footer">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cloud Storage magna psum olor .</p>
                                            <a class="btn-u" href="#"><i class="fa fa-shopping-cart"></i> Purchase Now</a>
                                        </div>                    
                                    </div>
                                </div>


                            </div>

                            <!-- End pricing table -->

                            <div class="col-md-12" style="margin-bottom: 20px;margin-top: -50px;">
                                <div class="col-md-2"></div>
                                <a href="company_premium.php" class="col-md-8 btn btn-success" style="padding: 30px">
                                    Upgrade Account
                                </a>
                                <div class="col-md-2"></div>
                            </div>
                        </div>
                        <!-- End .section_general -->

                    </div>
                    <!-- End .row -->

                </div>
                <!-- End .container-fluid -->

            </div>
            <!-- End .page_content -->

        </section>

    </div>
</div>
<!-- #content-wrapper -->
<?php include_once __DIR__ . '/footer.php'; ?>