<?php
$title = "Company Pages";
$pic_type = 'square';
$menus = array(
    'index.php' => array(
        'icon' => 'home',
        'label' => 'Home',
    ),
    'index-2.php' => array(
        'icon' => 'home',
        'label' => 'Home 2',
    ),
    'company.php' => array(
        'icon' => 'home',
        'label' => 'Company Home',
    ),
    'company_upgrade.php' => array(
        'icon' => 'wrench',
        'label' => 'Upgrade Account',
    ),
    'company_services.php' => array(
        'icon' => 'cog',
        'label' => 'Services',
    ),
    'company_blog.php' => array(
        'icon' => 'comments',
        'label' => 'Blog',
    ),
    'company_followers.php' => array(
        'icon' => 'users',
        'label' => 'Followers',
    ),
    'company_contact.php' => array(
        'icon' => 'paper-plane',
        'label' => 'Contact',
    ),
);
$show_quick_icons = FALSE;
?>
<?php include_once __DIR__ . '/header.php' ?>
<script>
    function getPage(page_id) {
        page_id = Number(page_id);
        var ret = false;
        var pages = $.parseJSON(localStorage.pages);
        $.each(pages, function (k, page) {
            if (page.id === page_id) {
                ret = page;
            }
        });
        return ret;
    }
</script>
<style>
    #main-content {
        padding: 0;
        margin: 0;
    }
    li.timeline-inverted div.timeline-badge {
        top: -100px;
    }
    li.timeline-inverted div.timeline-panel {
        top: -118px;
    }
    li:not(:first-child) div.timeline-badge {
        top: -100px;
    }
    li:not(:first-child) div.timeline-panel {
        top: -118px;
    }
</style>
<div class="col-md-1 bhoechie-tab-menu" style="width:6%;background-image: url('images/sub_nav_menu_bg.png');">
    <div class="list-group">
        <a href="#" class="list-group-item active text-center">
            <h4 class="fa fa-2x fa-cog"></h4><br/>General
        </a>
        <a href="#" class="list-group-item text-center">
            <h4 class="fa fa-2x fa-envelope"></h4><br/>Messaging
        </a>
        <a href="#" class="list-group-item text-center">
            <h4 class="fa fa-2x fa-info-circle"></h4><br/>Page Info
        </a>
        <a href="#" class="list-group-item text-center">
            <h4 class="fa fa-2x fa-globe"></h4><br/>Notifications
        </a>
        <a href="#" class="list-group-item text-center">
            <h4 class="fa fa-2x fa-user"></h4><br/>Page Roles
        </a>
        <a href="#" class="list-group-item text-center">
            <h4 class="fa fa-2x fa-star"></h4><br/>Featured 
        </a>
        <a href="#" class="list-group-item text-center">
            <h4 class="fa fa-2x fa-list-ul"></h4><br/>Activity Log
        </a>
    </div>
</div>
<div id="all_posts_div" class="col-md-7" style="<?php echo $f_name == 'index.php' ? '' : 'background-color: #fff;'; ?>padding: 5px 5px;width:50%">
    <div class="row-fluid">
        <div class="col-md-12" style="margin-top: 1px">
            <div class="fb-profile">
                <img align="left" class="fb-image-lg" src="http://lorempixel.com/850/280/nightlife/5/" alt="Profile image example"/>
                <span id="page_photo"></span>
                <div class="fb-profile-text">
                    <h1><span class="page_name"></span></h1>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <hr/>
        </div>
        <div class="col-md-6">
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a style="color:#000">Latest Posts</a></li>
                    <li><a href="company_pages.php" style="color:#3b5998">My Pages</a></li>
                    <li><a href="create_page.php?p_id=<?php echo $_GET['p_id']; ?>" style="color:#3b5998">Edit Page</a></li>
                </ul>
            </div>
            <div class="img img-thumbnail">
                <img align="left" class="fb-image-lg" src="http://lorempixel.com/850/280/nightlife/5/" alt="Profile image example"/>
            </div>
        </div>
        <div class="col-md-6">
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li>
                        <form action="#" method="post" role="form" enctype="multipart/form-data" class="facebook-share-box">
                            <ul class="post-types">
                                <li class="post-type">
                                    <a class="status" title="" href="#"><i class="icon icon-file"></i> Share Post</a>
                                </li>
                                <li class="post-type">
                                    <a class="photos" href="#"><i class="icon icon-camera"></i> Add Photos</a>
                                </li>
                            </ul>
                            <div class="share">
                                <div class="arrow"></div>
                                <div class="panel panel-danger">
                                    <div class="panel-body">
                                        <div class="">
                                            <textarea name="message" cols="40" rows="10" id="status_message" class="form-control message" style="height: 62px; overflow: hidden;" placeholder="What's on your mind ?"></textarea>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <input type="submit" name="submit" value="Post" class="btn btn-primary">                              
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-12">
            <hr/>
            <ul class="timeline">
                <li>
                    <div class="timeline-badge"><i class="glyphicon glyphicon-check"></i></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="timeline-title">Mussum ipsum cacilds</h4>
                            <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> 11 hours ago via Twitter</small></p>
                        </div>
                        <div class="timeline-body">
                            <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
                        </div>
                    </div>
                </li>
                <li class="timeline-inverted">
                    <div class="timeline-badge warning"><i class="glyphicon glyphicon-credit-card"></i></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="timeline-title">Mussum ipsum cacilds</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="timeline-badge danger"><i class="glyphicon glyphicon-credit-card"></i></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="timeline-title">Mussum ipsum cacilds</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
                        </div>
                    </div>
                </li>
                <li class="timeline-inverted">
                    <div class="timeline-badge warning"><i class="glyphicon glyphicon-phone"></i></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="timeline-title">Mussum ipsum cacilds</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="timeline-badge info"><i class="glyphicon glyphicon-floppy-disk"></i></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="timeline-title">Mussum ipsum cacilds</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
                            <hr>
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                                    <i class="glyphicon glyphicon-cog"></i> <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="timeline-inverted">
                    <div class="timeline-badge success"><i class="glyphicon glyphicon-thumbs-up"></i></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="timeline-title">Mussum ipsum cacilds</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.status').click(function () {
            $('.arrow').css("left", 0);
        });
        $('.photos').click(function () {
            $('.arrow').css("left", 146);
        });
        var key = "<?php echo $_GET['p_id']; ?>";
        var page = getPage(key);
        $('#page_name').val(page.name);
        $('.page_name').html(page.name);
        $('#page_info').val(page.description);
        var photo_path = '<img class="fb-image-profile thumbnail" align="left" src="uploads/' + page.photo + '"/>';
        $('#page_photo').html(photo_path);
    });
</script>
<!-- #content-wrapper -->
<?php include_once __DIR__ . '/footer.php'; ?>