<?php
$title = "Resume";
?>
<?php include_once __DIR__ . '/header.php' ?>
<div class="col-md-1 bhoechie-tab-menu" style="width:6%;background-image: url('images/sub_nav_menu_bg.png');">
    <div class="list-group">
        <a href="#" class="list-group-item active text-center" data-post_type="a-z">
            <h4 class="fa fa-2x fa-clock-o"></h4><br/>A-Z
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="a-z">
            <h4 class="fa fa-2x fa-user"></h4><br/>Only Mine
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="all posts">
            <h4 class="fa fa-2x fa-square"></h4><br/>Show All
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="media">
            <h4 class="fa fa-2x fa-music"></h4><br/>Media
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="collection">
            <h4 class="fa fa-2x fa-columns"></h4><br/><p style="margin-left: -10px;">Collections</p>
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="events">
            <h4 class="fa fa-2x fa-calendar"></h4><br/>Events 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="blogs">
            <h4 class="fa fa-2x fa-edit"></h4><br/>Blogs
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="Groups">
            <h4 class="fa fa-2x fa-users"></h4><br/>Groups 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="Pages">
            <h4 class="fa fa-2x fa-paste"></h4><br/>Pages 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="favorites">
            <h4 class="fa fa-2x fa-star"></h4><br/>Favorites 
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="member">
            <h4 class="fa fa-2x fa-users"></h4><br/>Members
        </a>
        <a href="#" class="list-group-item text-center" data-post_type="settings">
            <h4 class="fa fa-2x fa-cog"></h4><br/>Settings 
        </a>
    </div>
</div>
<div id="all_posts_div" class="col-md-7" style="<?php echo $f_name == 'index.php' ? '' : 'background-color: #fff;'; ?>padding: 5px 5px;width:50%">
    <div class="row-fluid" style="min-height: 600px">
        <div id="resume" class="container-fluid no-marg">

            <!-- .row -->
            <div class="row row_responsive">

                <!-- .section_general -->
                <div class="col-lg-11 section_general">

                    <header class="section-header">
                        <h3 class="section-title">Resume – Personal Info</h3>
                        <p>Awesome &amp; Creative Resume For You</p>
                        <div class="border-divider"></div>
                    </header>

                    <div class="row section_separate">

                        <!-- .resume-left -->
                        <div class="col-md-6 resume-left">    

                            <div class="title-section">
                                <h2 class="section_title">Experience</h2>
                                <div class="sep2"></div>
                            </div>

                            <!-- .attributes -->
                            <ul class="attributes">
                                <li class="first">
                                    <h5>Web Developer <span class="duration"><i class="fa fa-calendar color"></i> 2011 - 2013</span></h5>
                                    <h6><span class="fa fa-briefcase"></span> Name of Company</h6>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing Vivamus sit amet ligula non lectus cursus egestas. Cras erat lorem, fringilla quis sagittis in, sagittis inNam leo tortor Nam leo tortor Vivamus.</p>
                                </li>
                                <li>
                                    <h5>Front-End Developer <span class="duration"><i class="fa fa-calendar color"></i> 2010 - 2011</span></h5>
                                    <h6><span class="fa fa-briefcase"></span> Name of Company</h6>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing Vivamus sit amet ligula non lectus cursus egestas. Cras erat lorem, fringilla quis sagittis in, sagittis inNam leo tortor Nam leo tortor Vivamus.</p>
                                </li>

                            </ul>
                            <!-- /.attributes -->

                            <div class="title-section">
                                <h2 class="section_title">Education</h2>
                                <div class="sep2"></div>
                            </div>

                            <!-- .attributes -->
                            <ul class="attributes">
                                <li class="first">
                                    <h5>Master of Engineering <span class="duration"><i class="fa fa-calendar color"></i> 2011 - 2013</span></h5>
                                    <h6><span class="fa fa-book"></span> Name of University</h6>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing Vivamus sit amet ligula non lectus cursus egestas. Cras erat lorem, fringilla quis sagittis in, sagittis inNam leo tortor Nam leo tortor Vivamus.</p>
                                </li>
                                <li>
                                    <h5>Bachelor of Engineering <span class="duration"><i class="fa fa-calendar color"></i> 2010 - 2011</span></h5>
                                    <h6><span class="fa fa-book"></span> Name of University</h6>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing Vivamus sit amet ligula non lectus cursus egestas. Cras erat lorem, fringilla quis sagittis in, sagittis inNam leo tortor Nam leo tortor Vivamus.</p>
                                </li>
                            </ul>
                            <!-- /.attributes -->

                            <div class="title-section">
                                <h2 class="section_title">Awards</h2>
                                <div class="sep2"></div>
                            </div>

                            <!-- .attributes -->
                            <ul class="attributes">
                                <li class="first">
                                    <h5>Graphic &amp; Art Direction <span class="duration"><i class="fa fa-calendar color"></i> 2013 - 2014</span></h5>
                                    <h6><span class="fa fa-trophy"></span> Name of Institute</h6>
                                    <p>Emi Phasellus congue auctor risuspon, eget males. Pellentes que un imperdiet, odio quis orn sollicitud. Sed vitae lectus elementum mauris.</p>
                                </li>
                                <li>
                                    <h5>Design &amp; Art Direction <span class="duration"><i class="fa fa-calendar color"></i> 2012 - 2013</span></h5>
                                    <h6><span class="fa fa-trophy"></span> Name of Institute</h6>
                                    <p>Emi Phasellus congue auctor risuspon, eget males. Pellentes que un imperdiet, odio quis orn sollicitud. Sed vitae lectus elementum mauris.</p>
                                </li>

                            </ul>
                            <!-- /.attributes -->

                            <!-- .title_content -->
                            <div class="title-section">
                                <h2 class="section_title">Download my resume</h2>
                                <div class="sep2"></div>
                            </div>
                            <!-- /.title_content -->

                            <!-- .download_resume -->
                            <a class="download" href="#">
                                <span data-hover="Download My Resume"><i class="fa fa-cloud-download"></i> Download My Resume</span>
                            </a>
                            <!-- /.download_resume -->

                        </div>
                        <!-- /.resume-left -->

                        <!-- .resume-right -->
                        <div class="col-md-6">

                            <div class="title-section">
                                <h2 class="section_title">Designs skills</h2>
                                <div class="sep2"></div>
                            </div>

                            <div class="skills">
                                <!-- .skillbar -->
                                <div class="skillbar clearfix" data-percent="95%">
                                    <div class="skillbar-title"><span>Photoshop</span></div>
                                    <div class="skillbar-bar" style="width: 95%;"></div>
                                    <div class="skill-bar-percent">95%</div>
                                </div>

                                <!-- /.skillbar -->

                                <!-- .skillbar -->
                                <div class="skillbar clearfix" data-percent="90%">
                                    <div class="skillbar-title"><span>Illustrateur</span></div>
                                    <div class="skillbar-bar" style="width: 90%;"></div>
                                    <div class="skill-bar-percent">90%</div>
                                </div>
                                <!-- /.skillbar -->

                                <!-- .skillbar -->
                                <div class="skillbar clearfix" data-percent="65%">
                                    <div class="skillbar-title"><span>Indesign</span></div>
                                    <div class="skillbar-bar" style="width: 65%;"></div>
                                    <div class="skill-bar-percent">65%</div>
                                </div>
                                <!-- /.skillbar -->

                                <!-- .skillbar -->
                                <div class="skillbar clearfix" data-percent="35%">
                                    <div class="skillbar-title"><span>Flash</span></div>
                                    <div class="skillbar-bar" style="width: 35%;"></div>
                                    <div class="skill-bar-percent">35%</div>
                                </div>
                                <!-- /.skillbar -->
                            </div>


                            <!-- .title_content -->
                            <div class="title-section">
                                <h2 class="section_title">Programming skills</h2>
                                <div class="sep2"></div>
                            </div>
                            <!-- /.title_content -->

                            <div class="skills">
                                <!-- .skillbar -->
                                <div class="skillbar clearfix" data-percent="85%">
                                    <div class="skillbar-title"><span>Wordpress</span></div>
                                    <div class="skillbar-bar" style="width: 85%;"></div>
                                    <div class="skill-bar-percent">85%</div>
                                </div>
                                <!-- /.skillbar -->

                                <!-- .skillbar -->
                                <div class="skillbar clearfix" data-percent="75%">
                                    <div class="skillbar-title"><span>Joomla</span></div>
                                    <div class="skillbar-bar" style="width: 75%;"></div>
                                    <div class="skill-bar-percent">75%</div>
                                </div>
                                <!-- /.skillbar -->

                                <!-- .skillbar -->
                                <div class="skillbar clearfix" data-percent="60%">
                                    <div class="skillbar-title"><span>Drupal</span></div>
                                    <div class="skillbar-bar" style="width: 60%;"></div>
                                    <div class="skill-bar-percent">60%</div>
                                </div>
                                <!-- /.skillbar -->

                                <!-- .skillbar -->
                                <div class="skillbar clearfix" data-percent="89%">
                                    <div class="skillbar-title"><span>Php</span></div>
                                    <div class="skillbar-bar" style="width: 89%;"></div>
                                    <div class="skill-bar-percent">89%</div>
                                </div>
                                <!-- /.skillbar --> 
                            </div>

                            <!-- .title_content -->
                            <div class="title-section">
                                <h2 class="section_title">Office skills</h2>
                                <div class="sep2"></div>
                            </div>
                            <!-- /.title_content -->

                            <div class="skills">       
                                <!-- .skillbar -->
                                <div class="skillbar clearfix" data-percent="85%">
                                    <div class="skillbar-title"><span>MS Excel</span></div>
                                    <div class="skillbar-bar" style="width: 85%;"></div>
                                    <div class="skill-bar-percent">85%</div>
                                </div>
                                <!-- /.skillbar -->

                                <!-- .skillbar -->
                                <div class="skillbar clearfix " data-percent="95%">
                                    <div class="skillbar-title"><span>MS Word</span></div>
                                    <div class="skillbar-bar" style="width: 95%;"></div>
                                    <div class="skill-bar-percent">95%</div>
                                </div>
                                <!-- /.skillbar -->

                                <!-- .skillbar -->
                                <div class="skillbar clearfix " data-percent="60%">
                                    <div class="skillbar-title"><span>Powerpoint</span></div>
                                    <div class="skillbar-bar" style="width: 60%;"></div>
                                    <div class="skill-bar-percent">60%</div>
                                </div>
                                <!-- /.skillbar -->

                                <!-- .skillbar -->
                                <div class="skillbar clearfix " data-percent="40%">
                                    <div class="skillbar-title"><span>SharePoint</span></div>
                                    <div class="skillbar-bar" style="width: 40%;"></div>
                                    <div class="skill-bar-percent">40%</div>
                                </div>
                                <!-- /.skillbar -->   
                            </div>

                            <!-- .title_content -->
                            <div class="title-section">
                                <h2 class="section_title">Language skills</h2>
                                <div class="sep2"></div>
                            </div>
                            <!-- /.title_content -->

                            <div class="skills">
                                <!-- .skillbar -->
                                <div class="skillbar clearfix" data-percent="90%">
                                    <div class="skillbar-title"><span>English</span></div>
                                    <div class="skillbar-bar" style="width: 90%;"></div>
                                    <div class="skill-bar-percent">90%</div>
                                </div>
                                <!-- /.skillbar -->

                                <!-- .skillbar -->
                                <div class="skillbar clearfix " data-percent="80%">
                                    <div class="skillbar-title"><span>French</span></div>
                                    <div class="skillbar-bar" style="width: 80%;"></div>
                                    <div class="skill-bar-percent">80%</div>
                                </div>
                                <!-- /.skillbar -->

                                <!-- .skillbar -->
                                <div class="skillbar clearfix " data-percent="50%">
                                    <div class="skillbar-title"><span>Spanish</span></div>
                                    <div class="skillbar-bar" style="width: 50%;"></div>
                                    <div class="skill-bar-percent">50%</div>
                                </div>
                                <!-- /.skillbar -->

                                <!-- .skillbar -->
                                <div class="skillbar clearfix " data-percent="60%">
                                    <div class="skillbar-title"><span>Swiss</span></div>
                                    <div class="skillbar-bar" style="width: 60%;"></div>
                                    <div class="skill-bar-percent">60%</div>
                                </div>
                                <!-- /.skillbar -->

                            </div>

                            <!-- .title_content -->
                            <div class="title-section">
                                <h2 class="section_title">Hobbies and interests</h2>
                                <div class="sep2"></div>
                            </div>
                            <!-- /.title_content -->

                            <div class="skills">
                                <!-- .skillbar -->
                                <div class="skillbar clearfix" data-percent="90%">
                                    <div class="skillbar-title"><span>Reading</span></div>
                                    <div class="skillbar-bar" style="width: 90%;"></div>
                                    <div class="skill-bar-percent">95%</div>
                                </div>
                                <!-- /.skillbar -->

                                <!-- .skillbar -->
                                <div class="skillbar clearfix " data-percent="80%">
                                    <div class="skillbar-title"><span>Biking</span></div>
                                    <div class="skillbar-bar" style="width: 80%;"></div>
                                    <div class="skill-bar-percent">85%</div>
                                </div>
                                <!-- /.skillbar -->

                                <!-- .skillbar -->
                                <div class="skillbar clearfix " data-percent="50%">
                                    <div class="skillbar-title"><span>football</span></div>
                                    <div class="skillbar-bar" style="width: 50%;"></div>
                                    <div class="skill-bar-percent">90%</div>
                                </div>
                                <!-- /.skillbar -->

                                <!-- .skillbar -->
                                <div class="skillbar clearfix " data-percent="60%">
                                    <div class="skillbar-title"><span>travel</span></div>
                                    <div class="skillbar-bar" style="width: 60%;"></div>
                                    <div class="skill-bar-percent">80%</div>
                                </div>
                                <!-- /.skillbar -->							

                            </div>

                        </div>
                        <!-- /.resume-right -->

                    </div>	
                    <!-- /.end row -->


                    <div class="row no-marg section_separate" style="margin-bottom: 0;">

                        <div class="title-section">
                            <h2 class="section_title">Client reference</h2>
                            <div class="sep2"></div>
                        </div>

                        <div class="col-md-12" style="padding-left:0;padding-right:0">   

                            <div class="reference clearfix"> 	

                                <ul>
                                    <li class="clearfix">
                                        <img src="http://placehold.it/100x100" class="img_reference" width="100" height="100" alt="">
                                        <p>“Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.”</p>
                                        <span>Shelby Endicott, <strong>Assistant Coordinator</strong></span>
                                    </li>

                                    <li class="clearfix">
                                        <img src="http://placehold.it/100x100" class="img_reference" width="100" height="100" alt="">
                                        <p>“Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.”</p>
                                        <span>Emily Madison, <strong>Secretary</strong></span>
                                    </li>

                                    <li class="clearfix">
                                        <img src="http://placehold.it/100x100" class="img_reference" width="100" height="100" alt="">
                                        <p>“Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.”</p>
                                        <span>Chrissie Walter, <strong>Co-Founder</strong></span>
                                    </li>

                                </ul>
                            </div>
                        </div>


                    </div>
                    <!-- /.end row -->


                </div>
                <!-- End .section_general -->


            </div>
            <!-- End .row -->

        </div>
    </div>
    <!-- #content-wrapper -->
    <?php include_once __DIR__ . '/footer.php'; ?>