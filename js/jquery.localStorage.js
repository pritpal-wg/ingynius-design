(function ($) {
    $.ig_ls = function () {
        this.groups = [];
        this.defaults = {};
        this.enabled = function () {
            if (localStorage !== undefined) {
                if (!localStorage.groups)
                    localStorage.groups = JSON.stringify([]);
                this.groups = JSON.parse(localStorage.groups);
                return true;
            } else {
                console.log("Cannot use Localstorage on this device");
                return false;
            }
        };
        this.init = function (opts) {
            if (!this.enabled())
                return;
            opts = $.extend(this.defaults, opts);
        };
        this.saveGroup = function (group) {
            this.init();
            this.groups.push(group);
            localStorage.groups = JSON.stringify(this.groups);
            console.log(this.groups);
        };
    };
})(jQuery);