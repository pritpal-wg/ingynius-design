<?php $title = "Promo Page" ?>
<?php
// Get Current FileName
$file = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);

if (!isset($show_quick_icons))
    $show_quick_icons = TRUE;
if (!isset($show_top_menu))
    $show_top_menu = TRUE;
if (!isset($menus)) {
    $menus = array(
	'index.php' => array(
	    'icon' => 'home',
	    'label' => 'Home',
	),
	'index-2.php' => array(
	    'icon' => 'home',
	    'label' => 'Home 2',
	),
	'profile_wizard.php' => array(
	    'icon' => 'user',
	    'label' => 'My Profile',
	),
	'resume.php' => array(
	    'icon' => 'tasks',
	    'label' => 'Resume',
	),
	'blog.php' => array(
	    'icon' => 'comments',
	    'label' => 'Blog',
	),
	'interests.php' => array(
	    'icon' => 'user',
	    'label' => 'Interests',
	),
	'groups.php' => array(
	    'icon' => 'users',
	    'label' => 'Groups',
	),
	'events.php' => array(
	    'icon' => 'calendar',
	    'label' => 'Events',
	),
	'friends.php' => array(
	    'icon' => 'users',
	    'label' => 'Friends',
	),
	'following.php' => array(
	    'icon' => 'user',
	    'label' => 'Following',
	),
	'followers.php' => array(
	    'icon' => 'user',
	    'label' => 'Followers',
	),
	'company.php' => array(
	    'icon' => 'building',
	    'label' => 'Company',
	),
    );
}
?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
    <head>
        <title><?php echo isset($title) ? $title . ' - ' : '' ?>Ingynius</title>
        <!-- Basic Page Needs -->
        <meta charset="UTF-8" />
        <meta name="description" content="FlexyCodes - FlexyBlog Personal Blog Template. Creating my personal page!"/>
        <meta name="keywords" content="">
        <meta name="author" content="flexycodes">

        <!-- Mobile Specific Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- CSS | bootstrap -->
        <!-- Credits: http://getbootstrap.com/ -->
        <link  rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
        <link  rel="stylesheet" type="text/css" href="css/toastr.css" />

        <!-- CSS | font-awesome -->
        <!-- Credits: http://graphicburger.com/down/?q=simple-line-icons-webfont -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />

        <!-- CSS | simple-line-icons -->
        <!-- Credits: https://github.com/thesabbir/simple-line-icons -->
        <link rel="stylesheet" type="text/css" href="css/simple-line-icons.min.css">

        <!-- CSS | animate -->
        <!-- Credits: http://daneden.me/animate/ -->
        <link rel="stylesheet" type="text/css" href="css/animate.min.css" />

        <!-- CSS | flexslider -->
        <!-- Credits: http://www.woothemes.com/flexslider/ -->
        <link rel="stylesheet" type="text/css" href="css/flexslider.min.css" />

        <!-- CSS | prettyPhoto -->
        <!-- Credits: http://www.no-margin-for-errors.com/ -->
        <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css"/>

        <!-- CSS | Colors -->
        <link rel="stylesheet" type="text/css" href="css/switcher.css" />
        <link rel="stylesheet" type="text/css" href="css/colors/color_1.css" id="colors-style" />

        <!-- CSS | Style -->
        <!-- Credits: http://themeforest.net/user/FlexyCodes -->
        <link rel="stylesheet" type="text/css" href="css/main.css" />

        <!-- CSS | Google Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,700,300italic,400italic,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>	
        <link href='http://fonts.googleapis.com/css?family=Russo+One' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700' rel='stylesheet' type='text/css'>

        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon/favicon.ico">

        <!--[if IE 7]>
                        <link rel="stylesheet" type="text/css" href="css/icons/font-awesome-ie7.min.css"/>
        <![endif]-->

        <!-- jquery | jQuery 1.11.0 -->
        <!-- Credits: http://jquery.com -->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <!-- Js | jquery.ui.effect.js -->

        <!-- Credits: https://jqueryui.com/effect-->
        <script type="text/javascript" src="js/jquery.ui.effect.min.js"></script>

        <!-- Js | bootstrap -->
        <!-- Credits: http://getbootstrap.com/ -->
        <script type="text/javascript" src="js/bootstrap.min.js"></script> 

        <!-- Js | masonry.pkgd.min.js -->
        <!-- Credits: http://masonry.desandro.com -->
        <script type="text/javascript" src="js/masonry.pkgd.min.js"></script>

        <!-- Js | flexslider -->
        <!-- Credits: http://www.woothemes.com/flexslider/ -->
        <script type="text/javascript" src="js/jquery.flexslider-min.js"></script>

        <!-- Js | jquery.cookie -->
        <!-- Credits: https://github.com/carhartl/jquery-cookie --> 
        <script type="text/javascript" src="js/jsSwitcher/jquery.cookie.js"></script>	

        <!-- Js | switcher -->
        <!-- Credits: http://themeforest.net/user/FlexyCodes -->
        <script type="text/javascript" src="js/jsSwitcher/switcher.js"></script>	

        <!-- Js | nicescroll.js -->
        <!-- Credits: http://areaaperta.com/nicescroll/ -->
        <script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>

        <!-- jquery | rotate and portfolio -->
        <!-- Credits: http://jquery.com -->
        <script type="text/javascript" src="js/jquery.mixitup.min.js"></script> 

        <!-- jquery | prettyPhoto -->
        <!-- Credits: http://www.no-margin-for-errors.com/ -->
        <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

        <!-- Js | gmaps -->
        <!-- Credits: http://maps.google.com/maps/api/js?sensor=true-->
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script type="text/javascript" src="js/gmaps.min.js"></script>

        <!-- Js | Js -->
        <!-- Credits: http://themeforest.net/user/FlexyCodes -->
        <script type="text/javascript" src="js/jquery.localStorage.js"></script>
        <script type="text/javascript" src="js/main.js"></script>
        <script type="text/javascript" src="js/toastr.js"></script>

        <style>
            body{	
                background: #F1F1F1;
            }

            .main-pagination {
                overflow: hidden;
                text-align: center;
                margin: 30px 0 30px 0;
                padding-right: 10px;
            }

            .masonry_load_more {
                padding: 5px 0;
                cursor: pointer;
                border: dotted 1px grey;
                border-radius: 5px;
            }

            .masonry_load_more:hover {
                background: #fff;
                color: #010101;
            }
        </style>


    </head>

    <body>

        <!--[if lt IE 7]>
                        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->


        <!-- Laoding page  -->
        <div class="preloader">
            <div class="spinner">
                <div class="dot1"></div>
                <div class="dot2"></div>
            </div>
        </div>
        <!-- End Laoding page  -->
        <!-- .wrapper-content-->
        <div class="wrapper-content">
            <div class="header-main">
                <div class="content_wrappers">		
                    <div class="logo_wrapper">

                        <span class="site_title">
                            <a href="#" title="Throne">
                                <img src="images/logo-top.png" alt="FlexyBlog">
                            </a>
                        </span>

                    </div>

                    <a class="nav-btn menu_close" id="btn_open_menu" href="#"><i class="fa fa-bars"></i></a>
                    <a class="nav-btn sidebar_close" id="btn_open_sidebar" href="#"><i class="icon-action-undo"></i></a>

                </div>

            </div>
            <!-- #header -->
	    <?php if ($show_top_menu) { ?>
    	    <div id="content-wrapper" class="menu_content_wrapper full_width<?php echo isset($_COOKIE['menu_layout']) && $_COOKIE['menu_layout'] ? ' inverse' : '' ?>">
    		<!--<div class="top-menu-bg"></div>-->
    		<div id="main-content" style="margin: 10px;margin-bottom: 0">
    		    <ul class="top-menu full_width">
    			<li>
    			    <a href="index.php" class="<?php echo $file == 'index.php' ? 'top-menu-active' : '' ?>">Home</a>
    			</li>
    			<li>
    			    <a href="#">Business</a>
    			</li>
    			<li>
    			    <a href="#">Community</a>
    			</li>
    			<li>
    			    <a href="#">Entertainment</a>
    			</li>
    			<li>
    			    <a href="#"><img class="ig-icon" src="images/logo-icon.png" /></a>
    			</li>
    			<li>
    			    <a href="#">Events</a>
    			</li>
    			<li>
    			    <a href="#">Leisure</a>
    			</li>
    			<li>
    			    <a href="#">Self</a>
    			</li>
    			<li>
    			    <a href="#">Shopping</a>
    			</li>
    			<li>
    			    <a class="connect" href="connect.php">Connect</a>
    			</li>
    			<li>
    			    <a href="#"><img class="menu-icon" src="images/logout-icon.png" /></a>
    			</li>
    			<li>
    			    <a href="#"><img class="menu-icon" src="images/share-icon.png" /></a>
    			</li>
    			<li style="position: absolute;margin-top: 5px">
    			    <a href="#" id="trigger_click" title="Color Switcher"><span class="changebutton"><i class="fa fa-2x fa-cog"></i></span></a>
    			</li>
    		    </ul>
    		</div>
    	    </div>
	    <?php } ?>
	    <style>
		#main-content {
		    padding: 0;
		    margin: 10px;
		}
	    </style>
	    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	    <script>
		$(document).ready(function () {
//		    $('#profile-panel-toggle').click().hide();
//		    $('#btn_sidebar_wrapper').hide();
		});
	    </script>
	    <!-- content -->
	    <div id="content-wrapper" class="full_width">
		<div id="main-content">
		    <div class="container-fluid">
			<div class="row-fluid">
			    <div class="col-md-6" style="border-right: dashed 1px grey">
				<h2 class="page-header" style="text-align: center">Why you Should Signup</h2>
				<div class="row">
				    <div class="col-md-10 col-md-offset-1">
					<ul class="list-group">
					    <li class="list-group-item">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi porta arcu ut molestie pretium. Nam ultrices ante vitae tortor consequat aliquam</li>
					    <li class="list-group-item">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi porta arcu ut molestie pretium. Nam ultrices ante vitae tortor consequat aliquam</li>
					    <li class="list-group-item">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi porta arcu ut molestie pretium. Nam ultrices ante vitae tortor consequat aliquam</li>
					    <li class="list-group-item">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi porta arcu ut molestie pretium. Nam ultrices ante vitae tortor consequat aliquam</li>
					    <li class="list-group-item">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi porta arcu ut molestie pretium. Nam ultrices ante vitae tortor consequat aliquam</li>
					    <li class="list-group-item">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi porta arcu ut molestie pretium. Nam ultrices ante vitae tortor consequat aliquam</li>
					</ul>
					<a class="btn btn-success btn-block" href="connect.php">Sign up</a>
				    </div>
				</div>
				<form class="form-horizontal" method="post" hidden>
				    <div class="form-group">
					<label for="fname" class="col-sm-2 control-label">First Name</label>
					<div class="col-sm-10">
					    <input type="text" class="form-control" name="fname" id="fname" placeholder="First Name" />
					</div>
				    </div>
				    <div class="form-group">
					<label for="lname" class="col-sm-2 control-label">Last Name</label>
					<div class="col-sm-10">
					    <input type="text" class="form-control" name="lname" id="lname" placeholder="Last Name" />
					</div>
				    </div>
				    <div class="form-group">
					<label for="email" class="col-sm-2 control-label">Email</label>
					<div class="col-sm-10">
					    <input type="email" class="form-control" name="email" id="email`" placeholder="Valid Email Id" />
					</div>
				    </div>
				    <div class="form-group">
					<label for="phone" class="col-sm-2 control-label">Phone</label>
					<div class="col-sm-10">
					    <input type="text" class="form-control" id="phone" placeholder="Phone Number" />
					</div>
				    </div>
				    <div class="form-group">
					<label class="col-sm-2 control-label">Gender</label>
					<div class="col-sm-10">
					    <label class="checkbox-inline">
						<input type="radio" id="gender" name="gender" value="m" checked /> Male
					    </label>
					    <label class="checkbox-inline">
						<input type="radio" id="gender" name="gender" value="f" /> Female
					    </label>
					</div>
				    </div>
				    <div class="form-group">
					<label for="dob" class="col-sm-2 control-label">Date of Birth</label>
					<div class="col-sm-10">
					    <input type="text" name="dob" id="dob" class="form-control" placeholder="Select Date of Birth" readonly />
					</div>
				    </div>
				    <hr />
				    <div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
					    <!--<button type="submit" class="btn btn-default">Sign up</button>-->
					    <a href="index.php" class="btn btn-default">Sign up</a>
					</div>
				    </div>
				</form>
			    </div>
			    <div class="col-md-6">
				<h2 class="page-header" style="text-align: center">Login</h2>
				<form class="form-horizontal" method="post">
				    <div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Email</label>
					<div class="col-sm-10">
					    <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
					</div>
				    </div>
				    <div class="form-group">
					<label for="inputPassword3" class="col-sm-2 control-label">Password</label>
					<div class="col-sm-10">
					    <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
					</div>
				    </div>
				    <div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
					    <div class="checkbox">
						<label>
						    <input type="checkbox"> Remember me
						</label>
					    </div>
					</div>
				    </div>
				    <div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
					    <!--<button type="submit" class="btn btn-default">Sign in</button>-->
					    <a href="index.php" class="btn btn-default">Sign in</a>
					</div>
				    </div>
				</form>
			    </div>
			</div>
		    </div>
		</div>
	    </div>
	    <!-- #content-wrapper -->
	    <!-- sidebar -->
	    <div id="custumize-style" style="right: -293px">

		<h1>Layout Color Switcher</h1>
		<a href="#" class="switcher"><span class="changebutton"><i class="fa fa-times"></i></span></a>
		<div class="switcher_content">
		    <p>Choose one color below to change main layout color. You can apply more styles in theme options.</p>
		    <div>
			<h3>Skins Colors</h3>
			<ul class="colors-style" id="color1">
			    <li><a href="#" class="color_1"></a></li>
			    <li><a href="#" class="color_2"></a></li>
			    <li><a href="#" class="color_3"></a></li>
			    <li><a href="#" class="color_4"></a></li>
			    <li><a href="#" class="color_5"></a></li>
			    <li><a href="#" class="color_6"></a></li>
			    <li><a href="#" class="color_7"></a></li>
			    <li><a href="#" class="color_8"></a></li>
			    <li><a href="#" class="color_9"></a></li>
			    <li><a href="#" class="color_10"></a></li>
			    <li><a href="#" class="color_11"></a></li>
			    <li><a href="#" class="color_12"></a></li>
			    <li><a href="#" class="color_13"></a></li>
			    <li><a href="#" class="color_14"></a></li>
			    <li><a href="#" class="color_15"></a></li>
			    <li><a href="#" class="color_16"></a></li>
			    <li><a href="#" class="color_17"></a></li>
			    <li><a href="#" class="color_18"></a></li>
			    <li><a href="#" class="color_19"></a></li>
			    <li><a href="#" class="color_20"></a></li>
			    <li><a href="#" class="color_21"></a></li>
			    <li><a href="#" class="color_22"></a></li>
			    <li><a href="#" class="color_23"></a></li>
			    <li><a href="#" class="color_24"></a></li>
			    <li><a href="#" class="color_25"></a></li>
			    <li><a href="#" class="color_26"></a></li>
			    <li><a href="#" class="color_27"></a></li>
			    <li><a href="#" class="color_28"></a></li>
			    <li><a href="#" class="color_29"></a></li>
			    <li><a href="#" class="color_30"></a></li>
			    <li><a href="#" class="color_31"></a></li>
			    <li><a href="#" class="color_32"></a></li>
			    <li><a href="#" class="color_33"></a></li>
			    <li><a href="#" class="color_34"></a></li>
			    <li><a href="#" class="color_35"></a></li>
			    <li><a href="#" class="color_36"></a></li>
			    <li><a href="#" class="color_37"></a></li>
			    <li><a href="#" class="color_38"></a></li>
			    <li><a href="#" class="color_39"></a></li>
			    <li><a href="#" class="color_40"></a></li>
			</ul>
		    </div>
		    <div>
			<div class="pull-right" style="padding-top: 2px">
			    <button class=" btn btn-primary btn-xs" id="menu_layout_toggle_button">
				<i class="fa fa-refresh"></i>
			    </button>
			</div>
			<h3>Toggle Menu Layout</h3>
		    </div>
		    <div id="button-reset"><a href="#" class="button color green boxed">Reset</a></div>
		</div>
	    </div>
	    <!-- #sidebar -->
	    <a id="go-top-button" class="test" href="#"><i class="fa fa-chevron-up"></i></a>
	    <!--go-top-button-->
	</div>
	<!-- end .wrapper-content-->
	<div id="hedaer_mobile">
	    <div class="menu_closed" id="menu_closed" title="Close Menu" data-toggle="tooltip">
		<i class="icon-close"></i>
	    </div>


	    <div class="logo">
		<a href="#"><img src="http://placehold.it/109x109" width="109" height="109" alt="Logo"></a>
	    </div> <!--/ .logo -->

	    <h4 class="tagline">This is a lovely area for the tagline</h4>


	    <ul id="menu_mobile">

		<li class="menu-item menu-item-has-children"><a href="#"><i class="fa fa-home"></i>Home<i class="fa fa-angle-down icon_menu"></i></a>
		    <ul class="sub-menu">	
			<li class="menu-item"><a href="home-1.html">Layout Style 1</a></li>
			<li class="menu-item"><a href="home-2.html">Layout Style 2</a></li>
			<li class="menu-item"><a href="home-3.html">Layout Style 3</a></li>
			<li class="menu-item"><a href="home-4.html">Layout Style 4</a></li>
			<li class="menu-item"><a href="home-5.html">Layout Style 5</a></li>
			<li class="menu-item"><a href="home-6.html">Layout Style 6</a></li>
			<li class="menu-item"><a href="home-7.html">Layout Style 7</a></li>
			<li class="menu-item"><a href="home-8.html">Layout Style 8</a></li>
			<li class="menu-item"><a href="home-9.html">Layout Style 9</a></li>
			<li class="menu-item"><a href="home-10.html">Layout Style 10</a></li>
		    </ul>
		</li>

		<li class="menu-item menu-item-has-children"><a href="portfolio.html"><i class="fa fa-briefcase"></i>Portfolio<i class="fa fa-angle-down icon_menu"></i></a>
		    <ul class="sub-menu">	
			<li class="menu-item"><a href="portfolio.html">Portfolio</a></li>
			<li class="menu-item"><a href="portfolio-blog_single.php">Portfolio Single</a></li>
		    </ul>
		</li>


		<li class="menu-item menu-item-has-children"><a href="#"><i class="fa fa-bullhorn"></i>Blog<i class="fa fa-angle-down icon_menu"></i></a>

		    <ul class="sub-menu">
			<li class="menu-item"><a href="home-1.html">blog classic</a></li>
			<li class="menu-item"><a href="home-7.html">blog masonry</a></li>

			<li class="menu-item menu-item-has-children"><a href="#"><i class="fa fa-th-large"></i>blog grid<i class="fa fa-angle-down icon_menu"></i></a>
			    <ul class="sub-menu">
				<li class="menu-item"><a href="home-8.html">blog grid 2 column</a></li>
				<li class="menu-item"><a href="home-9.html">blog grid 3 column</a></li>
				<li class="menu-item"><a href="home-10.html">blog grid 4 column</a></li>
			    </ul>
			</li>
			<li class="menu-item"><a href="blog_single.php">Single Blog</a></li>
		    </ul>
		</li>

		<li class="menu-item menu-item-has-children"><a href="#"><i class="fa fa-file"></i>Page Templates<i class="fa fa-angle-down icon_menu"></i></a>

		    <ul class="sub-menu">
			<li class="menu-item menu-item-has-children"><a href="#"><i class="fa fa-bars"></i>Menu<i class="fa fa-angle-down icon_menu"></i></a>
			    <ul class="sub-menu">
				<li class="menu-item"><a href="#">Menu-1</a></li>
				<li class="menu-item"><a href="menu-2.html">Menu-2</a></li>
			    </ul>
			</li>
			<li class="menu-item"><a href="about.html">About-Me</a></li>
			<li class="menu-item"><a href="blog_single.php">Single</a></li>
			<li class="menu-item"><a href="writers.html">Writers</a></li>
			<li class="menu-item"><a href="404.html">404 Error</a></li>
			<li class="menu-item"><a href="contact.html">Contact</a></li>
		    </ul>
		</li>
		<li class="menu-item"><a href="contact.html"><i class="fa fa-envelope"></i>Contact</a></li>
		<li class="menu-item"><a href="#" target="_blank"><i class="fa fa-shopping-cart"></i>Purchase</a></li>
	    </ul>
	</div>
    </body>
</html>