<?php
$title = "Event";
$menus = array(
    'index.php' => array(
        'icon' => 'home',
        'label' => 'Home',
    ),
    'index-2.php' => array(
        'icon' => 'home',
        'label' => 'Home 2',
    ),
    'event_details.php' => array(
        'icon' => 'file-text',
        'label' => 'Details',
    ),
    'event_register_ticket.php' => array(
        'icon' => 'edit',
        'label' => 'Register Tickets',
    ),
    'event_photos.php' => array(
        'icon' => 'image',
        'label' => 'Photos',
    ),
    'event_videos.php' => array(
        'icon' => 'video-camera',
        'label' => 'Videos',
    ),
    'event_attending.php' => array(
        'icon' => 'users',
        'label' => 'Who\'s Attending',
    ),
    'event_contact.php' => array(
        'icon' => 'paper-plane',
        'label' => 'Contact',
    ),
);
$show_quick_icons = FALSE;
?>
<?php include_once __DIR__ . '/header.php' ?>
<link href="css/main2.css" rel="stylesheet" type="text/css" />
<style>
    #main-content {
        padding: 0;
        margin: 0;
    }
    .page-single {
        background: linear-gradient( rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7) ), url(images/images-fullscreen/8.jpg);
        background-size: cover;
        background-position: 50% 50%;
        background-repeat: no-repeat;
    }
</style>
<!-- content -->
<div id="content-wrapper" class="page-single">
    <div id="main-content">
        <article class="home-page" id="home_style">
            <div class="row">
                <div class="col-md-12 home-profile">
                    <div class="home-profile-image"> 
                        <img src="images/profile_img.jpg" alt=""> 
                    </div>
                </div><div class="col-md-12 home-details">
                    <h2 class="name_users">i'm <span class="span_color">John doe</span></h2>
                    <h3>Hello. I am<span class="rw-words rw-words-1">
                            <span>Designer</span>
                            <span>Front-end Developer</span>
                            <span>programer</span>
                            <span>writer</span>
                            <span>freelancer</span>
                            <span>an inventor</span>
                        </span></h3>
                    <hr>
                    <p class="medium"><i>Lorem ipsum dolor sit amet, consectetur adipiscing elit <br>
                            Etiam eu cursus lectus. In ultrice s leo sed leo bibendum eu interdum urna luctus.</i> </p>
                    <ul class="social">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                    </ul>
                </div>
            </div>
        </article>
    </div>
</div>
<!-- #content-wrapper -->
<?php include_once __DIR__ . '/footer.php'; ?>